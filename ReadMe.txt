To analyse the dataset please use the following order of scripts: 

1. NR_SAFE_Functions.r
2. Load_data.r
3. Data_analysis.r
4. Print_analysis_to_files.r
5. Make_Figures.r
6. Effect_size_calculation.r

Input folder with data to be analyzed is decided by the "inDir" variable in the "Load_data.r" script.
Output folder is decided by the "outDir" variable in the "Load_data.r" script.

Data on age and sex is not available due to data security laws.

The scripts require the following R packages to have been installed to be able to run: 
- tidyverse version 1.3.1
- readxl version 1.4.0
- openxlsx version 4.2.5
- lubridate version 1.8.0
- cowplot version 1.1.1
- ggpubr version 0.4.0
- ggsignif version 0.6.3
- rvg version 0.3.2
- officer version 0.6.0
- effsize version 0.8.1
