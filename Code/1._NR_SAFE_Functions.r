
# Contains all functions used in the NR-SAFE analysis in R:

#Required packages for scripts to run
library("tidyverse")
library("readxl")
library("openxlsx")
library("lubridate")
library("cowplot")
library("ggpubr")
library("ggsignif")
library("rvg")
library("officer")
library("effsize")

###  Data filtering functions 	###
# joins Data from one specific sheet of input file from vieDoc with blinding key:
joinDataFrames = function (x, y, z, input) {
	specificSheet = read_excel(input, sheet = x, skip = 1)
	ID_list = y
	subList = inner_join(specificSheet, ID_list, "SubjectId")
	subAnnotate <- subList %>% mutate(Randomisation =z, .before= "SubjectId")
}
# function for reading the "required compounds" sheets of whole blood and urine metabolomics: 
readOmicsReqSheet = function (fileName) {
	dataFrame = read_excel(fileName, sheet = "Requested compounds")
	dataFrame = dataFrame[,c(-3, -4)]
	colnames(dataFrame) = dataFrame[4,]
	colnames(dataFrame)[1] = "MS-Omics ID"
	colnames(dataFrame)[2] = "Sample number"
	dataFrame = dataFrame[-4,]
	dataFrame = as.data.frame(dataFrame)
	return(dataFrame)
}
# function for reading the metabolomics file with annotations 1-3 combined:
readOmicsAllAnnotations = function (fileName) {
	dataFrame = read_excel(fileName, sheet = "All_annotations")
	colnames(dataFrame) = dataFrame[9,]
	colnames(dataFrame)[1] = "MS-Omics ID"
	colnames(dataFrame)[2] = "Sample number"
	dataFrame = as.data.frame(dataFrame)
	return(dataFrame)
}
###################################

### Normality testing functions	###
# Performs normality testing via the Shapiro-Wilk test on a specific variable: 
normalityTest = function (variableInQuestion, data, list_variable_DF) {
	nrbase = filter(data, data[,"Randomisation"] == "NR" & data[,"ActivityId"] == "V02_1")
	nrend = filter(data, data[,"Randomisation"] == "NR" & data[,"ActivityId"] == "V09_1")
	plbase = filter(data, data[,"Randomisation"] == "Placebo" & data[,"ActivityId"] == "V02_1")
	plend = filter(data, data[,"Randomisation"] == "Placebo" & data[,"ActivityId"] == "V09_1")
	
	nrbase = as.numeric(unlist(nrbase[,variableInQuestion]))
	nrend = as.numeric(unlist(nrend[,variableInQuestion]))
	plbase = as.numeric(unlist(plbase[,variableInQuestion]))
	plend = as.numeric(unlist(plend[,variableInQuestion]))
	
	deltaNR = nrend - nrbase
	deltaPL = plend - plbase

	if (length(unique(deltaNR)) > 1) {
		deltaNRShap = shapiro.test(deltaNR)$p.value
	}
	if (length(unique(deltaNR)) == 1) {
		deltaNRShap = "Not possible to calculate, all values are identical"
	}
	if (length(unique(deltaPL)) > 1) {
		deltaPLShap = shapiro.test(deltaPL)$p.value
	}
	if (length(unique(deltaPL)) == 1) {
		deltaPLShap = "Not possible to calculate, all values are identical"
	}
	combiFrame = data.frame(
	Value = list_variable_DF[2,variableInQuestion])
	combiFrame[,"PL_Delta"] = deltaPLShap
	combiFrame[,"NR_Delta"] = deltaNRShap
	return(combiFrame)
}
# Combines the data generated in the "normalityTest" function into a data frame: 
combineNormalityTest = function (inputDF, list_variable) {
	list_variable_DF = list_variable
	list_variable = colnames(list_variable)
	list_variable = list_variable[-1]
	firstFrame = normalityTest(list_variable[1], inputDF, list_variable_DF)
	if (firstFrame[,"PL_Delta"] < 0.05 | firstFrame[,"NR_Delta"] < 0.05) {
		frame = normalityTest(list_variable[1], inputDF, list_variable_DF)
	}
	else {
		frame = data.frame("Value" = "", "PL_Delta" = "", "NR_Delta" = "")
	}	
	list_variable = list_variable[-1]
	for (variable in list_variable) {
		tempFrame = normalityTest(variable, inputDF, list_variable_DF)
		if (tempFrame[,"PL_Delta"] < 0.05 | tempFrame[,"NR_Delta"] < 0.05) {
			print(variable)
			frame = rbind(frame, tempFrame)	
		}
	}
	if (frame[1,1] == "") { frame = frame[-1,]  }
	return(frame)
}	
###################################


###  Data summary and analysis functions 	###
# Makes summary and statistical testing on variable in one line (uses t-test): 
testAndTable = function (inputDF, variableOfinterest, list_variable_DF) {
	missingValues = filter(inputDF, (is.na(inputDF[,variableOfinterest]) == TRUE) & (inputDF[,"ActivityId"] == "V02_1" | inputDF[,"ActivityId"] == "V09_1"))
	missingValues = missingValues[,c(c("SubjectId", "EventName", "ActivityId", "Randomisation"))]
	missingValues$comment = paste(missingValues$SubjectId, missingValues$EventName, missingValues$ActivityId, sep = "_")
	missingValueString = paste(as.vector(missingValues$comment), collapse = " | ")
	
	Analysed_samples_placebo = 10
	Analysed_samples_nr = 10
	
	testFrame = filter(inputDF, inputDF[,"ActivityId"] == "V02_1" | inputDF[,"ActivityId"] == "V09_1")
	if (any(is.na(testFrame[,variableOfinterest]))  == TRUE) {
		missingValues = filter(testFrame, (is.na(testFrame[,variableOfinterest]) == TRUE) & (testFrame[,"ActivityId"] == "V02_1" | testFrame[,"ActivityId"] == "V09_1"))
		missingSubjects = missingValues[,"SubjectId"]		
		listMissingSubjects = unique(unlist(missingSubjects))
		placeboFrame = filter(testFrame, testFrame[,"Randomisation"] == "Placebo")
		nrFrame = filter(testFrame, testFrame[,"Randomisation"] == "NR")
		Analysed_samples_placebo = Analysed_samples_placebo - length(unlist(unique(placeboFrame[is.na(placeboFrame[,variableOfinterest]), "SubjectId"])))
		Analysed_samples_nr = Analysed_samples_nr - length(unlist(unique(nrFrame[is.na(nrFrame[,variableOfinterest]), "SubjectId"])))
		rows = length(listMissingSubjects)
		for (i in 1:rows) {
				inputDF = filter(inputDF, inputDF[,"SubjectId"] != listMissingSubjects[i])		
		}
	}
	
	plbase = filter(inputDF, ActivityId == "V02_1" & Randomisation == "Placebo")
	plend = filter(inputDF, ActivityId == "V09_1" & Randomisation == "Placebo")
	nrbase = filter(inputDF, ActivityId == "V02_1" & Randomisation == "NR")
	nrend = filter(inputDF, ActivityId == "V09_1" & Randomisation == "NR")

	plbase = as.numeric(unlist(plbase[,variableOfinterest]))
	plend = as.numeric(unlist(plend[,variableOfinterest]))
	nrbase = as.numeric(unlist(nrbase[,variableOfinterest]))
	nrend = as.numeric(unlist(nrend[,variableOfinterest]))
	
	pldelta = plend - plbase
	nrdelta = nrend - nrbase
	
	if (list_variable_DF[4,variableOfinterest] == "T-test") {
		plDelta_ttest = as.vector(t.test(plend, plbase, two.tailed = TRUE, paired = TRUE)[3])
		nrDelta_ttest = as.vector(t.test(nrend, nrbase, two.tailed = TRUE, paired = TRUE)[3])
		delta_ttest = as.vector(t.test(pldelta, nrdelta, two.tailed = TRUE, paired = FALSE)[3])
	}
	if (list_variable_DF[4,variableOfinterest] == "Wilcox") {
		plDelta_ttest = as.vector(wilcox.test(plend, plbase, two.tailed = TRUE, paired = TRUE)[3])
		nrDelta_ttest = as.vector(wilcox.test(nrend, nrbase, two.tailed = TRUE, paired = TRUE)[3])
		delta_ttest = as.vector(wilcox.test(pldelta, nrdelta, two.tailed = TRUE, paired = FALSE)[3])
	}
		
	summaryData = data.frame("Variable" = variableOfinterest, 
	"Name" = list_variable_DF[2,variableOfinterest],
	"Unit" = list_variable_DF[3,variableOfinterest],
	"PL_BL_Mean" = as.vector(summary(plbase)[4]), 
	"PL_BL_Sd" = sd(plbase, na.rm = TRUE),
	"PL_V09_Mean" = as.vector(summary(plend)[4]),
	"PL_V09_Sd" = sd(plend, na.rm = TRUE),
	"PL_Delta_Mean" = as.vector(summary(pldelta)[4]),
	"PL_Delta_Sd" = sd(pldelta, na.rm = TRUE),
	"PL_Delta_T_test" = as.vector(unlist(plDelta_ttest)),
	"NR_BL_Mean" = as.vector(summary(nrbase)[4]), 
	"NR_BL_Sd" = sd(nrbase, na.rm = TRUE),
	"NR_V09_Mean" = as.vector(summary(nrend)[4]),
	"NR_V09_Sd" = sd(nrend, na.rm = TRUE),
	"NR_Delta_Mean" = as.vector(summary(nrdelta)[4]),
	"NR_Delta_Sd" = sd(nrdelta, na.rm = TRUE),
	"NR_Delta_T_test" = as.vector(unlist(nrDelta_ttest)),
	"Delta_T_test" = as.vector(unlist(delta_ttest)),
	"NA_values" = missingValueString,
	"Number_analysed_PL" = Analysed_samples_placebo,
	"Number_analysed_NR" = Analysed_samples_nr)
	return(summaryData)
}
# combines lines generated in testAndTable function into data frame: 
combine = function (inputDF, list_variable) {
	list_variable_DF = list_variable
	list_variable = colnames(list_variable)
	list_variable = list_variable[-1]
	frame = testAndTable(inputDF, list_variable[1], list_variable_DF)
	list_variable = list_variable[-1]
	for (variable in list_variable) {
		frame = rbind(frame, testAndTable(inputDF, variable, list_variable_DF))	
	}
	return(frame)
}	
# performs multiple testing correction (via the Hochberg-Benjamini method/False Discovery Rate) on data generated in "combine" function: 
multipleTestingCorrectionFDR = function (dataFrame){
	ordered_PL = dataFrame[order(dataFrame$PL_Delta_T_test),]
	ordered_PL$FDR_Delta_PL_T_test = p.adjust(ordered_PL$PL_Delta_T_test, method = "BH")
	binding1 = ordered_PL[,c("Name", "FDR_Delta_PL_T_test")]
	binding1 = binding1 %>% filter(!is.na(Name))

	ordered_NR = dataFrame[order(dataFrame$NR_Delta_T_test),]
	ordered_NR$FDR_Delta_NR_T_test = p.adjust(ordered_NR$NR_Delta_T_test, method = "BH")
	binding2 = ordered_NR[,c("Name", "FDR_Delta_NR_T_test")]
	binding2 = binding2 %>% filter(!is.na(Name))

	ordered_delta = dataFrame[order(dataFrame$Delta_T_test),]
	ordered_delta$FDR_Delta_T_test = p.adjust(ordered_delta$Delta_T_test, method = "BH")
	binding3 = ordered_delta[,c("Name", "FDR_Delta_T_test")]
	binding3 = binding3 %>% filter(!is.na(Name))
	
	j = merge(binding1, binding2, by = "Name")
	j = merge(j, binding3, by = "Name")
	return(j)
}
###################################


# gathers data for use in graphs:
gatherData = function (inputDF, variableOfinterest, list_variable_DF) {
	base = filter(inputDF, ActivityId == "V02_1")
	end = filter(inputDF, ActivityId == "V09_1")

	base = base[, c("SubjectId", variableOfinterest)]
	colnames(base)[2] = "base"
	end = end[, c("SubjectId", "Randomisation", variableOfinterest)]
	colnames(end)[3] = "end"
	both = merge(base, end, by = "SubjectId")
	both[,"Name"] = list_variable_DF[2,variableOfinterest]
	both[,"Unit"] = list_variable_DF[3,variableOfinterest]
	both$Delta = as.numeric(unlist(both$end)) - as.numeric(unlist(both$base))
	both$ActivityId = "Delta"
	both = both[,c(1, 3, 8, 7)]
	colnames(both)[4] = variableOfinterest
	dataDF = inputDF[,c("SubjectId", "Randomisation", "ActivityId", variableOfinterest)]
	both = rbind(dataDF, both)
	return(both)
}
# makes a data frame according to tidyverse principles from data generated in "gatherData" function: 
combineData = function (inputDF, list_variable) {
	list_variable_DF = list_variable
	list_variable = colnames(list_variable)
	list_variable = list_variable[-1]
	frame = gatherData(inputDF, list_variable[1], list_variable_DF)
	colnames(frame)[4] = list_variable_DF[2,list_variable[1]]
	list_variable = list_variable[-1]
	for (variable in list_variable) {
		frame2 = gatherData(inputDF, variable, list_variable_DF)
		colnames(frame2)[4] = list_variable_DF[2,variable]
		frame = cbind(frame, frame2[,4])
	}
	return(frame)
}


# makes a comparison of baselines for each variable:
baselineToBaseline = function (inputDF, variableOfinterest, list_variable_DF, LOD) {
		inputDF = filter(inputDF, inputDF[,"ActivityId"] == "V02_1")

		missingValues = filter(inputDF, (is.na(inputDF[,variableOfinterest]) == TRUE))
		missingValues = missingValues[,c(c("SubjectId", "EventName", "Randomisation"))]
		missingValues$comment = paste(missingValues$SubjectId, missingValues$EventName, sep = "_")
		missingValueString = paste(as.vector(missingValues$comment), collapse = " | ")
			
		Analysed_samples_placebo = 10
		Analysed_samples_nr = 10	
		
		testFrame = inputDF
		if (any(is.na(testFrame[,variableOfinterest]))) {
			missingValues = filter(testFrame, (is.na(testFrame[,variableOfinterest]) == TRUE)) 
			missingSubjects = missingValues[,"SubjectId"]		
			listMissingSubjects = unlist(missingSubjects)
			placeboFrame = filter(testFrame, testFrame[,"Randomisation"] == "Placebo")
			nrFrame = filter(testFrame, testFrame[,"Randomisation"] == "NR")
			Analysed_samples_placebo = Analysed_samples_placebo - length(unlist(unique(placeboFrame[is.na(placeboFrame[,variableOfinterest]), "SubjectId"])))
			Analysed_samples_nr = Analysed_samples_nr - length(unlist(unique(nrFrame[is.na(nrFrame[,variableOfinterest]), "SubjectId"])))
			rows = length(listMissingSubjects)
			for (i in 1:rows) {
				inputDF = filter(inputDF, inputDF[,"SubjectId"] != listMissingSubjects[i])		
			}
		}

		if (LOD == FALSE) {
			plbase = filter(inputDF, Randomisation == "Placebo")
			nrbase = filter(inputDF, Randomisation == "NR")
	
			plbase = as.numeric(unlist(plbase[,variableOfinterest]))
			nrbase = as.numeric(unlist(nrbase[,variableOfinterest]))
	
			if (list_variable_DF[4,variableOfinterest] == "T-test") {
				t_test_test = as.vector(t.test(plbase, nrbase, two.tailed = TRUE, paired = FALSE)[3])
			}
			if (list_variable_DF[4,variableOfinterest] == "Wilcox") {
				t_test_test = as.vector(wilcox.test(plbase, nrbase, two.tailed = TRUE, paired = FALSE)[3])
			}
			
			summaryData = data.frame("Variable" = variableOfinterest, 
			"Name" = list_variable_DF[2,variableOfinterest],
			"Unit" = list_variable_DF[3,variableOfinterest],
			"PL_BL_Mean" = as.vector(summary(plbase)[4]), 
			"PL_BL_Sd" = sd(plbase, na.rm = TRUE),
			"NR_BL_Mean" = as.vector(summary(nrbase)[4]), 
			"NR_BL_Sd" = sd(nrbase, na.rm = TRUE),
			"T_Test" = as.vector(unlist(t_test_test)),
			"NA_values" = missingValueString,
			"Number_analysed_PL" = Analysed_samples_placebo,
			"Number_analysed_NR" = Analysed_samples_nr,
			"Comment" = "")
			return(summaryData)
	}
		if (LOD == TRUE) {
			Analysed_samples_placebo = 10
			Analysed_samples_nr = 10
			Plvalues = filter(inputDF, inputDF[,"Randomisation"] == "Placebo")
			NRvalues = filter(inputDF, inputDF[,"Randomisation"] == "NR")
			listVariables = colnames(list_variable_DF)
			listVariables = listVariables[-1]
			LODthreshold = as.numeric(unlist(list_variable_DF[6,variableOfinterest]))
			howManyPLbelowLOD = 0
		if (any(as.numeric(unlist(Plvalues[,variableOfinterest])) < LODthreshold)) {
			belowLODPL = which(as.numeric(unlist(Plvalues[,variableOfinterest])) < LODthreshold) 
			howManyPLbelowLOD = length(belowLODPL)
			subIdBelowLODPL = Plvalues[belowLODPL,"SubjectId"]
			subIdBelowLODPL = unique(subIdBelowLODPL)
		for (i in subIdBelowLODPL) {
				inputDF = filter(inputDF, inputDF[,"SubjectId"] != i)
			}
			Analysed_samples_placebo = Analysed_samples_placebo - length(unlist(subIdBelowLODPL))
		}
		howManyNRbelowLOD = 0 
		if (any(as.numeric(unlist(NRvalues[,variableOfinterest])) < LODthreshold)) {
			belowLODNR = which(as.numeric(unlist(NRvalues[,variableOfinterest])) < LODthreshold) 
			howManyNRbelowLOD = length(belowLODNR)
			subIdBelowLODNR = NRvalues[belowLODNR,"SubjectId"]
			subIdBelowLODNR = unique(subIdBelowLODNR)
		for (i in subIdBelowLODNR) {
				inputDF = filter(inputDF, inputDF[,"SubjectId"] != i)
			}
			Analysed_samples_nr = Analysed_samples_nr - length(unlist(subIdBelowLODNR))
		}	
		
		
		if (Analysed_samples_nr >= 7 & Analysed_samples_placebo >= 7) {
			plbase = filter(inputDF, ActivityId == "V02_1" & Randomisation == "Placebo")
			nrbase = filter(inputDF, ActivityId == "V02_1" & Randomisation == "NR")
			plbase = as.numeric(unlist(plbase[,variableOfinterest]))
			nrbase = as.numeric(unlist(nrbase[,variableOfinterest]))
			if (list_variable_DF[7,variableOfinterest] == FALSE) {
			t_test_test = as.vector(t.test(plbase, nrbase, two.tailed = TRUE, paired = FALSE)[3])
			}
			if (list_variable_DF[7,variableOfinterest] == TRUE) {
			t_test_test = as.vector(wilcox.test(plbase, nrbase, two.tailed = TRUE, paired = FALSE)[3])
			}
						
			summaryData = data.frame("Variable" = variableOfinterest, 
			"Name" = list_variable_DF[2,variableOfinterest],
			"Unit" = list_variable_DF[3,variableOfinterest],
			"PL_BL_Mean" = as.vector(summary(plbase)[4]), 
			"PL_BL_Sd" = sd(plbase, na.rm = TRUE),
			"NR_BL_Mean" = as.vector(summary(nrbase)[4]), 
			"NR_BL_Sd" = sd(nrbase, na.rm = TRUE),
			"T_Test" = as.vector(unlist(t_test_test)),
			"NA_values" = missingValueString,
			"Number_analysed_PL" = Analysed_samples_placebo,
			"Number_analysed_NR" = Analysed_samples_nr,
			"Comment" = "")
			return(summaryData)
		}
		if (Analysed_samples_nr < 7 | Analysed_samples_placebo < 7) {
			summaryData = data.frame("Variable" = variableOfinterest, 
			"Name" = list_variable_DF[2,variableOfinterest],
			"Unit" = list_variable_DF[3,variableOfinterest],
			"PL_BL_Mean" = "", 
			"PL_BL_Sd" = "",
			"NR_BL_Mean" = "", 
			"NR_BL_Sd" = "",
			"T_Test" = "",
			"NA_values" = "",
			"Number_analysed_PL" = Analysed_samples_placebo,
			"Number_analysed_NR" = Analysed_samples_nr,
			"Comment" = "Too few samples are above limit of detection or are not NA.")
		}
	}
}
# combines lines generated in baselineToBaseline function into data frame: 
combineBaselineToBaseline = function (inputDF, list_variable, LOD) {
	list_variable_DF = list_variable
	list_variable = colnames(list_variable)
	list_variable = list_variable[-1]
	frame = baselineToBaseline(inputDF, list_variable[1], list_variable_DF, LOD)
	list_variable = list_variable[-1]
	for (variable in list_variable) {
		frame = rbind(frame, baselineToBaseline(inputDF, variable, list_variable_DF, LOD))	
	}
	return(frame)
}	

# Performs multiple testing correction of baseline comparisons
MultipleTestingCorrectionBaseline_to_Baseline = function (baselineFrame) {
	ordered = baselineFrame[order(baselineFrame$T_Test),]
	ordered$FDR_T_Test = p.adjust(ordered$T_Test, method = "BH")
	binding1 = ordered[,c("Variable", "FDR_T_Test")]
	binding1 = binding1 %>% filter(!is.na(Variable))
	baselineFrame = merge(baselineFrame, binding1, by = "Variable")
	return(baselineFrame)
}

### LC-MS metabolomics functions ###
# Function to test the LC-MS metabolomics and make a summary as one line. This function also lists how many individuals which had one sample that was below the limit of detection (LOD):
testAndTableMSomicsRequiredSheets = function (inputDF, variableOfinterest, list_variable_DF) {
	missingValues = filter(inputDF, (is.na(inputDF[,variableOfinterest]) == TRUE) & (inputDF[,"ActivityId"] == "V02_1" | inputDF[,"ActivityId"] == "V09_1"))
	missingValues = missingValues[,c(c("SubjectId", "EventName", "ActivityId", "Randomisation"))]
	missingValues$comment = paste(missingValues$SubjectId, missingValues$EventName, missingValues$ActivityId, sep = "_")
	missingValueString = paste(as.vector(missingValues$comment), collapse = " | ")
	
	Analysed_samples_placebo = 10
	Analysed_samples_nr = 10
	
	testFrame = filter(inputDF, inputDF[,"ActivityId"] == "V02_1" | inputDF[,"ActivityId"] == "V09_1")
	if (any(is.na(testFrame[,variableOfinterest]))) {
		missingValues = filter(testFrame, (is.na(testFrame[,variableOfinterest]) == TRUE) & (testFrame[,"ActivityId"] == "V02_1" | testFrame[,"ActivityId"] == "V09_1"))
		missingSubjects = missingValues[,"SubjectId"]		
		listMissingSubjects = unlist(missingSubjects)
		placeboFrame = filter(testFrame, testFrame[,"Randomisation"] == "Placebo")
		nrFrame = filter(testFrame, testFrame[,"Randomisation"] == "NR")
		Analysed_samples_placebo = Analysed_samples_placebo - length(unlist(unique(placeboFrame[is.na(placeboFrame[,variableOfinterest]), "SubjectId"])))
		Analysed_samples_nr = Analysed_samples_nr - length(unlist(unique(nrFrame[is.na(nrFrame[,variableOfinterest]), "SubjectId"])))
		rows = length(listMissingSubjects)
		for (i in 1:rows) {
				inputDF = filter(inputDF, inputDF[,"SubjectId"] != listMissingSubjects[i])		
		}
	}
	
	Plvalues = filter(inputDF, inputDF[,"Randomisation"] == "Placebo")
	NRvalues = filter(inputDF, inputDF[,"Randomisation"] == "NR")
	listVariables = colnames(list_variable_DF)
	listVariables = listVariables[-1]
	count = 1
		
	LODthreshold = as.numeric(unlist(list_variable_DF[6,variableOfinterest]))
	howManyPLbelowLOD = 0
	if (any(as.numeric(unlist(Plvalues[,variableOfinterest])) < LODthreshold)) {
		belowLODPL = which(as.numeric(unlist(Plvalues[,variableOfinterest])) < LODthreshold) 
		howManyPLbelowLOD = length(belowLODPL)
	}
	howManyNRbelowLOD = 0 
	if (any(as.numeric(unlist(NRvalues[,variableOfinterest])) < LODthreshold)) {
		belowLODPL = which(as.numeric(unlist(NRvalues[,variableOfinterest])) < LODthreshold) 
		howManyNRbelowLOD = length(belowLODPL)
	}
	
	plbase = filter(inputDF, ActivityId == "V02_1" & Randomisation == "Placebo")
	plend = filter(inputDF, ActivityId == "V09_1" & Randomisation == "Placebo")
	nrbase = filter(inputDF, ActivityId == "V02_1" & Randomisation == "NR")
	nrend = filter(inputDF, ActivityId == "V09_1" & Randomisation == "NR")

	plbase = as.numeric(unlist(plbase[,variableOfinterest]))
	plend = as.numeric(unlist(plend[,variableOfinterest]))
	nrbase = as.numeric(unlist(nrbase[,variableOfinterest]))
	nrend = as.numeric(unlist(nrend[,variableOfinterest]))
	
	pldelta = plend - plbase
	nrdelta = nrend - nrbase

	if (list_variable_DF[7, variableOfinterest] == TRUE) { 	
		plDelta_T_test = as.vector(wilcox.test(plend, plbase, alternative = "two.sided", paired = TRUE)[3])
		nrDelta_T_test = as.vector(wilcox.test(nrend, nrbase, alternative = "two.sided", paired = TRUE)[3])
		delta_T_test = as.vector(wilcox.test(pldelta, nrdelta, alternative = "two.sided", paired = FALSE)[3])
	}
	if (list_variable_DF[7, variableOfinterest] == FALSE) {	
		plDelta_T_test = as.vector(t.test(plend, plbase, two.tailed = TRUE, paired = TRUE)[3])
		nrDelta_T_test = as.vector(t.test(nrend, nrbase, two.tailed = TRUE, paired = TRUE)[3])
		delta_T_test = as.vector(t.test(pldelta, nrdelta, two.tailed = TRUE, paired = FALSE)[3])
	}
	
	summaryData = data.frame(
	"Variable" = variableOfinterest, 
	"Name" = list_variable_DF[2,variableOfinterest],
	"Unit" = list_variable_DF[3,variableOfinterest],
	"PL_BL_Mean" = as.vector(summary(plbase)[4]), 
	"PL_BL_Sd" = sd(plbase, na.rm = TRUE),
	"PL_V09_Mean" = as.vector(summary(plend)[4]),
	"PL_V09_Sd" = sd(plend, na.rm = TRUE),
	"PL_Delta_Mean" = as.vector(summary(pldelta)[4]),
	"PL_Delta_Sd" = sd(pldelta, na.rm = TRUE),
	"PL_Delta_T_test" = as.vector(unlist(plDelta_T_test)),
	"NR_BL_Mean" = as.vector(summary(nrbase)[4]), 
	"NR_BL_Sd" = sd(nrbase, na.rm = TRUE),
	"NR_V09_Mean" = as.vector(summary(nrend)[4]),
	"NR_V09_Sd" = sd(nrend, na.rm = TRUE),
	"NR_Delta_Mean" = as.vector(summary(nrdelta)[4]),
	"NR_Delta_Sd" = sd(nrdelta, na.rm = TRUE),
	"NR_Delta_T_test" = as.vector(unlist(nrDelta_T_test)),
	"Delta_T_test" = as.vector(unlist(delta_T_test)),
	"NA_values" = missingValueString,
	"Number_subjectId_analysed_PL" = Analysed_samples_placebo,
	"Number_subjectId_analysed_NR" = Analysed_samples_nr,
	"PL_samples_below_LOD" = howManyPLbelowLOD,
	"NR_samples_below_LOD" = howManyNRbelowLOD)
	return(summaryData)
}
# combines output lines from the function above:
combineRequiredSheets = function (inputDF, list_variable) {
	list_variable_DF = list_variable
	list_variable = colnames(list_variable)
	list_variable = list_variable[-1]
	frame = testAndTableMSomicsRequiredSheets(inputDF, list_variable[1], list_variable_DF)
	list_variable = list_variable[-1]
	for (variable in list_variable) {
		frame = rbind(frame, testAndTableMSomicsRequiredSheets(inputDF, variable, list_variable_DF))	
	}
	return(frame)
}	
# This function is similar to the one above only that it filtes out samples from individuals which have one baseline or visit-7 value which is below the limit of detection (LOD):
# It does not analyse variables where there is more than 3 missing individuals due to them having an NA value or values below LOD:
testAndTableMSomicsRequiredSheetsFilterLODS = function (inputDF, variableOfinterest, list_variable_DF) {
	inputDF = as.data.frame(inputDF)
	
	missingValues = filter(inputDF, (is.na(inputDF[,variableOfinterest]) == TRUE) & (inputDF[,"ActivityId"] == "V02_1" | inputDF[,"ActivityId"] == "V09_1"))
	missingValues = missingValues[,c(c("SubjectId", "EventName", "ActivityId", "Randomisation"))]
	missingValues$comment = paste(missingValues$SubjectId, missingValues$EventName, missingValues$ActivityId, sep = "_")
	missingValueString = paste(as.vector(missingValues$comment), collapse = " | ")
	
	Analysed_samples_placebo = 10
	Analysed_samples_nr = 10
	
	testFrame = filter(inputDF, inputDF[,"ActivityId"] == "V02_1" | inputDF[,"ActivityId"] == "V09_1")
	if (any(is.na(testFrame[,variableOfinterest]))) {
		missingValues = filter(testFrame, (is.na(testFrame[,variableOfinterest]) == TRUE) & (testFrame[,"ActivityId"] == "V02_1" | testFrame[,"ActivityId"] == "V09_1"))
		missingSubjects = missingValues[,"SubjectId"]		
		listMissingSubjects = unlist(missingSubjects)
		placeboFrame = filter(testFrame, testFrame[,"Randomisation"] == "Placebo")
		nrFrame = filter(testFrame, testFrame[,"Randomisation"] == "NR")
		Analysed_samples_placebo = Analysed_samples_placebo - length(unlist(unique(placeboFrame[is.na(placeboFrame[,variableOfinterest]), "SubjectId"])))
		Analysed_samples_nr = Analysed_samples_nr - length(unlist(unique(nrFrame[is.na(nrFrame[,variableOfinterest]), "SubjectId"])))
		rows = length(listMissingSubjects)
		for (i in 1:rows) {
				inputDF = filter(inputDF, inputDF[,"SubjectId"] != listMissingSubjects[i])		
		}
	}
	
	Plvalues = filter(inputDF, inputDF[,"Randomisation"] == "Placebo")
	NRvalues = filter(inputDF, inputDF[,"Randomisation"] == "NR")
	listVariables = colnames(list_variable_DF)
	listVariables = listVariables[-1]
		
	LODthreshold = as.numeric(unlist(list_variable_DF[6,variableOfinterest]))
	howManyPLbelowLOD = 0
	if (any(as.numeric(unlist(Plvalues[,variableOfinterest])) < LODthreshold)) {
		belowLODPL = which(as.numeric(unlist(Plvalues[,variableOfinterest])) < LODthreshold) 
		howManyPLbelowLOD = length(belowLODPL)
		subIdBelowLODPL = Plvalues[belowLODPL,"SubjectId"]
		subIdBelowLODPL = unique(subIdBelowLODPL)
		for (i in subIdBelowLODPL) {
			inputDF = filter(inputDF, inputDF[,"SubjectId"] != i)
		}
		Analysed_samples_placebo = Analysed_samples_placebo - length(subIdBelowLODPL)
	}
	howManyNRbelowLOD = 0 
	if (any(as.numeric(unlist(NRvalues[,variableOfinterest])) < LODthreshold)) {
		belowLODNR = which(as.numeric(unlist(NRvalues[,variableOfinterest])) < LODthreshold) 
		howManyNRbelowLOD = length(belowLODNR)
		subIdBelowLODNR = NRvalues[belowLODNR,"SubjectId"]
		subIdBelowLODNR = unique(subIdBelowLODNR)
		for (i in subIdBelowLODNR) {
			inputDF = filter(inputDF, inputDF[,"SubjectId"] != i)
		}
		Analysed_samples_nr = Analysed_samples_nr - length(subIdBelowLODNR)
	}

	if (Analysed_samples_placebo >= 7 & Analysed_samples_nr >= 7) { 
		plbase = filter(inputDF, ActivityId == "V02_1" & Randomisation == "Placebo")
		plend = filter(inputDF, ActivityId == "V09_1" & Randomisation == "Placebo")
		nrbase = filter(inputDF, ActivityId == "V02_1" & Randomisation == "NR")
		nrend = filter(inputDF, ActivityId == "V09_1" & Randomisation == "NR")

		plbase = as.numeric(unlist(plbase[,variableOfinterest]))
		plend = as.numeric(unlist(plend[,variableOfinterest]))
		nrbase = as.numeric(unlist(nrbase[,variableOfinterest]))
		nrend = as.numeric(unlist(nrend[,variableOfinterest]))
	
		pldelta = plend - plbase
		nrdelta = nrend - nrbase
	
		if (list_variable_DF[7, variableOfinterest] == TRUE) { 	
			plDelta_T_test = as.vector(wilcox.test(plend, plbase, alternative = "two.sided", paired = TRUE)[3])
			nrDelta_T_test = as.vector(wilcox.test(nrend, nrbase, alternative = "two.sided", paired = TRUE)[3])
			delta_T_test = as.vector(wilcox.test(pldelta, nrdelta, alternative = "two.sided", paired = FALSE)[3])
		}
		if (list_variable_DF[7, variableOfinterest] == FALSE) {	
			plDelta_T_test = as.vector(t.test(plend, plbase, two.tailed = TRUE, paired = TRUE)[3])
			nrDelta_T_test = as.vector(t.test(nrend, nrbase, two.tailed = TRUE, paired = TRUE)[3])
			delta_T_test = as.vector(t.test(pldelta, nrdelta, two.tailed = TRUE, paired = FALSE)[3])
		}
		
		summaryData = data.frame(
		"Variable" = variableOfinterest, 
		"Name" = list_variable_DF[2,variableOfinterest],
		"Unit" = list_variable_DF[3,variableOfinterest],
		"PL_BL_Mean" = as.vector(summary(plbase)[4]), 
		"PL_BL_Sd" = sd(plbase, na.rm = TRUE),
		"PL_V09_Mean" = as.vector(summary(plend)[4]),
		"PL_V09_Sd" = sd(plend, na.rm = TRUE),
		"PL_Delta_Mean" = as.vector(summary(pldelta)[4]),
		"PL_Delta_Sd" = sd(pldelta, na.rm = TRUE),
		"PL_Delta_T_test" = as.vector(unlist(plDelta_T_test)),
		"NR_BL_Mean" = as.vector(summary(nrbase)[4]), 
		"NR_BL_Sd" = sd(nrbase, na.rm = TRUE),
		"NR_V09_Mean" = as.vector(summary(nrend)[4]),
		"NR_V09_Sd" = sd(nrend, na.rm = TRUE),
		"NR_Delta_Mean" = as.vector(summary(nrdelta)[4]),
		"NR_Delta_Sd" = sd(nrdelta, na.rm = TRUE),
		"NR_Delta_T_test" = as.vector(unlist(nrDelta_T_test)),
		"Delta_T_test" = as.vector(unlist(delta_T_test)),
		"NA_values" = missingValueString,
		"Number_subjectId_analysed_PL" = Analysed_samples_placebo,
		"Number_subjectId_analysed_NR" = Analysed_samples_nr,
		"PL_samples_below_LOD" = howManyPLbelowLOD,
		"NR_samples_below_LOD" = howManyNRbelowLOD)
		return(summaryData)
	}
	if (Analysed_samples_placebo >= 7 & Analysed_samples_nr < 7) { 
		plbase = filter(inputDF, ActivityId == "V02_1" & Randomisation == "Placebo")
		plend = filter(inputDF, ActivityId == "V09_1" & Randomisation == "Placebo")
		
		plbase = as.numeric(unlist(plbase[,variableOfinterest]))
		plend = as.numeric(unlist(plend[,variableOfinterest]))
		
		pldelta = plend - plbase
		
		if (list_variable_DF[7, variableOfinterest] == TRUE) { 	
			plDelta_T_test = as.vector(wilcox.test(plend, plbase, alternative = "two.sided", paired = TRUE)[3])
		}
		if (list_variable_DF[7, variableOfinterest] == FALSE) {	
			plDelta_T_test = as.vector(t.test(plend, plbase, two.tailed = TRUE, paired = TRUE)[3])
		}

		summaryData = data.frame(
		"Variable" = variableOfinterest, 
		"Name" = list_variable_DF[2,variableOfinterest],
		"Unit" = list_variable_DF[3,variableOfinterest],
		"PL_BL_Mean" = as.vector(summary(plbase)[4]), 
		"PL_BL_Sd" = sd(plbase, na.rm = TRUE),
		"PL_V09_Mean" = as.vector(summary(plend)[4]),
		"PL_V09_Sd" = sd(plend, na.rm = TRUE),
		"PL_Delta_Mean" = as.vector(summary(pldelta)[4]),
		"PL_Delta_Sd" = sd(pldelta, na.rm = TRUE),
		"PL_Delta_T_test" = as.vector(unlist(plDelta_T_test)),
		"NR_BL_Mean" = NA, 
		"NR_BL_Sd" = NA,
		"NR_V09_Mean" = NA,
		"NR_V09_Sd" = NA,
		"NR_Delta_Mean" = NA,
		"NR_Delta_Sd" = NA,
		"NR_Delta_T_test" = NA,
		"Delta_T_test" = NA,
		"NA_values" = missingValueString,
		"Number_subjectId_analysed_PL" = Analysed_samples_placebo,
		"Number_subjectId_analysed_NR" = Analysed_samples_nr,
		"PL_samples_below_LOD" = howManyPLbelowLOD,
		"NR_samples_below_LOD" = howManyNRbelowLOD)
		return(summaryData)
	}
	if (Analysed_samples_placebo < 7 & Analysed_samples_nr >= 7) { 
		nrbase = filter(inputDF, ActivityId == "V02_1" & Randomisation == "NR")
		nrend = filter(inputDF, ActivityId == "V09_1" & Randomisation == "NR")

		nrbase = as.numeric(unlist(nrbase[,variableOfinterest]))
		nrend = as.numeric(unlist(nrend[,variableOfinterest]))
	
		nrdelta = nrend - nrbase
	
		if (list_variable_DF[7, variableOfinterest] == TRUE) { 	
			nrDelta_T_test = as.vector(wilcox.test(nrend, nrbase, alternative = "two.sided", paired = TRUE)[3])
		}
		if (list_variable_DF[7, variableOfinterest] == FALSE) {	
			nrDelta_T_test = as.vector(t.test(nrend, nrbase, two.tailed = TRUE, paired = TRUE)[3])
		}
	
		summaryData = data.frame(
		"Variable" = variableOfinterest, 
		"Name" = list_variable_DF[2,variableOfinterest],
		"Unit" = list_variable_DF[3,variableOfinterest],
		"PL_BL_Mean" = NA, 
		"PL_BL_Sd" = NA,
		"PL_V09_Mean" = NA,
		"PL_V09_Sd" = NA,
		"PL_Delta_Mean" = NA,
		"PL_Delta_Sd" = NA,
		"PL_Delta_T_test" = NA,
		"NR_BL_Mean" = as.vector(summary(nrbase)[4]), 
		"NR_BL_Sd" = sd(nrbase, na.rm = TRUE),
		"NR_V09_Mean" = as.vector(summary(nrend)[4]),
		"NR_V09_Sd" = sd(nrend, na.rm = TRUE),
		"NR_Delta_Mean" = as.vector(summary(nrdelta)[4]),
		"NR_Delta_Sd" = sd(nrdelta, na.rm = TRUE),
		"NR_Delta_T_test" = as.vector(unlist(nrDelta_T_test)),
		"Delta_T_test" = NA,
		"NA_values" = missingValueString,
		"Number_subjectId_analysed_PL" = Analysed_samples_placebo,
		"Number_subjectId_analysed_NR" = Analysed_samples_nr,
		"PL_samples_below_LOD" = howManyPLbelowLOD,
		"NR_samples_below_LOD" = howManyNRbelowLOD)
		return(summaryData)
	}
	if (Analysed_samples_placebo < 7 & Analysed_samples_nr < 7) { 
		summaryData = data.frame(
		"Variable" = variableOfinterest, 
		"Name" = list_variable_DF[2,variableOfinterest],
		"Unit" = list_variable_DF[3,variableOfinterest],
		"PL_BL_Mean" = NA, 
		"PL_BL_Sd" = NA,
		"PL_V09_Mean" = NA,
		"PL_V09_Sd" = NA,
		"PL_Delta_Mean" = NA,
		"PL_Delta_Sd" = NA,
		"PL_Delta_T_test" = NA,
		"NR_BL_Mean" = NA, 
		"NR_BL_Sd" = NA,
		"NR_V09_Mean" = NA,
		"NR_V09_Sd" = NA,
		"NR_Delta_Mean" = NA,
		"NR_Delta_Sd" = NA,
		"NR_Delta_T_test" = NA,
		"Delta_T_test" = NA,
		"NA_values" = missingValueString,
		"Number_subjectId_analysed_PL" = Analysed_samples_placebo,
		"Number_subjectId_analysed_NR" = Analysed_samples_nr,
		"PL_samples_below_LOD" = howManyPLbelowLOD,
		"NR_samples_below_LOD" = howManyNRbelowLOD)
		return(summaryData)
	}	
}
# combines output lines from the function above:
combineRequiredSheetsFiltersLODS = function (inputDF, list_variable) {
	list_variable_DF = list_variable
	list_variable = colnames(list_variable)
	list_variable = list_variable[-1]
	frame = testAndTableMSomicsRequiredSheetsFilterLODS(inputDF, list_variable[1], list_variable_DF)
	list_variable = list_variable[-1]
	for (variable in list_variable) {
		frame = rbind(frame, testAndTableMSomicsRequiredSheetsFilterLODS(inputDF, variable, list_variable_DF))	
	}
	return(frame)
}	

# Performs Wilcoxon and T-test of selected variable to compare if significant difference of outcome between the tests: 
testAndTableWilcoxAndTtest = function (inputDF, variableOfinterest, list_variable_DF) {
	missingValues = filter(inputDF, (is.na(inputDF[,variableOfinterest]) == TRUE) & (inputDF[,"ActivityId"] == "V02_1" | inputDF[,"ActivityId"] == "V09_1"))
	missingValues = missingValues[,c(c("SubjectId", "EventName", "ActivityId", "Randomisation"))]
	missingValues$comment = paste(missingValues$SubjectId, missingValues$EventName, missingValues$ActivityId, sep = "_")
	missingValueString = paste(as.vector(missingValues$comment), collapse = " | ")
	
	Analysed_samples_placebo = 10
	Analysed_samples_nr = 10
	
	testFrame = filter(inputDF, inputDF[,"ActivityId"] == "V02_1" | inputDF[,"ActivityId"] == "V09_1")
	if (any(is.na(testFrame[,variableOfinterest]))  == TRUE) {
		missingValues = filter(testFrame, (is.na(testFrame[,variableOfinterest]) == TRUE) & (testFrame[,"ActivityId"] == "V02_1" | testFrame[,"ActivityId"] == "V09_1"))
		missingSubjects = missingValues[,"SubjectId"]		
		listMissingSubjects = unique(unlist(missingSubjects))
		placeboFrame = filter(testFrame, testFrame[,"Randomisation"] == "Placebo")
		nrFrame = filter(testFrame, testFrame[,"Randomisation"] == "NR")
		Analysed_samples_placebo = Analysed_samples_placebo - length(unlist(unique(placeboFrame[is.na(placeboFrame[,variableOfinterest]), "SubjectId"])))
		Analysed_samples_nr = Analysed_samples_nr - length(unlist(unique(nrFrame[is.na(nrFrame[,variableOfinterest]), "SubjectId"])))
		rows = length(listMissingSubjects)
		for (i in 1:rows) {
				inputDF = filter(inputDF, inputDF[,"SubjectId"] != listMissingSubjects[i])		
		}
	}
	
	plbase = filter(inputDF, ActivityId == "V02_1" & Randomisation == "Placebo")
	plend = filter(inputDF, ActivityId == "V09_1" & Randomisation == "Placebo")
	nrbase = filter(inputDF, ActivityId == "V02_1" & Randomisation == "NR")
	nrend = filter(inputDF, ActivityId == "V09_1" & Randomisation == "NR")

	plbase = as.numeric(unlist(plbase[,variableOfinterest]))
	plend = as.numeric(unlist(plend[,variableOfinterest]))
	nrbase = as.numeric(unlist(nrbase[,variableOfinterest]))
	nrend = as.numeric(unlist(nrend[,variableOfinterest]))
	
	pldelta = plend - plbase
	nrdelta = nrend - nrbase
	
	plDelta_ttest = as.vector(t.test(plend, plbase, two.tailed = TRUE, paired = TRUE)[3])
	nrDelta_ttest = as.vector(t.test(nrend, nrbase, two.tailed = TRUE, paired = TRUE)[3])
	delta_ttest = as.vector(t.test(pldelta, nrdelta, two.tailed = TRUE, paired = FALSE)[3])

	plDelta_wilcox = as.vector(wilcox.test(plend, plbase, two.tailed = TRUE, paired = TRUE)[3])
	nrDelta_wilcox = as.vector(wilcox.test(nrend, nrbase, two.tailed = TRUE, paired = TRUE)[3])
	delta_wilcox = as.vector(wilcox.test(pldelta, nrdelta, two.tailed = TRUE, paired = FALSE)[3])
		
	summaryData = data.frame("Variable" = variableOfinterest, 
	"Name" = list_variable_DF[2,variableOfinterest],
	"Unit" = list_variable_DF[3,variableOfinterest],
	"PL_BL_Mean" = as.vector(summary(plbase)[4]), 
	"PL_BL_Sd" = sd(plbase, na.rm = TRUE),
	"PL_V09_Mean" = as.vector(summary(plend)[4]),
	"PL_V09_Sd" = sd(plend, na.rm = TRUE),
	"PL_Delta_Mean" = as.vector(summary(pldelta)[4]),
	"PL_Delta_Sd" = sd(pldelta, na.rm = TRUE),
	"PL_Delta_T_test" = as.vector(unlist(plDelta_ttest)),
	"PL_Delta_Wilcox" = as.vector(unlist(plDelta_wilcox)),
	"NR_BL_Mean" = as.vector(summary(nrbase)[4]), 
	"NR_BL_Sd" = sd(nrbase, na.rm = TRUE),
	"NR_V09_Mean" = as.vector(summary(nrend)[4]),
	"NR_V09_Sd" = sd(nrend, na.rm = TRUE),
	"NR_Delta_Mean" = as.vector(summary(nrdelta)[4]),
	"NR_Delta_Sd" = sd(nrdelta, na.rm = TRUE),
	"NR_Delta_T_test" = as.vector(unlist(nrDelta_ttest)),
	"NR_Delta_Wilcox" = as.vector(unlist(nrDelta_wilcox)),
	"Delta_T_test" = as.vector(unlist(delta_ttest)),
	"Delta_Wilcox" = as.vector(unlist(delta_wilcox)),
	"NA_values" = missingValueString,
	"Number_analysed_PL" = Analysed_samples_placebo,
	"Number_analysed_NR" = Analysed_samples_nr)
	
	return(summaryData)
}
# combines the output above into a data frame: 
combineWilcoxAndTtest = function (inputDF, list_variable) {
	list_variable_DF = list_variable
	list_variable = colnames(list_variable)
	list_variable = list_variable[-1]
	frame = testAndTableWilcoxAndTtest(inputDF, list_variable[1], list_variable_DF)
	list_variable = list_variable[-1]
	for (variable in list_variable) {
		frame = rbind(frame, testAndTableWilcoxAndTtest(inputDF, variable, list_variable_DF))	
	}
	return(frame)
}	

# Performs Wilcoxon and T-test of selected variable to compare if significant difference of outcome between the tests:
# Filters out values where more than 3 values are below the limit of detection (LOD):
testAndTableMSomicsRequiredSheetsFilterLODSWilcoxAndTtest = function (inputDF, variableOfinterest, list_variable_DF) {
	inputDF = as.data.frame(inputDF)

	missingValues = filter(inputDF, (is.na(inputDF[,variableOfinterest]) == TRUE) & (inputDF[,"ActivityId"] == "V02_1" | inputDF[,"ActivityId"] == "V09_1"))
	missingValues = missingValues[,c(c("SubjectId", "EventName", "ActivityId", "Randomisation"))]
	missingValues$comment = paste(missingValues$SubjectId, missingValues$EventName, missingValues$ActivityId, sep = "_")
	missingValueString = paste(as.vector(missingValues$comment), collapse = " | ")
	
	Analysed_samples_placebo = 10
	Analysed_samples_nr = 10
	
	testFrame = filter(inputDF, inputDF[,"ActivityId"] == "V02_1" | inputDF[,"ActivityId"] == "V09_1")
	if (any(is.na(testFrame[,variableOfinterest]))) {
		missingValues = filter(testFrame, (is.na(testFrame[,variableOfinterest]) == TRUE) & (testFrame[,"ActivityId"] == "V02_1" | testFrame[,"ActivityId"] == "V09_1"))
		missingSubjects = missingValues[,"SubjectId"]		
		listMissingSubjects = unlist(missingSubjects)
		placeboFrame = filter(testFrame, testFrame[,"Randomisation"] == "Placebo")
		nrFrame = filter(testFrame, testFrame[,"Randomisation"] == "NR")
		Analysed_samples_placebo = Analysed_samples_placebo - length(unlist(unique(placeboFrame[is.na(placeboFrame[,variableOfinterest]), "SubjectId"])))
		Analysed_samples_nr = Analysed_samples_nr - length(unlist(unique(nrFrame[is.na(nrFrame[,variableOfinterest]), "SubjectId"])))
		rows = length(listMissingSubjects)
		for (i in 1:rows) {
				inputDF = filter(inputDF, inputDF[,"SubjectId"] != listMissingSubjects[i])		
		}
	}
	
	Plvalues = filter(inputDF, inputDF[,"Randomisation"] == "Placebo")
	NRvalues = filter(inputDF, inputDF[,"Randomisation"] == "NR")
	listVariables = colnames(list_variable_DF)
	listVariables = listVariables[-1]
		
	LODthreshold = as.numeric(unlist(list_variable_DF[6,variableOfinterest]))
	howManyPLbelowLOD = 0
	if (any(as.numeric(unlist(Plvalues[,variableOfinterest])) < LODthreshold)) {
		belowLODPL = which(as.numeric(unlist(Plvalues[,variableOfinterest])) < LODthreshold) 
		howManyPLbelowLOD = length(belowLODPL)
		subIdBelowLODPL = Plvalues[belowLODPL,"SubjectId"]
		subIdBelowLODPL = unique(subIdBelowLODPL)
		for (i in subIdBelowLODPL) {
			inputDF = filter(inputDF, inputDF[,"SubjectId"] != i)
		}
		Analysed_samples_placebo = Analysed_samples_placebo - length(unlist(subIdBelowLODPL))
	}
	howManyNRbelowLOD = 0 
	if (any(as.numeric(unlist(NRvalues[,variableOfinterest])) < LODthreshold)) {
		belowLODNR = which(as.numeric(unlist(NRvalues[,variableOfinterest])) < LODthreshold) 
		howManyNRbelowLOD = length(belowLODNR)
		subIdBelowLODNR = NRvalues[belowLODNR,"SubjectId"]
		subIdBelowLODNR = unique(subIdBelowLODNR)
		for (i in subIdBelowLODNR) {
			print(i)
			inputDF = filter(inputDF, inputDF[,"SubjectId"] != i)
		}
		Analysed_samples_nr = Analysed_samples_nr - length(unlist(subIdBelowLODNR))
	}

	if (Analysed_samples_placebo >= 7 & Analysed_samples_nr >= 7) { 
		plbase = filter(inputDF, ActivityId == "V02_1" & Randomisation == "Placebo")
		plend = filter(inputDF, ActivityId == "V09_1" & Randomisation == "Placebo")
		nrbase = filter(inputDF, ActivityId == "V02_1" & Randomisation == "NR")
		nrend = filter(inputDF, ActivityId == "V09_1" & Randomisation == "NR")

		plbase = as.numeric(unlist(plbase[,variableOfinterest]))
		plend = as.numeric(unlist(plend[,variableOfinterest]))
		nrbase = as.numeric(unlist(nrbase[,variableOfinterest]))
		nrend = as.numeric(unlist(nrend[,variableOfinterest]))
	
		pldelta = plend - plbase
		nrdelta = nrend - nrbase
	
		plDelta_T_test = as.vector(t.test(plend, plbase, two.tailed = TRUE, paired = TRUE)[3])
		nrDelta_T_test = as.vector(t.test(nrend, nrbase, two.tailed = TRUE, paired = TRUE)[3])
		delta_T_test = as.vector(t.test(pldelta, nrdelta, two.tailed = TRUE, paired = FALSE)[3])
		
		plDelta_wilcox = as.vector(wilcox.test(plend, plbase, two.tailed = TRUE, paired = TRUE)[3])
		nrDelta_wilcox = as.vector(wilcox.test(nrend, nrbase, two.tailed = TRUE, paired = TRUE)[3])
		delta_wilcox = as.vector(wilcox.test(pldelta, nrdelta, two.tailed = TRUE, paired = FALSE)[3])

	
		summaryData = data.frame(
		"Variable" = variableOfinterest, 
		"Name" = list_variable_DF[2,variableOfinterest],
		"Unit" = list_variable_DF[3,variableOfinterest],
		"PL_BL_Mean" = as.vector(summary(plbase)[4]), 
		"PL_BL_Sd" = sd(plbase, na.rm = TRUE),
		"PL_V09_Mean" = as.vector(summary(plend)[4]),
		"PL_V09_Sd" = sd(plend, na.rm = TRUE),
		"PL_Delta_Mean" = as.vector(summary(pldelta)[4]),
		"PL_Delta_Sd" = sd(pldelta, na.rm = TRUE),
		"PL_Delta_T_test" = as.vector(unlist(plDelta_T_test)),
		"PL_Delta_Wilcox" = as.vector(unlist(plDelta_wilcox)),
		"NR_BL_Mean" = as.vector(summary(nrbase)[4]), 
		"NR_BL_Sd" = sd(nrbase, na.rm = TRUE),
		"NR_V09_Mean" = as.vector(summary(nrend)[4]),
		"NR_V09_Sd" = sd(nrend, na.rm = TRUE),
		"NR_Delta_Mean" = as.vector(summary(nrdelta)[4]),
		"NR_Delta_Sd" = sd(nrdelta, na.rm = TRUE),
		"NR_Delta_T_test" = as.vector(unlist(nrDelta_T_test)),
		"NR_Delta_Wilcox" = as.vector(unlist(nrDelta_wilcox)),
		"Delta_T_test" = as.vector(unlist(delta_T_test)),
		"Delta_Wilcox" = as.vector(unlist(delta_wilcox)),
		"NA_values" = missingValueString,
		"Number_subjectId_analysed_PL" = Analysed_samples_placebo,
		"Number_subjectId_analysed_NR" = Analysed_samples_nr,
		"PL_samples_below_LOD" = howManyPLbelowLOD,
		"NR_samples_below_LOD" = howManyNRbelowLOD)
		return(summaryData)
	}
	if (Analysed_samples_placebo >= 7 & Analysed_samples_nr < 7) { 
		plbase = filter(inputDF, ActivityId == "V02_1" & Randomisation == "Placebo")
		plend = filter(inputDF, ActivityId == "V09_1" & Randomisation == "Placebo")
		
		plbase = as.numeric(unlist(plbase[,variableOfinterest]))
		plend = as.numeric(unlist(plend[,variableOfinterest]))
		
		pldelta = plend - plbase
		
		plDelta_T_test = as.vector(t.test(plend, plbase, two.tailed = TRUE, paired = TRUE)[3])
				
		plDelta_wilcox = as.vector(wilcox.test(plend, plbase, two.tailed = TRUE, paired = TRUE)[3])
		
		summaryData = data.frame(
		"Variable" = variableOfinterest, 
		"Name" = list_variable_DF[2,variableOfinterest],
		"Unit" = list_variable_DF[3,variableOfinterest],
		"PL_BL_Mean" = as.vector(summary(plbase)[4]), 
		"PL_BL_Sd" = sd(plbase, na.rm = TRUE),
		"PL_V09_Mean" = as.vector(summary(plend)[4]),
		"PL_V09_Sd" = sd(plend, na.rm = TRUE),
		"PL_Delta_Mean" = as.vector(summary(pldelta)[4]),
		"PL_Delta_Sd" = sd(pldelta, na.rm = TRUE),
		"PL_Delta_T_test" = as.vector(unlist(plDelta_T_test)),
		"PL_Delta_Wilcox" = as.vector(unlist(plDelta_wilcox)),
		"NR_BL_Mean" = NA, 
		"NR_BL_Sd" = NA,
		"NR_V09_Mean" = NA,
		"NR_V09_Sd" = NA,
		"NR_Delta_Mean" = NA,
		"NR_Delta_Sd" = NA,
		"NR_Delta_T_test" = NA,
		"NR_Delta_Wilcox" = NA, 
		"Delta_T_test" = NA,
		"Delta_Wilcox" = NA, 
		"NA_values" = missingValueString,
		"Number_subjectId_analysed_PL" = Analysed_samples_placebo,
		"Number_subjectId_analysed_NR" = Analysed_samples_nr,
		"PL_samples_below_LOD" = howManyPLbelowLOD,
		"NR_samples_below_LOD" = howManyNRbelowLOD)
		return(summaryData)
	}
	if (Analysed_samples_placebo < 7 & Analysed_samples_nr >= 7) { 
		nrbase = filter(inputDF, ActivityId == "V02_1" & Randomisation == "NR")
		nrend = filter(inputDF, ActivityId == "V09_1" & Randomisation == "NR")

		nrbase = as.numeric(unlist(nrbase[,variableOfinterest]))
		nrend = as.numeric(unlist(nrend[,variableOfinterest]))
	
		nrdelta = nrend - nrbase
	
		nrDelta_T_test = as.vector(t.test(nrend, nrbase, two.tailed = TRUE, paired = TRUE)[3])
		
		nrDelta_wilcox = as.vector(wilcox.test(nrend, nrbase, two.tailed = TRUE, paired = TRUE)[3])
		
		summaryData = data.frame(
		"Variable" = variableOfinterest, 
		"Name" = list_variable_DF[2,variableOfinterest],
		"Unit" = list_variable_DF[3,variableOfinterest],
		"PL_BL_Mean" = NA, 
		"PL_BL_Sd" = NA,
		"PL_V09_Mean" = NA,
		"PL_V09_Sd" = NA,
		"PL_Delta_Mean" = NA,
		"PL_Delta_Sd" = NA,
		"PL_Delta_T_test" = NA,
		"PL_Delta_Wilcox" = NA, 
		"NR_BL_Mean" = as.vector(summary(nrbase)[4]), 
		"NR_BL_Sd" = sd(nrbase, na.rm = TRUE),
		"NR_V09_Mean" = as.vector(summary(nrend)[4]),
		"NR_V09_Sd" = sd(nrend, na.rm = TRUE),
		"NR_Delta_Mean" = as.vector(summary(nrdelta)[4]),
		"NR_Delta_Sd" = sd(nrdelta, na.rm = TRUE),
		"NR_Delta_T_test" = as.vector(unlist(nrDelta_T_test)),
		"NR_Delta_Wilcox" = as.vector(unlist(nrDelta_wilcox)),
		"Delta_T_test" = NA,
		"Delta_Wilcox" = NA, 
		"NA_values" = missingValueString,
		"Number_subjectId_analysed_PL" = Analysed_samples_placebo,
		"Number_subjectId_analysed_NR" = Analysed_samples_nr,
		"PL_samples_below_LOD" = howManyPLbelowLOD,
		"NR_samples_below_LOD" = howManyNRbelowLOD)
		return(summaryData)
	}
	if (Analysed_samples_placebo < 7 & Analysed_samples_nr < 7) { 
		summaryData = data.frame(
		"Variable" = variableOfinterest, 
		"Name" = list_variable_DF[2,variableOfinterest],
		"Unit" = list_variable_DF[3,variableOfinterest],
		"PL_BL_Mean" = NA, 
		"PL_BL_Sd" = NA,
		"PL_V09_Mean" = NA,
		"PL_V09_Sd" = NA,
		"PL_Delta_Mean" = NA,
		"PL_Delta_Sd" = NA,
		"PL_Delta_T_test" = NA,
		"PL_Delta_Wilcox" = NA, 
		"NR_BL_Mean" = NA, 
		"NR_BL_Sd" = NA,
		"NR_V09_Mean" = NA,
		"NR_V09_Sd" = NA,
		"NR_Delta_Mean" = NA,
		"NR_Delta_Sd" = NA,
		"NR_Delta_T_test" = NA,
		"NR_Delta_Wilcox" = NA, 
		"Delta_T_test" = NA,
		"Delta_Wilcox" = NA, 
		"NA_values" = missingValueString,
		"Number_subjectId_analysed_PL" = Analysed_samples_placebo,
		"Number_subjectId_analysed_NR" = Analysed_samples_nr,
		"PL_samples_below_LOD" = howManyPLbelowLOD,
		"NR_samples_below_LOD" = howManyNRbelowLOD)
		return(summaryData)
	}	
}
# combines the output above into a data frame: 
combineRequiredSheetsFiltersLODSWilcoxAndTtest = function (inputDF, list_variable) {
	list_variable_DF = list_variable
	list_variable = colnames(list_variable)
	list_variable = list_variable[-1]
	frame = testAndTableMSomicsRequiredSheetsFilterLODSWilcoxAndTtest(inputDF, list_variable[1], list_variable_DF)
	list_variable = list_variable[-1]
	for (variable in list_variable) {
		frame = rbind(frame, testAndTableMSomicsRequiredSheetsFilterLODSWilcoxAndTtest(inputDF, variable, list_variable_DF))	
	}
	return(frame)
}	

# Performs multiple testing correction of the functions above: 
multipleTestingCorrectionFDRWilcoxAndTtest = function (dataFrame){
	ordered_PL_t = dataFrame[order(dataFrame$PL_Delta_T_test),]
	ordered_PL_t$FDR_Delta_PL_T_test = p.adjust(ordered_PL_t$PL_Delta_T_test, method = "BH")
	binding1 = ordered_PL_t[,c("Name", "FDR_Delta_PL_T_test")]
	binding1 = binding1 %>% filter(!is.na(Name))

	ordered_NR_t = dataFrame[order(dataFrame$NR_Delta_T_test),]
	ordered_NR_t$FDR_Delta_NR_T_test = p.adjust(ordered_NR_t$NR_Delta_T_test, method = "BH")
	binding2 = ordered_NR_t[,c("Name", "FDR_Delta_NR_T_test")]
	binding2 = binding2 %>% filter(!is.na(Name))

	ordered_delta_t = dataFrame[order(dataFrame$Delta_T_test),]
	ordered_delta_t$FDR_Delta_T_test = p.adjust(ordered_delta_t$Delta_T_test, method = "BH")
	binding3 = ordered_delta_t[,c("Name", "FDR_Delta_T_test")]
	binding3 = binding3 %>% filter(!is.na(Name))
	
	ordered_PL_w = dataFrame[order(dataFrame$PL_Delta_Wilcox),]
	ordered_PL_w$FDR_Delta_PL_Wilcox = p.adjust(ordered_PL_w$PL_Delta_Wilcox, method = "BH")
	binding4 = ordered_PL_w[,c("Name", "FDR_Delta_PL_Wilcox")]
	binding4 = binding4 %>% filter(!is.na(Name))

	ordered_NR_w = dataFrame[order(dataFrame$NR_Delta_Wilcox),]
	ordered_NR_w$FDR_Delta_NR_Wilcox = p.adjust(ordered_NR_w$NR_Delta_Wilcox, method = "BH")
	binding5 = ordered_NR_w[,c("Name", "FDR_Delta_NR_Wilcox")]
	binding5 = binding5 %>% filter(!is.na(Name))

	ordered_delta_w = dataFrame[order(dataFrame$Delta_Wilcox),]
	ordered_delta_w$FDR_Delta_Wilcox = p.adjust(ordered_delta_w$Delta_Wilcox, method = "BH")
	binding6 = ordered_delta_w[,c("Name", "FDR_Delta_Wilcox")]
	binding6 = binding6 %>% filter(!is.na(Name))	
		
	j = merge(binding1, binding2, by = "Name")
	j = merge(j, binding3, by = "Name")
	j = merge(j, binding4, by = "Name")
	j = merge(j, binding5, by = "Name")
	j = merge(j, binding6, by = "Name")
	return(j)
}


# The following functions formats generated data frames as tables: 
# Is formatted for all other anlyses than the omics analyses:
FormattedTable = function (dataFrame) {
	variable = dataFrame[,"Variable"]
	tableFrame = data.frame("Test" = "", "Visit" = "", "PL_mean" = "", "Sd_PL" ="", "NR_mean" = "", "Sd_NR" = "", "p_value" = "")
	rowCount = 1
	for (value in variable) {
		line = filter(dataFrame, dataFrame[,"Variable"] == value)
		tableFrame[rowCount,] = c(paste0(line[1,1]," (",line[1,3],")"), "V1", line[1,4], line[1,5], line[1,11], line[1,12], "")
		rowCount = rowCount + 1
		tableFrame[rowCount,] = c("", "V7", line[1,6], line[1,7], line[1,13], line[1,14], "")
		rowCount = rowCount + 1	
		tableFrame[rowCount,] = c("", "Delta", line[1,8], line[1,9], line[1,15], line[1,16], line[1,18])  # last two are p-values
		rowCount = rowCount + 1
		tableFrame[rowCount,] = c("", "p_value", line[1,10], "", line[1,17], "", "") # p-values
		rowCount = rowCount + 1
	}
	tableFrame[,3] = as.numeric(tableFrame[,3])
	tableFrame[,4] = as.numeric(tableFrame[,4])
	tableFrame[,5] = as.numeric(tableFrame[,5])
	tableFrame[,6] = as.numeric(tableFrame[,6])
	tableFrame[,7] = as.numeric(tableFrame[,7])
	return(tableFrame)
}

# Version of the function above which produces rounded values with 3 decimals:
FormattedTableRounded = function (dataFrame) {
	dataFrame[,4] = as.numeric(dataFrame[,4])
	dataFrame[,5] = as.numeric(dataFrame[,5])
	dataFrame[,6] = as.numeric(dataFrame[,6])
	dataFrame[,7] = as.numeric(dataFrame[,7])
	dataFrame[,8] = as.numeric(dataFrame[,8])
	dataFrame[,9] = as.numeric(dataFrame[,9])
	dataFrame[,10] = as.numeric(dataFrame[,10])
	dataFrame[,11] = as.numeric(dataFrame[,11])
	dataFrame[,12] = as.numeric(dataFrame[,12])
	dataFrame[,13] = as.numeric(dataFrame[,13])
	dataFrame[,14] = as.numeric(dataFrame[,14])
	dataFrame[,15] = as.numeric(dataFrame[,15])
	dataFrame[,16] = as.numeric(dataFrame[,16])
	dataFrame[,17] = as.numeric(dataFrame[,17])
	dataFrame[,18] = as.numeric(dataFrame[,18])
	dataFrame <- data.frame(lapply(dataFrame, function(x) if(is.numeric(x)) round(x, 3) else x))
	variable = dataFrame[,"Variable"]	
	tableFrame = data.frame("Test" = "", "Visit" = "", "Placebo_sd" = "", "NR_sd" = "", "p_value" = "")
	rowCount = 1
	for (value in variable) {
		line = filter(dataFrame, dataFrame[,"Variable"] == value)	
		line[1,4] = round(line[1,4], 3)
		line[1,5] = round(line[1,5], 3)
		line[1,11] = round(line[1,11], 3)
		line[1,12] = round(line[1,12], 3)
		line[1,6] = round(line[1,6], 3)
		line[1,7] = round(line[1,7], 3)
		line[1,13] = round(line[1,13], 3)
		line[1,14] = round(line[1,14], 3)
		line[1,8] = round(line[1,8], 3)
		line[1,9] = round(line[1,9], 3)
		line[1,15] = round(line[1,15], 3)
		line[1,16] = round(line[1,16], 3)
		line[1,18] = round(line[1,18], 3)
		line[1,10] = round(line[1,10], 3)
		line[1,17] = round(line[1,17], 3)
		if (line[1,18] < 0.001) {line[1,18] = "<0.001"}
		if (line[1,10] < 0.001) {line[1,10] = "<0.001"}
		if (line[1,17] < 0.001) {line[1,17] = "<0.001"}
		tableFrame[rowCount,] = c(paste0(line[1,1]," (",line[1,3],")"), "V1", paste0(line[1,4]," +- ",line[1,5]), paste0(line[1,11]," +- " ,line[1,12]), "")
		rowCount = rowCount + 1
		tableFrame[rowCount,] = c("", "V7", paste0(line[1,6]," +- ",line[1,7]), paste0(line[1,13]," +- ",line[1,14]), "")
		rowCount = rowCount + 1	
		tableFrame[rowCount,] = c("", "Delta", paste0(line[1,8]," +- ",line[1,9]), paste0(line[1,15]," +- ",line[1,16]), line[1,18]) 
		rowCount = rowCount + 1
		tableFrame[rowCount,] = c("", "p_value", line[1,10], line[1,17], "") # p-values
		rowCount = rowCount + 1
	}
	return(tableFrame)
}

# includes adjusted p-values:
FormattedTableWithAdjPValue = function (dataFrame) {
	variable = dataFrame[,"Variable"]
	tableFrame = data.frame("Test" = "", "Visit" = "", "PL_mean" = "", "Sd_PL" ="", "NR_mean" = "", "Sd_NR" = "", "p_value" = "", "p_adj" = "")
	rowCount = 1
	for (value in variable) {
		line = filter(dataFrame, dataFrame[,"Variable"] == value)
		tableFrame[rowCount,] = c(paste0(line[1,1]," (",line[1,3],")"), "V1", line[1,4], line[1,5], line[1,11], line[1,12], "", "")
		rowCount = rowCount + 1
		tableFrame[rowCount,] = c("", "V7", line[1,6], line[1,7], line[1,13], line[1,14], "", "")
		rowCount = rowCount + 1	
		#if (line[1,18] <= 0.05) {line[1,18] = paste0(line[1,18],"******")}
		#if (line[1,24] <= 0.05) {line[1,24] = paste0(line[1,24],"******")}
		tableFrame[rowCount,] = c("", "Delta", line[1,8], line[1,9], line[1,15], line[1,16], line[1,18], line[1,24])  # last two are p-values
		rowCount = rowCount + 1
		#if (line[1,10] <= 0.05) {line[1,10] = paste0(line[1,10],"******")}
		#if (line[1,17] <= 0.05) {line[1,17] = paste0(line[1,17],"******")}
		tableFrame[rowCount,] = c("", "p_value", line[1,10], "", line[1,17], "", "", "") # p-values
		rowCount = rowCount + 1
		#if (line[1,22] <= 0.05) {line[1,22] = paste0(line[1,22],"******")}
		#sif (line[1,23] <= 0.05) {line[1,23] = paste0(line[1,23],"******")}
		tableFrame[rowCount,] = c("", "p_adj", line[1,22], "", line[1,23], "", "", "") # adjusted p-values
		rowCount = rowCount + 1
	}
	tableFrame[,3] = as.numeric(tableFrame[,3])
	tableFrame[,4] = as.numeric(tableFrame[,4])
	tableFrame[,5] = as.numeric(tableFrame[,5])
	tableFrame[,6] = as.numeric(tableFrame[,6])
	tableFrame[,7] = as.numeric(tableFrame[,7])
	tableFrame[,8] = as.numeric(tableFrame[,8])
	return(tableFrame)
}

# Version of the function above which produces rounded values with 3 decimals:
FormattedTableRoundedWithAdjPValue = function (dataFrame) {
	dataFrame[,4] = as.numeric(dataFrame[,4])
	dataFrame[,5] = as.numeric(dataFrame[,5])
	dataFrame[,6] = as.numeric(dataFrame[,6])
	dataFrame[,7] = as.numeric(dataFrame[,7])
	dataFrame[,8] = as.numeric(dataFrame[,8])
	dataFrame[,9] = as.numeric(dataFrame[,9])
	dataFrame[,10] = as.numeric(dataFrame[,10])
	dataFrame[,11] = as.numeric(dataFrame[,11])
	dataFrame[,12] = as.numeric(dataFrame[,12])
	dataFrame[,13] = as.numeric(dataFrame[,13])
	dataFrame[,14] = as.numeric(dataFrame[,14])
	dataFrame[,15] = as.numeric(dataFrame[,15])
	dataFrame[,16] = as.numeric(dataFrame[,16])
	dataFrame[,17] = as.numeric(dataFrame[,17])
	dataFrame[,18] = as.numeric(dataFrame[,18])
	dataFrame[,22] = as.numeric(dataFrame[,22])
	dataFrame[,23] = as.numeric(dataFrame[,23])
	dataFrame[,24] = as.numeric(dataFrame[,24])
	dataFrame <- data.frame(lapply(dataFrame, function(x) if(is.numeric(x)) round(x, 3) else x))
	variable = dataFrame[,"Variable"]
	tableFrame = data.frame("Test" = "", "Visit" = "", "Placebo_sd" = "", "NR_sd" = "", "p_value" = "", "p_adj" = "")
	rowCount = 1
	for (value in variable) {
		line = filter(dataFrame, dataFrame[,"Variable"] == value)	
		line[1,4] = round(line[1,4], 3)
		line[1,5] = round(line[1,5], 3)
		line[1,11] = round(line[1,11], 3)
		line[1,12] = round(line[1,12], 3)
		line[1,4] = round(line[1,4], 3)
		line[1,6] = round(line[1,6], 3)
		line[1,7] = round(line[1,7], 3)
		line[1,13] = round(line[1,13], 3)
		line[1,14] = round(line[1,14], 3)
		line[1,8] = round(line[1,8], 3)
		line[1,9] = round(line[1,9], 3)
		line[1,15] = round(line[1,15], 3)
		line[1,16] = round(line[1,16], 3)
		line[1,18] = round(line[1,18], 3)
		line[1,24] = round(line[1,24], 3)
		line[1,10] = round(line[1,10], 3)
		line[1,17] = round(line[1,17], 3)
		line[1,22] = round(line[1,22], 3)
		line[1,23] = round(line[1,23], 3)
		if (line[1,18] < 0.001 & !is.na(line[1,18])) {line[1,18] = "<0.001"}
		if (line[1,24] < 0.001 & !is.na(line[1,24])) {line[1,24] = "<0.001"}
		if (line[1,10] < 0.001 & !is.na(line[1,10])) {line[1,10] = "<0.001"}
		if (line[1,17] < 0.001 & !is.na(line[1,17])) {line[1,17] = "<0.001"}
		if (line[1,22] < 0.001 & !is.na(line[1,22])) {line[1,22] = "<0.001"}
		if (line[1,23] < 0.001 & !is.na(line[1,23])) {line[1,23] = "<0.001"}
		tableFrame[rowCount,] = c(paste0(line[1,1]," (",line[1,3],")"), 	"V1", 			paste0(line[1,4]," +- ",line[1,5]), 	paste0(line[1,11]," +- ",line[1,12]), 	"",				"")
		rowCount = rowCount + 1
		tableFrame[rowCount,] = c("", 										"V7", 			paste0(line[1,6]," +- ",line[1,7]), 	paste0(line[1,13]," +- ",line[1,14]), 	"", 			"")
		rowCount = rowCount + 1	
		tableFrame[rowCount,] = c("", 										"Delta", 		paste0(line[1,8]," +- ",line[1,9]), 	paste0(line[1,15]," +- ",line[1,16]), 	line[1,18], 	line[1,24]) 
		rowCount = rowCount + 1
		tableFrame[rowCount,] = c("", 										"p_value", 		line[1,10], 							line[1,17], 							"", 			"") # p-values
		rowCount = rowCount + 1
		tableFrame[rowCount,] = c("", 										"p_adj", 	line[1,22], 							line[1,23], 							"", 			"") # adjusted p-values
		rowCount = rowCount + 1
	}
	return(tableFrame)
}

# This function is formatted for the LCMS metabolomics analyses: 
FormattedTableOmics = function (dataFrame) {
	variable = dataFrame[,"Variable"]
	tableFrame = data.frame("Test" = "", "Visit" = "", "PL_mean" = "", "Sd_PL" ="", "NR_mean" = "", "Sd_NR" = "", "p_value" = "", "p_adj" = "")
	rowCount = 1
	for (value in variable) {
		line = filter(dataFrame, dataFrame[,"Variable"] == value)
		tableFrame[rowCount,] = c(paste0(line[1,1]," (",line[1,3],")"), "V1", line[1,4], line[1,5], line[1,11], line[1,12], "", "")
		rowCount = rowCount + 1
		tableFrame[rowCount,] = c("", "V7", line[1,6], line[1,7], line[1,13], line[1,14], "", "")
		rowCount = rowCount + 1	
		#if (line[1,18] <= 0.05) {line[1,18] = paste0(line[1,18],"******")}
		#if (line[1,26] <= 0.05) {line[1,26] = paste0(line[1,26],"******")}
		tableFrame[rowCount,] = c("", "Delta", line[1,8], line[1,9], line[1,15], line[1,16], line[1,18], line[1,26])  # last two are p-values
		rowCount = rowCount + 1
		#if (line[1,10] <= 0.05) {line[1,10] = paste0(line[1,10],"******")}
		#if (line[1,17] <= 0.05) {line[1,17] = paste0(line[1,17],"******")}
		tableFrame[rowCount,] = c("", "p_value", line[1,10], "", line[1,17], "", "", "") # p-values
		rowCount = rowCount + 1
		#if (line[1,24] <= 0.05) {line[1,24] = paste0(line[1,24],"******")}
		#if (line[1,25] <= 0.05) {line[1,25] = paste0(line[1,25],"******")}
		tableFrame[rowCount,] = c("", "p_adj", line[1,24], "", line[1,25], "", "", "") # adjusted p-values
		rowCount = rowCount + 1
	}
	tableFrame[,3] = as.numeric(tableFrame[,3])
	tableFrame[,4] = as.numeric(tableFrame[,4])
	tableFrame[,5] = as.numeric(tableFrame[,5])
	tableFrame[,6] = as.numeric(tableFrame[,6])
	tableFrame[,7] = as.numeric(tableFrame[,7])
	tableFrame[,8] = as.numeric(tableFrame[,8])
	return(tableFrame)
}

# This is the same as above but filters out values where more than 3 values are below the limit of detection (LOD)
FormattedTableOmicsFilterLODs = function (dataFrame) {
	variable = dataFrame[,"Variable"]
	tableFrame = data.frame("Test" = "", "Visit" = "", "PL_mean" = "", "Sd_PL" ="", "NR_mean" = "", "Sd_NR" = "", "p_value" = "", "p_adj" = "", "Statistical_test" = "")
	rowCount = 1
	for (value in variable) {
		line = filter(dataFrame, dataFrame[,"Variable"] == value)
		if (line[1,20] >= 7 & line[1,21] >= 7) {
			tableFrame[rowCount,] = c(paste0(line[1,1]," (",line[1,3],")"), "V1", line[1,4], line[1,5], line[1,11], line[1,12], "", "", line[1,27])
			rowCount = rowCount + 1
			tableFrame[rowCount,] = c("", "V7", line[1,6], line[1,7], line[1,13], line[1,14], "", "", "")
			rowCount = rowCount + 1	
			tableFrame[rowCount,] = c("", "Delta", line[1,8], line[1,9], line[1,15], line[1,16], line[1,18], line[1,26], "")  # last two are p-values
			rowCount = rowCount + 1
			tableFrame[rowCount,] = c("", "p_value", line[1,10], "", line[1,17], "", "", "", "") # p-values
			rowCount = rowCount + 1
			tableFrame[rowCount,] = c("", "p_adj", line[1,24], "", line[1,25], "", "", "", "") # adjusted p-values
			rowCount = rowCount + 1
		}
		if (line[1,20] >= 7 & line[1,21] < 7) {
			tableFrame[rowCount,] = c(paste0(line[1,1]," (",line[1,3],")"), "V1", line[1,4], line[1,5], NA, NA, "", "", line[1,27])
			rowCount = rowCount + 1
			tableFrame[rowCount,] = c("", "V7", line[1,6], line[1,7], NA, NA, "", "", "")
			rowCount = rowCount + 1	
			tableFrame[rowCount,] = c("", "Delta", line[1,8], line[1,9], NA, NA, NA, NA, NA)  # last two are p-values
			rowCount = rowCount + 1
			#if (line[1,10] <= 0.05) {line[1,10] = paste0(line[1,10],"******")}
			tableFrame[rowCount,] = c("", "p_value", line[1,10], "", NA, "", "", "", "") # p-values
			rowCount = rowCount + 1
			#if (line[1,24] <= 0.05) {line[1,24] = paste0(line[1,24],"******")}
			tableFrame[rowCount,] = c("", "p_adj", line[1,24], "", NA, "", "", "", "") # adjusted p-values
			rowCount = rowCount + 1
		}
		if (line[1,20] < 7 & line[1,21] >= 7) {
			tableFrame[rowCount,] = c(paste0(line[1,1]," (",line[1,3],")"), "V1", NA, NA, line[1,11], line[1,12], "", "", line[1,27])
			rowCount = rowCount + 1
			tableFrame[rowCount,] = c("", "V7", NA, NA, line[1,13], line[1,14], "", "", "")
			rowCount = rowCount + 1	
			tableFrame[rowCount,] = c("", "Delta", NA, NA, line[1,15], line[1,16], NA, NA, "")  # last two are p-values
			rowCount = rowCount + 1
			#if (line[1,17] <= 0.05) {line[1,17] = paste0(line[1,17],"******")}
			tableFrame[rowCount,] = c("", "p_value", NA, "", line[1,17], "", "", "", "") # p-values
			rowCount = rowCount + 1
			#if (line[1,25] <= 0.05) {line[1,25] = paste0(line[1,25],"******")}
			tableFrame[rowCount,] = c("", "p_adj", NA, "", line[1,25], "", "", "", "") # adjusted p-values
			rowCount = rowCount + 1
		}
		if (line[1,20] < 7 & line[1,21] < 7) {
			tableFrame[rowCount,] = c(paste0(line[1,1]," (",line[1,3],")"), "Both samples below LOD threshold", NA, NA, NA, NA, "", "", "")
			rowCount = rowCount + 1 
		}

	}
	tableFrame[,3] = as.numeric(tableFrame[,3])
	tableFrame[,4] = as.numeric(tableFrame[,4])
	tableFrame[,5] = as.numeric(tableFrame[,5])
	tableFrame[,6] = as.numeric(tableFrame[,6])
	tableFrame[,7] = as.numeric(tableFrame[,7])
	tableFrame[,8] = as.numeric(tableFrame[,8])
	return(tableFrame)
}

# This is the same function as the one abov but rounds numbers to 3 decimals:
FormattedTableOmicsFilterLODsRounded = function (dataFrame) {
	dataFrame[,4] = as.numeric(dataFrame[,4])
	dataFrame[,5] = as.numeric(dataFrame[,5])
	dataFrame[,6] = as.numeric(dataFrame[,6])
	dataFrame[,7] = as.numeric(dataFrame[,7])
	dataFrame[,8] = as.numeric(dataFrame[,8])
	dataFrame[,9] = as.numeric(dataFrame[,9])
	dataFrame[,10] = as.numeric(dataFrame[,10])
	dataFrame[,11] = as.numeric(dataFrame[,11])
	dataFrame[,12] = as.numeric(dataFrame[,12])
	dataFrame[,13] = as.numeric(dataFrame[,13])
	dataFrame[,14] = as.numeric(dataFrame[,14])
	dataFrame[,15] = as.numeric(dataFrame[,15])
	dataFrame[,16] = as.numeric(dataFrame[,16])
	dataFrame[,17] = as.numeric(dataFrame[,17])
	dataFrame[,18] = as.numeric(dataFrame[,18])
	dataFrame[,24] = as.numeric(dataFrame[,24])
	dataFrame[,25] = as.numeric(dataFrame[,25])
	dataFrame[,26] = as.numeric(dataFrame[,26])
	variable = dataFrame[,"Variable"]
	tableFrame = data.frame("Test" = "", "Visit" = "", "Placebo_sd" = "", "NR_sd" = "", "p_value" = "", "p_adj" = "", "Statistical_test" = "")
	rowCount = 1
	for (value in variable) {
		line = filter(dataFrame, dataFrame[,"Variable"] == value)
		line[1,4] = round(line[1,4], 3)
		line[1,5] = round(line[1,5], 3)
		line[1,6] = round(line[1,6], 3)
		line[1,7] = round(line[1,7], 3)
		line[1,8] = round(line[1,8], 3)
		line[1,9] = round(line[1,9], 3)
		line[1,10] = round(line[1,10], 3)
		line[1,11] = round(line[1,11], 3)
		line[1,12] = round(line[1,12], 3)
		line[1,13] = round(line[1,13], 3)
		line[1,14] = round(line[1,14], 3)
		line[1,15] = round(line[1,15], 3)
		line[1,16] = round(line[1,16], 3)
		line[1,17] = round(line[1,17], 3)
		line[1,18] = round(line[1,18], 3)
		line[1,24] = round(line[1,24], 3)
		line[1,25] = round(line[1,25], 3)
		line[1,26] = round(line[1,26], 3)
		if (line[1,10] < 0.001 & !is.na(line[1,10])) {line[1,10] = "<0.001"}
		if (line[1,17] < 0.001 & !is.na(line[1,17])) {line[1,17] = "<0.001"}
		if (line[1,24] < 0.001 & !is.na(line[1,24])) {line[1,24] = "<0.001"}
		if (line[1,25] < 0.001 & !is.na(line[1,25])) {line[1,25] = "<0.001"}
		if (line[1,18] < 0.001 & !is.na(line[1,18])) {line[1,18] = "<0.001"}
		if (line[1,26] < 0.001 & !is.na(line[1,26])) {line[1,26] = "<0.001"}
		if (line[1,20] >= 7 & line[1,21] >= 7) {
			tableFrame[rowCount,] = c(paste0(line[1,1]," (",line[1,3],")"), 	"V1", 			paste0(line[1,4]," +- ",line[1,5]), 	paste0(line[1,11]," +- ",line[1,12]), 	"", 		"", line[1,27])
			rowCount = rowCount + 1
			tableFrame[rowCount,] = c("", 										"V7", 			paste0(line[1,6]," +- ",line[1,7]), 	paste0(line[1,13]," +- ",line[1,14]), 	"", 		"", "")
			rowCount = rowCount + 1	
			tableFrame[rowCount,] = c("", 										"Delta", 		paste0(line[1,8]," +- ",line[1,9]), 	paste0(line[1,15]," +- ",line[1,16]), 	line[1,18], line[1,26], "")  # last two are p-values
			rowCount = rowCount + 1
			tableFrame[rowCount,] = c("", 										"p_value", 		line[1,10], 							line[1,17], 							"", 		"", "") # p-values
			rowCount = rowCount + 1
			tableFrame[rowCount,] = c("", 										"p_adj", 	line[1,24], 							line[1,25], 							"", 		"", "") # adjusted p-values
			rowCount = rowCount + 1
		}
		if (line[1,20] >= 7 & line[1,21] < 7) {
			tableFrame[rowCount,] = c(paste0(line[1,1]," (",line[1,3],")"), 	"V1", 			paste0(line[1,4]," +- ",line[1,5]),		NA, 									NA, 		"", line[1,27])
			rowCount = rowCount + 1
			tableFrame[rowCount,] = c("", 										"V7", 			paste0(line[1,6]," +- ",line[1,7]), 	NA, 									NA, 		"", "")
			rowCount = rowCount + 1	
			tableFrame[rowCount,] = c("", 										"Delta", 		paste0(line[1,8]," +- ",line[1,9]), 	NA, 									NA, 		NA, "")  # last two are p-values
			rowCount = rowCount + 1
			tableFrame[rowCount,] = c("", 										"p_value", 		line[1,10], 							"", 									NA, 		"", "") # p-values
			rowCount = rowCount + 1
			tableFrame[rowCount,] = c("", 										"p_adj", 	line[1,24], 							"", 									NA, 		"", "") # adjusted p-values
			rowCount = rowCount + 1
		}
		if (line[1,20] < 7 & line[1,21] >= 7) {
			tableFrame[rowCount,] = c(paste0(line[1,1]," (",line[1,3],")"), 	"V1", 			NA, 									paste0(line[1,11]," +- ",line[1,12]), 	"", 		"", "")
			rowCount = rowCount + 1
			tableFrame[rowCount,] = c("", 										"V7",			NA, 									paste0(line[1,13]," +- ",line[1,14]), 	"", 		"", "")
			rowCount = rowCount + 1	
			tableFrame[rowCount,] = c("", 										"Delta",		NA, 									paste0(line[1,15]," +- ",line[1,16]), 	NA, 		NA, "")  # last two are p-values
			rowCount = rowCount + 1
			tableFrame[rowCount,] = c("", 										"p_value", 		NA, 									line[1,17], 							"", 		"", "") # p-values
			rowCount = rowCount + 1
			tableFrame[rowCount,] = c("",										"p_adj", 		NA, 									line[1,25], 							"", 		"", "") # adjusted p-values
			rowCount = rowCount + 1
		}
		if (line[1,20] < 7 & line[1,21] < 7) {
			tableFrame[rowCount,] = c(paste0(line[1,1]," (",line[1,3],")"), 	"Both samples below LOD threshold", 		NA, NA, NA, NA, "")
			rowCount = rowCount + 1 
		}

	}
	return(tableFrame)
}


# Formats baseline comparison output to table: 
FormattedBaselineComparisons = function (dataFrame) {
	dataFrame[,4] = as.numeric(dataFrame[,4])
	dataFrame[,5] = as.numeric(dataFrame[,5])
	dataFrame[,6] = as.numeric(dataFrame[,6])
	dataFrame[,7] = as.numeric(dataFrame[,7])
	dataFrame[,8] = as.numeric(dataFrame[,8])
	dataFrame[,13] = as.numeric(dataFrame[,13])
	dataFrame <- data.frame(lapply(dataFrame, function(x) if(is.numeric(x)) round(x, 3) else x))
	variable = dataFrame[,"Variable"]
	tableFrame = data.frame("Name" = "", "Placebo_sd" = "", "NR_sd" = "", "p_value" = "", "p_adj" = "")
	rowCount = 1
	for (value in variable) {
		line = filter(dataFrame, dataFrame[,"Variable"] == value)
		tableFrame[rowCount,] = c(paste0(line[1,2]," (",line[1,3],")"), paste0(line[1,4]," +- ",line[1,5]), paste0(line[1,6]," +- ",line[1,7]), line[1,8], line[1,13])
		rowCount = rowCount + 1
	}
	return(tableFrame)
}

# Formats and round demographics output data frame to 3 decimals:
FormattedTableRoundedDemographics = function (dataFrame) {
	SexRow = dataFrame[1,]
	SexRow = SexRow[,c("Name", "PL_BL_Mean", "NR_BL_Mean", "p-value")]
	colnames(SexRow) = c("Name", "Placebo_sd", "NR_sd", "p_value")
	dataFrame = dataFrame[-1,]
	dataFrame[,2] = as.numeric(dataFrame[,2])
	dataFrame[,3] = as.numeric(dataFrame[,3])
	dataFrame[,4] = as.numeric(dataFrame[,4])
	dataFrame[,5] = as.numeric(dataFrame[,5])
	dataFrame[,6] = as.numeric(dataFrame[,6])
	dataFrame <- data.frame(lapply(dataFrame, function(x) if(is.numeric(x)) round(x, 2) else x))
	variable = dataFrame[,"Name"]
	tableFrame = data.frame("Name" = "", "Placebo_sd" = "", "NR_sd" = "", "p_value" = "")
	rowCount = 1
	for (value in variable) {
		line = filter(dataFrame, dataFrame[,"Name"] == value)
		tableFrame[rowCount,] = c(line[1,1], paste0(line[1,2]," +- ",line[1,3]), paste0(line[1,4]," +- " ,line[1,5]), line[1,6])
		rowCount = rowCount + 1
	}
	tableFrame = rbind(SexRow, tableFrame)
	return(tableFrame)
}


# Graph functions #

# sorts numbers to significance by star system. This will be added to graphs later:  
starSignif = function (p_val) {
	p_val = as.numeric(unlist(p_val))
	if (p_val < 0.0005) {
		p_val = "***"
	}
	if (p_val < 0.05 & p_val >= 0.005) {
		p_val = "*"
	}	
	if (p_val < 0.005 & p_val >= 0.0005) {
		p_val = "**"
	}
	if (p_val >= 0.05) {
		p_val = ""
	}
	return(p_val)
}

# Formats Y axis to e^10 format: 
formatYaxis <- function(x) {     # Create user-defined function
	x <- str_split_fixed(formatC(x, format = "e"), "e", 2)
	alpha <- as.numeric(x[ , 1])
	power <- as.integer(x[ , 2])
	paste(alpha * 10, power - 1L, sep = "e")
}

# Function to print graphs to powerpoint for manual formatting, HEIGHT and WIDTH is in inches: 
print_to_pdf_vector_sizeOption = function (graph, HEIGHT, WIDTH) {
	editable_graph <- dml(ggobj = graph)
	doc <- add_slide(doc, layout = "Title and Content", master = "Office Theme")
	doc <- ph_with(x = doc, editable_graph, location = ph_location(height = HEIGHT, width = WIDTH))
}

# Makes paired plots of all individuals at V1 and V7. "adjusted" argument is either TRUE for adjusted p-value or FALSE for unadjusted p-value. "scientificNotation" is either TRUE for numbers in scientific notation (i.e: e^x format) or FALSE for number written out with multiple decimals. 
makePairedPlotNoFacets = function (variable, dataFrame, testMethod, list, corr_frame, adjusted, scientificNotation) {
	colnames(list) = list[2,]	
	Analysed_samples_placebo = 10
	Analysed_samples_nr = 10
	testFrame = filter(dataFrame, dataFrame[,"ActivityId"] == "V02_1" | dataFrame[,"ActivityId"] == "V09_1")
	if (any(is.na(testFrame[,variable]))) {
		missingValues = filter(testFrame, (is.na(testFrame[,variable]) == TRUE) & (testFrame[,"ActivityId"] == "V02_1" | testFrame[,"ActivityId"] == "V09_1"))
		missingSubjects = missingValues[,"SubjectId"]		
		listMissingSubjects = unlist(missingSubjects)
		placeboFrame = filter(testFrame, testFrame[,"Randomisation"] == "Placebo")
		nrFrame = filter(testFrame, testFrame[,"Randomisation"] == "NR")
		Analysed_samples_placebo = Analysed_samples_placebo - length(unlist(unique(placeboFrame[is.na(placeboFrame[,variable]), "SubjectId"])))
		Analysed_samples_nr = Analysed_samples_nr - length(unlist(unique(nrFrame[is.na(nrFrame[,variable]), "SubjectId"])))
		rows = length(listMissingSubjects)
		for (i in 1:rows) {
				dataFrame = filter(dataFrame, dataFrame[,"SubjectId"] != listMissingSubjects[i])		
		}
	}

	dataFrame[dataFrame == "V02_1"] <- "BL"
	dataFrame[dataFrame == "V09_1"] <- "V09"
	significanceNR = ""
	significancePL = ""
	if (adjusted == TRUE) {
		variableLine = filter(corr_frame, corr_frame["Name"] == variable)
		significancePlacebo = variableLine[1,22]
		significanceNR = variableLine[1,23]
		significancePlacebo = starSignif(significancePlacebo)
		significanceNR = starSignif(significanceNR)
	}
	if (adjusted == FALSE) {
		variableLine = filter(corr_frame, corr_frame["Name"] == variable)
		significancePlacebo = variableLine[1,10]
		significanceNR = variableLine[1,17]
		significancePlacebo = starSignif(significancePlacebo)
		significanceNR = starSignif(significanceNR)
	}
	pl_df = filter(dataFrame, (dataFrame[,"ActivityId"] != "Delta") & (dataFrame[,"Randomisation"] == "Placebo") & ((dataFrame[,"ActivityId"] == "BL") | (dataFrame[,"ActivityId"] == "V09")))
	pl_df$Event = paste0("PL_",pl_df$ActivityId)
	max_BL = filter(pl_df, pl_df[,"ActivityId"] == "BL")
	max_v09 = filter(pl_df, pl_df[,"ActivityId"] == "V09")
	max_pl = c((max(as.numeric(unlist(max_BL[,variable]))) * 1.02), (max(as.numeric(unlist((max_v09[,variable])))) * 1.02))
	nr_df = filter(dataFrame, (dataFrame[,"ActivityId"] != "Delta") & (dataFrame[,"Randomisation"] == "NR") & ((dataFrame[,"ActivityId"] == "BL") | (dataFrame[,"ActivityId"] == "V09")))
	nr_df$Event = paste0("NR_",nr_df$ActivityId)
	max_BL = filter(nr_df, nr_df[,"ActivityId"] == "BL")
	max_v09 = filter(nr_df, nr_df[,"ActivityId"] == "V09")
	max_nr = c((max(as.numeric(unlist(max_BL[,variable]))) * 1.02), (max(as.numeric(unlist((max_v09[,variable])))) * 1.02))
	max_both = c(max_pl, max_nr)
	y_pos_p_value_plus_02 = max(max_both)
	max_value = y_pos_p_value_plus_02 * 1.1
	both_df = rbind(nr_df, pl_df)
	both_df[,variable] = unlist(as.numeric(both_df[,variable]))
	colnames(list) = list[2,]
	b = ggplot(both_df, aes(x = factor(Event, levels= c("PL_BL", "PL_V09", "NR_BL", "NR_V09")), y = unlist(as.numeric(both_df[,variable]))), colour = Randomisation) + theme_classic() 
	b = b + geom_line(aes(group=SubjectId, colour = Randomisation), size = sizeOfLines) + geom_point(size = sizeOfPoints, aes(colour = Randomisation))
	b = b + labs(y = list[3,variable], x = "", title = variable) + theme(plot.title = element_text(hjust = 0.5, color="black"))
	b = b + theme(plot.title = element_text(hjust = 0.5, size = sizeOfLabels), axis.text.x = element_text(size = sizeOfLabels, color="black"), axis.text.y = element_text(size = sizeOfLabels, color="black")) + theme(axis.title = element_text(size = sizeOfLabels, color="black"))
	b = b + theme(strip.text.x = element_text(size = sizeOfLabels)) + theme(axis.ticks = element_line(color="black", size = sizeOfAxisLines)) + theme(axis.line = element_line(color = "black", size = sizeOfAxisLines))
	if(scientificNotation == TRUE) {
		b = b + scale_y_continuous(expand=c(0, 0), limits=c(0, max_value), label = formatYaxis)
	}
	if(scientificNotation != TRUE) {
		b = b + scale_y_continuous(expand=c(0, 0), limits=c(0, max_value))
	}
	b = b + annotate("text", x = "NR_BL", y = y_pos_p_value_plus_02, label = significanceNR, size = sizeOfLabels, color="black")
	b = b + annotate("text", x = "PL_BL", y = y_pos_p_value_plus_02, label = significancePL, size = sizeOfLabels, color="black")
	b = b + theme(legend.position = "none")	
	b = b + scale_color_manual(values=c(color_NR, color_placebo))	
	
	return(b)
}

# Makes paired plots of all individuals from the LCMS metabolomics at V1 and V7. Argument "levelOfLOD" is numeric and indicates the minimum level of values that can be below level of detection (LOD) in each group to be analyzed. The other arguments are the same as above: 
makePairedPlotLODSremovedNoFacets = function (variable, dataFrame, testMethod, list, corr_frame, adjusted, levelOfLOD, scientificNotation) {
	colnames(list) = list[2,]	
	
	Analysed_samples_placebo = 10
	Analysed_samples_nr = 10

	testFrame = filter(dataFrame, dataFrame[,"ActivityId"] == "V02_1" | dataFrame[,"ActivityId"] == "V09_1")
	if (any(is.na(testFrame[,variable]))) {
		missingValues = filter(testFrame, (is.na(testFrame[,variable]) == TRUE) & (testFrame[,"ActivityId"] == "V02_1" | testFrame[,"ActivityId"] == "V09_1"))
		missingSubjects = missingValues[,"SubjectId"]		
		listMissingSubjects = unlist(missingSubjects)
		placeboFrame = filter(testFrame, testFrame[,"Randomisation"] == "Placebo")
		nrFrame = filter(testFrame, testFrame[,"Randomisation"] == "NR")
		Analysed_samples_placebo = Analysed_samples_placebo - length(unlist(unique(placeboFrame[is.na(placeboFrame[,variable]), "SubjectId"])))
		Analysed_samples_nr = Analysed_samples_nr - length(unlist(unique(nrFrame[is.na(nrFrame[,variable]), "SubjectId"])))
		rows = length(listMissingSubjects)
		for (i in 1:rows) {
				dataFrame = filter(dataFrame, dataFrame[,"SubjectId"] != listMissingSubjects[i])		
		}
	}

	Plvalues = filter(dataFrame, dataFrame[,"Randomisation"] == "Placebo" & dataFrame[,"ActivityId"] != "Delta")
	NRvalues = filter(dataFrame, dataFrame[,"Randomisation"] == "NR" & dataFrame[,"ActivityId"] != "Delta")

	LODthreshold = as.numeric(unlist(list[6,variable]))
	howManyPLbelowLOD = 0
	if (any(as.numeric(unlist(Plvalues[,variable])) < LODthreshold)) {
		belowLODPL = which(as.numeric(unlist(Plvalues[,variable])) < LODthreshold) 
		howManyPLbelowLOD = length(belowLODPL)
		subIdBelowLODPL = Plvalues[belowLODPL,"SubjectId"]
		subIdBelowLODPL = unique(subIdBelowLODPL)
		for (i in subIdBelowLODPL) {
			dataFrame = filter(dataFrame, dataFrame[,"SubjectId"] != i)
		}
		Analysed_samples_placebo = Analysed_samples_placebo - length(subIdBelowLODPL)
	}
	howManyNRbelowLOD = 0 
	if (any(as.numeric(unlist(NRvalues[,variable])) < LODthreshold)) {
		belowLODNR = which(as.numeric(unlist(NRvalues[,variable])) < LODthreshold) 
		howManyNRbelowLOD = length(belowLODNR)
		subIdBelowLODNR = NRvalues[belowLODNR,"SubjectId"]
		subIdBelowLODNR = unique(subIdBelowLODNR)
		for (i in subIdBelowLODNR) {
			dataFrame = filter(dataFrame, dataFrame[,"SubjectId"] != i)
		}
		Analysed_samples_nr = Analysed_samples_nr - length(subIdBelowLODNR)
	}

	dataFrame[dataFrame == "V02_1"] <- "BL"
	dataFrame[dataFrame == "V09_1"] <- "V09"

	significanceNR = ""
	significancePL = ""
	if (Analysed_samples_placebo >= levelOfLOD & Analysed_samples_nr >= levelOfLOD) { 
		if (adjusted == TRUE) {
			variableLine = filter(corr_frame, corr_frame["Name"] == variable)
			significancePlacebo = variableLine[1,24]
			significanceNR = variableLine[1,25]
			significancePlacebo = starSignif(significancePlacebo)
			significanceNR = starSignif(significanceNR)
		
		}
		if (adjusted == FALSE) {
			variableLine = filter(corr_frame, corr_frame["Name"] == variable)
			significancePlacebo = variableLine[1,10]
			significanceNR = variableLine[1,17]
			significancePlacebo = starSignif(significancePlacebo)
			significanceNR = starSignif(significanceNR)
		}
	
		pl_df = filter(dataFrame, (dataFrame[,"ActivityId"] != "Delta") & (dataFrame[,"Randomisation"] == "Placebo") & ((dataFrame[,"ActivityId"] == "BL") | (dataFrame[,"ActivityId"] == "V09")))
		pl_df$Event = paste0("PL_",pl_df$ActivityId)
		max_BL = filter(pl_df, pl_df[,"ActivityId"] == "BL")
		max_v09 = filter(pl_df, pl_df[,"ActivityId"] == "V09")
		max_pl = c((max(as.numeric(unlist(max_BL[,variable]))) * 1.02), (max(as.numeric(unlist((max_v09[,variable])))) * 1.02))

		nr_df = filter(dataFrame, (dataFrame[,"ActivityId"] != "Delta") & (dataFrame[,"Randomisation"] == "NR") & ((dataFrame[,"ActivityId"] == "BL") | (dataFrame[,"ActivityId"] == "V09")))
		nr_df$Event = paste0("NR_",nr_df$ActivityId)
		max_BL = filter(nr_df, nr_df[,"ActivityId"] == "BL")
		max_v09 = filter(nr_df, nr_df[,"ActivityId"] == "V09")
		max_nr = c((max(as.numeric(unlist(max_BL[,variable]))) * 1.02), (max(as.numeric(unlist((max_v09[,variable])))) * 1.02))

		max_both = c(max_pl, max_nr)
		y_pos_p_value_plus_02 = max(max_both)
		max_value = y_pos_p_value_plus_02 * 1.1
				
		both_df = rbind(nr_df, pl_df)
		both_df[,variable] = unlist(as.numeric(both_df[,variable]))
		colnames(list) = list[2,]
		
		b = ggplot(both_df, aes(x = factor(Event, levels= c("PL_BL", "PL_V09", "NR_BL", "NR_V09")), y = unlist(as.numeric(both_df[,variable]))), colour = Randomisation) + theme_classic() 
		b = b + geom_line(aes(group=SubjectId, colour = Randomisation), size = sizeOfLines) + geom_point(size = sizeOfPoints, aes(colour = Randomisation))
		b = b + labs(y = list[3,variable], x = "", title = paste0(variable," (NR, n=",Analysed_samples_nr,", PL, n=",Analysed_samples_placebo,")")) + theme(plot.title = element_text(hjust = 0.5, color="black"))
		b = b + theme(plot.title = element_text(hjust = 0.5, size = sizeOfLabels), axis.text.x = element_text(color="black", size = sizeOfLabels), axis.text.y = element_text(color="black", size = sizeOfLabels)) + theme(axis.title = element_text(color="black", size = sizeOfLabels))
		b = b + theme(strip.text.x = element_text(size = sizeOfLabels))
		if(scientificNotation == TRUE) {
			b = b + scale_y_continuous(expand=c(0, 0), limits=c(0, max_value), label = formatYaxis)
		}
		if(scientificNotation != TRUE) {
			b = b + scale_y_continuous(expand=c(0, 0), limits=c(0, max_value))
		}
		b = b + annotate("text", x = "NR_BL", y = y_pos_p_value_plus_02, label = significanceNR, size = sizeOfLabels, color="black")
		b = b + annotate("text", x = "PL_BL", y = y_pos_p_value_plus_02, label = significancePL, size = sizeOfLabels, color="black")
		b = b + theme(legend.position = "none")	
		b = b + scale_color_manual(values=c(color_NR, color_placebo)) + theme(axis.ticks = element_line(color="black", size = sizeOfAxisLines)) + theme(axis.line = element_line(color = "black", size = sizeOfAxisLines))
		
		return(b)
	}
	if (Analysed_samples_placebo >= levelOfLOD & Analysed_samples_nr < levelOfLOD) { 
		if (adjusted == TRUE) {
			variableLine = filter(corr_frame, corr_frame["Name"] == variable)
			significancePlacebo = variableLine[1,24]
			significancePlacebo = starSignif(significancePlacebo)
		}
		if (adjusted == FALSE) {
			variableLine = filter(corr_frame, corr_frame["Name"] == variable)
			significancePlacebo = variableLine[1,10]
			significancePlacebo = starSignif(significancePlacebo)
		}	
		pl_df = filter(dataFrame, (dataFrame[,"ActivityId"] != "Delta") & (dataFrame[,"Randomisation"] == "Placebo") & ((dataFrame[,"ActivityId"] == "BL") | (dataFrame[,"ActivityId"] == "V09")))
		pl_df$Event = paste0("PL_",pl_df$ActivityId)
		max_BL = filter(pl_df, pl_df[,"ActivityId"] == "BL")
		max_v09 = filter(pl_df, pl_df[,"ActivityId"] == "V09")
		max_pl = c((max(as.numeric(unlist(max_BL[,variable]))) * 1.02), (max(as.numeric(unlist((max_v09[,variable])))) * 1.02))
		max_both = max_pl
		y_pos_p_value_plus_02 = max(max_both)
		max_value = y_pos_p_value_plus_02 * 1.1

		print(paste0("Variable: ",variable))
		print(paste0("NR group has less than ",levelOfLOD," individuals with LOD above detection limit or multiple NA values."))
		print(paste0("Analysed samples Placebo: ",Analysed_samples_placebo))
		print(paste0("Analysed samples NR: ",Analysed_samples_nr))		
	
		b = ggplot(both_df, aes(x = factor(Event, levels= c("PL_BL", "PL_V09")), y = unlist(as.numeric(both_df[,variable]))), colour = Randomisation) + theme_classic() 
		b = b + geom_line(aes(group=SubjectId, colour = Randomisation), size = sizeOfLines) + geom_point(size = sizeOfPoints, aes(colour = Randomisation))
		b = b + labs(y = list[3,variable], x = "", title = paste0(variable," (NR, n=",Analysed_samples_nr,", PL, n=",Analysed_samples_placebo,")")) + theme(plot.title = element_text(hjust = 0.5, color="black"))
		b = b + theme(plot.title = element_text(hjust = 0.5, size = sizeOfLabels), axis.text.x = element_text(size = sizeOfLabels, color="black"), axis.text.y = element_text(size = sizeOfLabels, color="black")) + theme(axis.title = element_text(size = sizeOfLabels, color="black"))
		b = b + theme(strip.text.x = element_text(size = 8))
		b = b + theme(strip.text.x = element_text(size = sizeOfLabels))
		if(scientificNotation == TRUE) {
			b = b + scale_y_continuous(expand=c(0, 0), limits=c(0, max_value), label = formatYaxis)
		}
		if(scientificNotation != TRUE) {
			b = b + scale_y_continuous(expand=c(0, 0), limits=c(0, max_value))
		}
		b = b + scale_x_discrete(limits = c("PL_BL","PL_V09","NR_BL", "NR_V09"))
		b = b + annotate("text", x = "PL_BL", y = y_pos_p_value_plus_02, label = significancePL, size = sizeOfLabels, color="black")
		b = b + theme(legend.position = "none")	
		b = b + scale_color_manual(values=c(color_NR, color_placebo)) + theme(axis.ticks = element_line(color="black", size = sizeOfAxisLines)) + theme(axis.line = element_line(color = "black", size = sizeOfAxisLines))
		b = b + scale_x_discrete(limits = c("PL_BL","PL_V09","NR_BL", "NR_V09"))
			
		return(b)		
	}
	if (Analysed_samples_placebo < levelOfLOD & Analysed_samples_nr >= levelOfLOD) { 
		if (adjusted == TRUE) {
			variableLine = filter(corr_frame, corr_frame["Name"] == variable)
			significanceNR = variableLine[1,25]
			significanceNR = starSignif(significanceNR)
		
		}
		if (adjusted == FALSE) {
			variableLine = filter(corr_frame, corr_frame["Name"] == variable)
			significanceNR = variableLine[1,17]
			significanceNR = starSignif(significanceNR)
		}
	
		nr_df = filter(dataFrame, (dataFrame[,"ActivityId"] != "Delta") & (dataFrame[,"Randomisation"] == "NR") & ((dataFrame[,"ActivityId"] == "BL") | (dataFrame[,"ActivityId"] == "V09")))
		nr_df$Event = paste0("NR_",nr_df$ActivityId)
		max_BL = filter(nr_df, nr_df[,"ActivityId"] == "BL")
		max_v09 = filter(nr_df, nr_df[,"ActivityId"] == "V09")
		max_nr = c((max(as.numeric(unlist(max_BL[,variable]))) * 1.02), (max(as.numeric(unlist((max_v09[,variable])))) * 1.02))
		max_both = max_nr
		y_pos_p_value_plus_02 = max(max_both)
		max_value = y_pos_p_value_plus_02 * 1.1
		both_df = nr_df
		both_df[,variable] = unlist(as.numeric(both_df[,variable]))
		colnames(list) = list[2,]
		
		both_df[both_df == "NR_BL"] <- "NR-V1"
		both_df[both_df == "NR_V09"] <- "NR-V7"
	
		b = ggplot(both_df, aes(x = factor(Event, levels= c("NR-V1", "NR-V7")), y = unlist(as.numeric(both_df[,variable]))), colour = Randomisation) + theme_classic() 
		b = b + geom_line(aes(group=SubjectId, colour = Randomisation), size = sizeOfLines) + geom_point(size = sizeOfPoints, aes(colour = Randomisation))
		b = b + labs(y = list[3,variable], x = "", title = paste0(variable," (NR, n=",Analysed_samples_nr,", PL, n=",Analysed_samples_placebo,")")) + theme(plot.title = element_text(hjust = 0.5, color="black"))
		b = b + theme(plot.title = element_text(hjust = 0.5, size = sizeOfLabels), axis.text.x = element_text(size = sizeOfLabels, color="black"), axis.text.y = element_text(size = sizeOfLabels, color="black")) + theme(axis.title = element_text(size = sizeOfLabels, color="black"))
		b = b + theme(strip.text.x = element_text(size = sizeOfLabels))
		if(scientificNotation == TRUE) {
			b = b + scale_y_continuous(expand=c(0, 0), limits=c(0, max_value), label = formatYaxis)
		}
		if(scientificNotation != TRUE) {
			b = b + scale_y_continuous(expand=c(0, 0), limits=c(0, max_value))
		}
		b = b + scale_x_discrete(limits = c("PL-V1", "PL-V7","NR-V1", "NR-V7"))
		b = b + annotate("text", x = "NR-V1", y = y_pos_p_value_plus_02, label = significanceNR, size = sizeOfLabels, color="black")
		LODtext = paste0("Below LOD")
		b = b + annotate("text", x = "PL-V1", y = (max(max_both)/2), label = LODtext, size = (sizeOfLabels), color="black")
		b = b + theme(legend.position = "none")	
		b = b + scale_color_manual(values=c(color_NR, color_placebo)) + theme(axis.ticks = element_line(color="black", size = sizeOfAxisLines)) + theme(axis.line = element_line(color = "black", size = sizeOfAxisLines))
		return(b)
	}
	if (Analysed_samples_placebo < levelOfLOD & Analysed_samples_nr < levelOfLOD) { 
		data_dud = data.frame("col" = 1, "row" = 1)
		b = ggplot(data_dud, aes(x=col, y=row))
		b = b + labs(y = "", x = "", title = paste0(variable," (NR, n=",Analysed_samples_nr,", PL, n=",Analysed_samples_placebo,")")) + theme(plot.title = element_text(hjust = 0.5, size = 10)) + theme_void()
		b = b + theme(plot.title = element_text(hjust = 0.5, size = sizeOfLabels))
		b = b + annotate("text", , x = 1, y = 1, label = paste0("Below LOD"), size = sizeOfLabels)
		return(b)
	}	
}

# This makes a box plot comparing delta values between two variables, arguments are used in the same way as "makePairedPlotNoFacets" function: 
makeSingleBoxPlotNO_FACETS = function (variable, dataFrame, testMethod, list, corr_frame, adjusted, scientificNotation) {
	colnames(list) = list[2,]
	Analysed_samples_placebo = 10
	Analysed_samples_nr = 10
	testFrame = filter(dataFrame, dataFrame[,"ActivityId"] == "V02_1" | dataFrame[,"ActivityId"] == "V09_1")
	if (any(is.na(testFrame[,variable]))) {
		missingValues = filter(testFrame, (is.na(testFrame[,variable]) == TRUE) & (testFrame[,"ActivityId"] == "V02_1" | testFrame[,"ActivityId"] == "V09_1"))
		missingSubjects = missingValues[,"SubjectId"]		
		listMissingSubjects = unlist(missingSubjects)
		placeboFrame = filter(testFrame, testFrame[,"Randomisation"] == "Placebo")
		nrFrame = filter(testFrame, testFrame[,"Randomisation"] == "NR")
		Analysed_samples_placebo = Analysed_samples_placebo - length(unlist(unique(placeboFrame[is.na(placeboFrame[,variable]), "SubjectId"])))
		Analysed_samples_nr = Analysed_samples_nr - length(unlist(unique(nrFrame[is.na(nrFrame[,variable]), "SubjectId"])))
		rows = length(listMissingSubjects)
		for (i in 1:rows) {
				dataFrame = filter(dataFrame, dataFrame[,"SubjectId"] != listMissingSubjects[i])		
		}
	}
	
	variableColNumber = which(colnames(dataFrame) == variable)
	colnames(dataFrame)[variableColNumber] = gsub("\\+", "_", colnames(dataFrame)[variableColNumber])
	colnames(dataFrame)[variableColNumber] = gsub("\\/", "....", colnames(dataFrame)[variableColNumber])
	
	listVariable = variable
	variableGraph = gsub("\\+", "_", variable)
	variableGraph = gsub("\\/", "....", variableGraph)
		
	dataFrame[dataFrame == "V02_1"] <- "BL"
	dataFrame[dataFrame == "V09_1"] <- "V09"
		
	dataFrame[,variableGraph] = as.numeric(unlist(dataFrame[,variableGraph]))
	
	colnames(list) = list[2,]
	
	dataFrame[,variableGraph] = as.numeric(unlist(dataFrame[,variableGraph]))
	dataFrameDelta = filter(dataFrame, dataFrame[,"ActivityId"] == "Delta")
	max_BL = filter(dataFrameDelta, dataFrameDelta[,"Randomisation"] == "NR")
	max_v09 = filter(dataFrameDelta, dataFrameDelta[,"Randomisation"] == "Placebo")
	max_delta = c((max(as.numeric(unlist(max_BL[,variableGraph]))) * 1.02), (max(as.numeric(unlist((max_v09[,variableGraph])))) * 1.02))
	colnames(list) = list[2,]
	b = ggboxplot(dataFrameDelta, x = "Randomisation", y = variableGraph, fill = "Randomisation", palette = c(color_placebo_boxplot, color_NR_boxplot)) + theme_classic()
	b = b + labs(y = list[3,variable], x = "", title = variable) + theme(plot.title = element_text(hjust = 0.5))
	b = b + theme(axis.text.x = element_text(color="black"), axis.text.y = element_text(color="black")) + theme(axis.title = element_text(color="black"))
	b = b + theme(plot.title = element_text(hjust = 0.5, size = sizeOfLabels), axis.text.x = element_text(size = sizeOfLabels), axis.text.y = element_text(size = sizeOfLabels)) + theme(axis.title = element_text(size = sizeOfLabels))		
	y_pos_p_value_plus_02 = max(max_delta)
	y_pos_p_value_plus_02 = y_pos_p_value_plus_02 * 1.1
	upper_limit = y_pos_p_value_plus_02 * 1.1
	
	if(scientificNotation == TRUE) {
		b = b + scale_y_continuous(expand=c(0, 0), limits=c(0, upper_limit), label = formatYaxis)
	}
	if(scientificNotation != TRUE) {
		b = b + scale_y_continuous(expand=c(0, 0), limits=c(0, upper_limit))
	}
	
	if (adjusted == TRUE) {
		variableLine = filter(corr_frame, corr_frame["Name"] == listVariable)
		significancePlacebo = variableLine[1,22]
		significanceNR = variableLine[1,23]
		significanceDelta = variableLine[1,24]
		significancePlacebo = starSignif(significancePlacebo)
		significanceNR = starSignif(significanceNR)
		significanceDelta = starSignif(significanceDelta)	
	}
	if (adjusted == FALSE) {
		variableLine = filter(corr_frame, corr_frame["Name"] == listVariable)
		significancePlacebo = variableLine[1,10]
		significanceNR = variableLine[1,17]
		significanceDelta = variableLine[1,18]
		significancePlacebo = starSignif(significancePlacebo)
		significanceNR = starSignif(significanceNR)
		significanceDelta = starSignif(significanceDelta)
	}
	
	b = b + theme(strip.text.x = element_text(size = sizeOfLabels)) + theme(axis.ticks = element_line(color="black", size = sizeOfAxisLines)) + theme(axis.line = element_line(color = "black", size = sizeOfAxisLines))
	b = b + theme(legend.position = "none")
	b = b + annotate("text", x= "NR", y = y_pos_p_value_plus_02, label = significanceDelta, size = sizeOfLabels)
	return(b)
}

# Makes a graphs with mean values for each randomisation group for each visit in the study. If "StDev" argument is "WithStDev", the graphs show standard deviations. If "Range" argument is "WithRange", the range is displayed instead:
LineAverageValue_NO_FACETS = function (variableInQuestion, data, variableName, variableUnit, StDev, Range) {
	data_graph = data
	data_graph = as.data.frame(data_graph)
	data_graph[data_graph == "Placebo"] <- "PL"
	tempDfda = data.frame (SubjectId = data_graph[,"SubjectId"], EventId = data_graph[,"EventId"], Group = data_graph[,"Randomisation"], Value = data_graph[,variableInQuestion])
	tempDfda[,"Value"] <- as.numeric(tempDfda[,"Value"] )
	mean_data <- group_by(tempDfda, EventId, Group) %>% summarise(Value = mean(Value, na.rm = TRUE))
	sd_data <- group_by(tempDfda, EventId, Group) %>% summarise(StDev = sd(Value, na.rm = TRUE))
	min_data <- group_by(tempDfda, EventId, Group) %>% summarise(Min = min(Value, na.rm = TRUE))
	max_data <- group_by(tempDfda, EventId, Group) %>% summarise(Max = max(Value, na.rm = TRUE))
	mean_data = merge(mean_data, sd_data, by = c("EventId", "Group"))
	mean_data = merge(mean_data, min_data, by = c("EventId", "Group"))
	mean_data = merge(mean_data, max_data, by = c("EventId", "Group"))
	max_all_values = max(mean_data[,"Max"])
	a = ggplot(na.omit(mean_data), aes(x = EventId, y = Value, colour = Group, group = Group)) + geom_point(size = sizeOfPoints) + geom_line(size = sizeOfLines)
	a = a + theme_classic()
	a = a + labs(title = variableName) + xlab("") + ylab(variableUnit)
	a = a + scale_y_continuous(expand=c(0, 0), limits=c(0, (max_all_values*1.02)))
	if (StDev == "WithStDev") {
	a = a + geom_errorbar(aes(ymin=Value-StDev, ymax=Value+StDev), width = 0.2, size = 0.25) 
	}
	if (Range == "WithRange") {
	a = a + geom_errorbar(aes(ymin=Min, ymax=Max), width = 0.2, size = 0.25)
	}
	a = a + scale_color_manual(values=c(color_NR, color_placebo)) + theme(axis.ticks = element_line(color="black", size = sizeOfAxisLines)) + theme(axis.line = element_line(color = "black", size = sizeOfAxisLines))
	a = a + theme(axis.text.x = element_text(color="black", size = sizeOfLabels), axis.text.y = element_text(color="black", size = sizeOfLabels)) + theme(axis.title = element_text(color="black", size = sizeOfLabels))
	return(a)
}



