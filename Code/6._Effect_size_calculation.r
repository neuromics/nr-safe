
# Code to calculate effect size from the MDS-UPDRS Data:

a = up_rawDataWithTimes
a[,"Time since levodopa"] = as.numeric(a[,"Time since levodopa"]) 
a[,"Hoehn & Yahr"] = as.numeric(a[,"Hoehn & Yahr"])
a[which(is.na(a[,"Time since levodopa"])), "Time since levodopa"] = 0
 
cohen_NR_base = filter(a, a[,"Randomisation"] == "NR" & a[,"ActivityId"] == "V02_1")
cohen_NR_end = filter(a, a[,"Randomisation"] == "NR" & a[,"ActivityId"] == "V09_1")

cohen_PL_base = filter(a, a[,"Randomisation"] == "Placebo" & a[,"ActivityId"] == "V02_1")
cohen_PL_end = filter(a, a[,"Randomisation"] == "Placebo" & a[,"ActivityId"] == "V09_1")

calculate_cohen_d = function(column) {
	deltaNR = cohen_NR_end[,column] - cohen_NR_base[,column]
	deltaPL = cohen_PL_end[,column] - cohen_PL_base[,column]
	cohen.d(deltaNR, deltaPL)
}

calculate_cohen_d("UPDRS Total")
calculate_cohen_d("UPDRS Part 1")
calculate_cohen_d("UPDRS Part 2")
calculate_cohen_d("UPDRS Part 3")
calculate_cohen_d("UPDRS Part 4")
calculate_cohen_d("Time since levodopa")
calculate_cohen_d("Hoehn & Yahr")

b = data_up_subset
b[which(is.na(b[,"Minutes_since_levodopa"])), "Minutes_since_levodopa"] = 0
b[,"Minutes_since_levodopa"] = as.numeric(b[,"Minutes_since_levodopa"]) 
b[,"HoehnYahrCD"] = as.numeric(b[,"HoehnYahrCD"])

cohen_NR_base_sub = filter(b, b[,"Randomisation"] == "NR" & b[,"ActivityId"] == "V02_1")
cohen_NR_end_sub = filter(b, b[,"Randomisation"] == "NR" & b[,"ActivityId"] == "V09_1")

cohen_PL_base_sub = filter(b, b[,"Randomisation"] == "Placebo" & b[,"ActivityId"] == "V02_1")
cohen_PL_end_sub = filter(b, b[,"Randomisation"] == "Placebo" & b[,"ActivityId"] == "V09_1")

calculate_cohen_d_sub = function(column) {
	deltaNR = cohen_NR_end_sub[,column] - cohen_NR_base_sub[,column]
	deltaPL = cohen_PL_end_sub[,column] - cohen_PL_base_sub[,column]
	cohen.d(deltaNR, deltaPL)
}

calculate_cohen_d_sub("MDSUPDRTOS")
calculate_cohen_d_sub("MDSUPDRS_Section1")
calculate_cohen_d_sub("MDSUPDRS_Section2")
calculate_cohen_d_sub("MDSUPDRS_Section3")
calculate_cohen_d_sub("MDSUPDRS_Section4")
calculate_cohen_d_sub("Minutes_since_levodopa")
calculate_cohen_d_sub("HoehnYahrCD")

