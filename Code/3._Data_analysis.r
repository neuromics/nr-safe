
########################################################
	 # LISTS OF VARIABLES #
# This list contains all variables to be analysed in the data from the "NADmed_data.xlsx", "VieDocData_in_sheets.xlsx" and "VieDoc_Data_all.xlsx" files: 

# Vital signs: 
vs_list = data.frame(
	"Variable" = c("Variable", "Name", "Unit", "Test"),
	"VSSYS" = c("VSSYS","Systolic blood pressure", "mmHg", "T-test"),
	"VSDIA" = c("VSDIA", "Diastolic blood pressure", "mmHg", "Wilcox"),
	"VSPULS"= c("VSPULS","Pulse","/min", "Wilcox"),
	"VSWEIGHT1" = c("VSWEIGHT1", "Weight", "kg", "T-test")
)

# NadMed data: 
nad_list = data.frame(
	"Variable" = c("Variable", "Name", "Unit", "Test"),
	"NAD_uM" = c("NAD, uM", "NAD+", "uM", "T-test"),
	"NADH_uM" = c("NAD, uM", "NADH", "uM", "Wilcox"),
	"NADpool_uM" = c("NAD pool, uM", "NAD+ pool", "uM", "T-test"),
	"NAD__NADH" =  c("NAD+/NADH", "NAD+/NADH", "Ratio", "Wilcox"),
	"NADP_uM" = c("NADP, uM", "NADP+", "uM", "Wilcox"),
	"NADPH_uM" = c("NADPH, uM", "NADPH", "uM", "T-test"),
	"NADPpool_uM" = c("NADP pool, uM", "NADP+ pool", "uM", "T-test"),
	"NADP__NADPH" = c("NADP+/NADPH", "NADP+/NADPH", "Ratio", "T-test"),
	"GSH_mM" = c("GSH, mM", "GSH", "mM", "T-test"),
	"GSSG_mM" = c("GSSG, mM", "GSSG", "mM", "T-test"),
	"GSHpool" = c("GSH pool", "GSH pool", "mM", "T-test"),
	"GSH_GSSG" = c("GSH/GSSG", "GSH/GSSG", "Ratio", "T-test")
) 

# Clinical laboratory values: 
sr_list = data.frame(
	"Variable"				= c("Variable",					"Name", 				"Unit", 				"Test"),
	"SR1_Copy1" 			= c("SR1_Copy1", 				"Hemoglobin", 			"g/dL", "T-test"),
	"SR1_Copy18_Copy1" 		= c("SR1_Copy18_Copy1", 		"RBC",					"10*12/L", "T-test"),
	"SR1_Copy3" 			= c("SR1_Copy3" ,				"WBC", 					"10*9/L", "T-test"),
	"SR1_Copy3_Copy2" 		= c("SR1_Copy3_Copy2", 			"Neutrophils", 			"10*9/L", "T-test"),
	"SR1_Copy3_Copy3" 		= c("SR1_Copy3_Copy3", 			"Lymphocytes", 			"10*9/L", "Wilcox"),
	"SR1_Copy3_Copy4" 		= c("SR1_Copy3_Copy4", 			"Monocytes", 			"10*9/L", "Wilcox"),
	"SR1_Copy3_Copy5" 		= c("SR1_Copy3_Copy5", 			"Eosinophils", 			"10*9/L", "Wilcox"),
	"SR1_Copy3_Copy6" 		= c("SR1_Copy3_Copy6", 			"Basophils", 			"10*9/L", "Wilcox"),
	"SR1_Copy2" 			= c("SR1_Copy2" ,				"Platelets", 			"10*9/L", "T-test"),
	"SR1_Copy16_Copy1" 		= c("SR1_Copy16_Copy1", 		"Methylmalonic acid", 	"pmol/L", "Wilcox"),
	"SR1_Copy17_Copy1" 		= c("SR1_Copy17_Copy1", 		"Homocysteine", 		"µmol/L", "T-test"),
	"SR1_Copy18_Copy2" 		= c("SR1_Copy18_Copy2", 		"vitamin B12", 			"pmol/L", "Wilcox"),
	"SR1_Copy13_Copy1" 		= c("SR1_Copy13_Copy1", 		"Folic acid", 			"nmol/L", "Wilcox"),
	"SR1" 					= c("SR1", 						"CRP", 					"mg/L", "Wilcox"),
	"SR1_Copy8" 			= c("SR1_Copy8", 				"Creatinine", 			"µmol/L", "T-test"),
	"SR1_Copy19_Copy2" 		= c("SR1_Copy19_Copy2", 		"Urea", 				"mmol/L", "T-test"),
	"SR1_Copy5" 			= c("SR1_Copy5", 				"Sodium", 				"mmol/L", "T-test"),
	"SR1_Copy6" 			= c("SR1_Copy6", 				"Potassium", 			"mmol/L", "T-test"),
	"SR1_Copy10" 			= c("SR1_Copy10",				"ALAT", 				"U/L", "Wilcox"),
	"SR1_Copy9" 			= c("SR1_Copy9", 				"ASAT", 				"U/L", "Wilcox"),
	"SR1_Copy19_Copy1"		= c("SR1_Copy19_Copy1",			"ALP", 					"U/L", "T-test"),
	"SR1_Copy12"			= c("SR1_Copy12", 				"GT", 					"U/L", "T-test"),
	"SR1_Copy17" 			= c("SR1_Copy17", 				"Bilirubin", 			"µmol/L", "T-test"),
	"SR1_Copy19" 			= c("SR1_Copy19", 				"CK", 					"U/L", "Wilcox"),
	"SR1_Copy19_Copy3" 		= c("SR1_Copy19_Copy3", 		"Total cholesterol", 	"mmol/L", "T-test"),
	"SR1_Copy19_Copy4" 		= c("SR1_Copy19_Copy4", 		"HDL", 					"mmol/L", "T-test"),
	"SR1_Copy19_Copy5" 		= c("SR1_Copy19_Copy5", 		"LDL", 					"mmol/L", "T-test"),
	"SR1_Copy1_Copy1" 		= c("SR1_Copy1_Copy1", 			"Ionised calcium", 		"mmol/L", "T-test"),
	"SR1_Copy14" 			= c("SR1_Copy14", 				"TSH", 					"mIU/L", "Wilcox"),
	"SR1_Copy15" 			= c("SR1_Copy15", 				"FT4", 					"pmol/L", "T-test"),
	"SR1_Copy1_Copy1_Copy2" = c("SR1_Copy1_Copy1_Copy2",	"Serum insulin",		"mIU/L", "T-test"),
	"SR1_Copy1_Copy1_Copy1" = c("SR1_Copy1_Copy1_Copy1", 	"Fasting blood glucose","mmol/L", "T-test"),
	"SR1_Copy19_Copy6" 		= c("SR1_Copy19_Copy6", 		"Triglycerides", 		"mmol/L", "T-test")
)

# MDS-UPDRS
up_listWithTimes = data.frame(
	"Variable" = c("Variable", "Name", "Unit", "Test"),
	"MDSUPDRS_Section1" = c("MDSUPDRS_Section1", "UPDRS Part 1", "score", "T-test"),
	"MDSUPDRS_Section2" = c("MDSUPDRS_Section2", "UPDRS Part 2", "score", "T-test"),
	"MDSUPDRS_Section3" = c("MDSUPDRS_Section3", "UPDRS Part 3", "score", "T-test"),
	"MDSUPDRS_Section4" = c("MDSUPDRS_Section4", "UPDRS Part 4", "score", "T-test"),
	"MDSUPDRTOS" = c("1_MDSUPDRTOS", "UPDRS Total", "score", "T-test"),
	"HoehnYahrCD" = c("1_HoehnYahrCD", "Hoehn & Yahr", "score", "Wilcox"),
	"Minutes_since_levodopa" = c("Minutes_since_levodopa", "Time since levodopa", "minutes", "T-test")
)

###################################################################
# Data analysis:
###################################################################

# Filters out participants from clinical laboratory values who did not fast: 
# filter out SubjectId 01-22 values for creatinine, urea, potassium and sodium 
# as kreatinine was 1micromol/L, urea 60mmol/L, sodium 3.9 mmol/L and potassium 138mmol/L on day 28
# and therefore highly likely data entry error.
# sets the above mentioned values to NA
data_sr_filtered = data_sr
data_sr_filtered <- within(data_sr_filtered, SR1_Copy8[SubjectId == '01-22'] <- NA)
data_sr_filtered <- within(data_sr_filtered, SR1_Copy19_Copy2[SubjectId == '01-22'] <- NA)
data_sr_filtered <- within(data_sr_filtered, SR1_Copy5[SubjectId == '01-22'] <- NA)
data_sr_filtered <- within(data_sr_filtered, SR1_Copy6[SubjectId == '01-22'] <- NA)

# Filtering data to remove subject 01-10 and 01-5 as these did not fast at either baseline or both baseline and day 28: 
# SubjectIds removed: 01-05 and 01-10 from fasting serum insulin, fasting glucose and triglycerides
data_sr_filtered <- within(data_sr_filtered, SR1_Copy1_Copy1_Copy2[SubjectId == '01-5'] <- NA)
data_sr_filtered <- within(data_sr_filtered, SR1_Copy1_Copy1_Copy1[SubjectId == '01-5'] <- NA)
data_sr_filtered <- within(data_sr_filtered, SR1_Copy19_Copy6[SubjectId == '01-5'] <- NA)
data_sr_filtered <- within(data_sr_filtered, SR1_Copy1_Copy1_Copy2[SubjectId == '01-10'] <- NA)
data_sr_filtered <- within(data_sr_filtered, SR1_Copy1_Copy1_Copy1[SubjectId == '01-10'] <- NA)
data_sr_filtered <- within(data_sr_filtered, SR1_Copy19_Copy6[SubjectId == '01-10'] <- NA)

# in MDS-UPDRS dataset, subject 01-18 was erroneously entered as being on Levodopa at baseline. The Minutes since levodopa value was therefore set as NA:
data_up[c(which(data_up[,"SubjectId"] == "01-18")),"Patient_on_levodopa"] = 0
data_up[c(which(data_up[,"SubjectId"] == "01-18")),"Minutes_since_levodopa"] = NA

# 			MDS-UPDRS subset with similar times since levodopa (NR: n = 8, PL: n = 8):
data_up_subset = filter(data_up, data_up[,"SubjectId"] != "01-20")
data_up_subset = filter(data_up_subset, data_up_subset[,"SubjectId"] != "01-5")
data_up_subset = filter(data_up_subset, data_up_subset[,"SubjectId"] != "01-6")
data_up_subset = filter(data_up_subset, data_up_subset[,"SubjectId"] != "01-8")

# 						Normality testing: 
# Performs normality testing via the Shapiro-Wilk test on the dataset:
# Output from the combineNormalityTest function is a list of Shapiro-Wilk values with non-significant variables filtered out: 
options(scipen=999)

vs_normalityTest = combineNormalityTest(data_vs, vs_list)
vs_normalityTest = rbind(c("Vital signs", "", ""), vs_normalityTest)
vs_normalityTest[(nrow(vs_normalityTest) + 1),1] = "Clinical laboratory values"
sr_normalityTest = combineNormalityTest(data_sr_filtered, sr_list)
sr_normalityTest[(nrow(sr_normalityTest) + 1),1] = "MDS-UPDRS"
up_normalityTest = combineNormalityTest(data_up, up_listWithTimes)
up_normalityTest[(nrow(up_normalityTest) + 1),1] = "MDS-UPDRS subset"
up_subsetNormalityTest = combineNormalityTest(data_up_subset, up_listWithTimes)
up_subsetNormalityTest[(nrow(up_subsetNormalityTest) + 1),1] = "NAD and GSH metabolome"
nad_normalityTest = combineNormalityTest(nad, nad_list)
nad_normalityTest[(nrow(nad_normalityTest) + 1),1] = "LCMS whole blood"
ms_bl_req_normalityTest = combineNormalityTest(MS_omics_req_blood, list_var_ms_blood_req)
ms_bl_req_normalityTest[(nrow(ms_bl_req_normalityTest) + 1),1] = "LCMS urine"
ms_ur_req_normalityTest = combineNormalityTest(MS_omics_req_urine, list_var_ms_urine_req)

normality_test = data.frame("Value" = "", "PL_Delta" = "", "NR_Delta" = "")
normality_test = rbind(normality_test, vs_normalityTest)
normality_test = rbind(normality_test, sr_normalityTest)
normality_test = rbind(normality_test, up_normalityTest)
normality_test = rbind(normality_test, up_subsetNormalityTest)
normality_test = rbind(normality_test, nad_normalityTest)
normality_test = rbind(normality_test, ms_bl_req_normalityTest)
normality_test = rbind(normality_test, ms_ur_req_normalityTest)
if (normality_test[1,1] == "") { normality_test = normality_test[-1,]  }

normality_test[,2] = as.numeric(unlist(normality_test[,2]))
normality_test[,3] = as.numeric(unlist(normality_test[,3]))

#######################################################################
#   Normality check on baseline demographic variables:
setwd(inDir)

#Read in Personal information from PI "sheeted" file
data_pi_nr = joinDataFrames("PI", nr_ids, "NR", inputFile)
data_pi_pl = joinDataFrames("PI", pl_ids, "Placebo", inputFile)
data_pi = rbind(data_pi_nr, data_pi_pl)

Demo_normality = data.frame( Value = "", "Placebo" = "", "NR" = "")
data_nr_v00 = filter(data, EventId == "V00" & Randomisation == "NR")
data_pl_v00 = filter(data, EventId == "V00" & Randomisation == "Placebo")
data_nr_base = filter(data, ActivityId == "V02_1" & Randomisation == "NR")
data_pl_base = filter(data, ActivityId == "V02_1" & Randomisation == "Placebo")
line = 1

# This code is shown only for illustration, as data is not available due to data privacy laws:

#Age
#nr = as.numeric(unlist(data_nr_v00[, "1.PIAGE"]))
#pl = as.numeric(unlist(data_pl_v00[, "1.PIAGE"]))
#ageShapNR = shapiro.test(nr)$p.value
#ageShapPL = shapiro.test(pl)$p.value
#if (ageShapNR < 0.05 | ageShapPL < 0.05) {
#	Demo_normality[line,] = c("Age", ageShapPL, ageShapNR)
#	line = line + 1
#}

#Age
#nr = as.numeric(unlist(data_nr_v00[, "1.PIAGE"]))
#pl = as.numeric(unlist(data_pl_v00[, "1.PIAGE"]))
#ageShapNR = shapiro.test(nr)$p.value
#ageShapPL = shapiro.test(pl)$p.value
#if (ageShapNR < 0.05 | ageShapPL < 0.05) {
#	Demo_normality[line,] = c("Age", ageShapPL, ageShapNR)
#	line = line + 1
#}


#Height
nr =  as.numeric(unlist(data_nr_base[, "1.VSHEIGHT1"]))
pl =  as.numeric(unlist(data_pl_base[, "1.VSHEIGHT1"]))
heightShapNR = shapiro.test(nr)$p.value
heightShapPL = shapiro.test(pl)$p.value
if (heightShapNR < 0.05 | heightShapPL < 0.05) {
	Demo_normality[line,] = c("Height", heightShapPL, heightShapNR)
	line = line + 1
}
#Weight
nr =  as.numeric(unlist(data_nr_base[, "1.VSWEIGHT1"]))
pl =  as.numeric(unlist(data_pl_base[, "1.VSWEIGHT1"]))
weightShapNR = shapiro.test(nr)$p.value
weightShapPL = shapiro.test(pl)$p.value
if (weightShapNR < 0.05 | weightShapPL < 0.05) {
	Demo_normality[line,] = c("Weight", weightShapPL, weightShapNR)
	line = line + 1
}
#Drug compliance
data_nr_base = filter(data, ActivityId == "V09_2" & Randomisation == "NR")
data_pl_base = filter(data, ActivityId == "V09_2" & Randomisation == "Placebo")
nr =  as.numeric(unlist(data_nr_base[, "1.DACOMPL"]))
pl =  as.numeric(unlist(data_pl_base[, "1.DACOMPL"]))
DrugCompShapNR = shapiro.test(nr)$p.value
DrugCompShapPL = shapiro.test(pl)$p.value
if (DrugCompShapNR < 0.05 | DrugCompShapPL < 0.05) {
	Demo_normality[line,] = c("Drug compliance", DrugCompShapPL, DrugCompShapNR)
	line = line + 1
}
#Years since diagnosis of PD
PD_diagnosis = filter(data, EventId == "CE04" & data[,"1.MHTERM"] == "Parkinsons sykdom")
PD_diagnose_year = c(2015, 2014, 2018, 2019, 2013, 2018, 2020, 2017, 2012, 2020, 2014, 2014, 2009, 2012, 2018, 2018, 2020, 2020, 2009, 2017)
PD_year = cbind(PD_diagnosis, PD_diagnose_year)
data_pl = filter(PD_year, Randomisation == "Placebo")
data_nr = filter(PD_year, Randomisation == "NR")
year_pl = data_pl[, "PD_diagnose_year"]
year_nr = data_nr[, "PD_diagnose_year"]
pl = as_tibble(year_pl)
nr = as_tibble(year_nr)
pl = 2022 - pl 
nr = 2022 - nr
pl = as.numeric(unlist((pl)))
nr = as.numeric(unlist((nr)))
TimePDShapNR = shapiro.test(nr)$p.value
TimePDShapPL = shapiro.test(pl)$p.value
if (TimePDShapNR < 0.05 | TimePDShapPL < 0.05) {
	Demo_normality[line,] = c("Years since PD diagnosis", TimePDShapPL, TimePDShapNR)
	line = line + 1
}
#BMI
data_nr_base = filter(data, ActivityId == "V02_1" & Randomisation == "NR")
data_pl_base = filter(data, ActivityId == "V02_1" & Randomisation == "Placebo") 
nr =  as.numeric(unlist(data_nr_base[, "1.VSBMI1"]))
pl =  as.numeric(unlist(data_pl_base[, "1.VSBMI1"]))
BMIShapNR = shapiro.test(nr)$p.value
BMIShapPL = shapiro.test(pl)$p.value
if (BMIShapNR < 0.05 | BMIShapPL < 0.05) {
	Demo_normality[line,] = c("BMI", BMIShapPL, BMIShapNR)
	line = line + 1
}

if (Demo_normality[1,1] == "") { Demo_normality = Demo_normality[-1,]  }

Demo_normality[,2] = as.numeric(unlist(Demo_normality[,2]))
Demo_normality[,3] = as.numeric(unlist(Demo_normality[,3]))


#						Statistical testing of dataset:

# Examine if baselines are similar between the randomisations: 

# Checks if Baselines are similar between randomisations: 
baseBase_vs = combineBaselineToBaseline(data_vs, vs_list, FALSE)
baseBase_sr = combineBaselineToBaseline(data_sr_filtered, sr_list, FALSE)
baseBase_nad = combineBaselineToBaseline(nad, nad_list, FALSE)
list_var_ms_blood_req[7,"Nicotine amide"] = TRUE
list_var_ms_blood_req[7,"Homocysteine"] = TRUE
list_var_ms_blood_req[7,"N1-Methyl-2-pyridone-5-carboxamide"] = TRUE
list_var_ms_blood_req[7,"NAD+"] = TRUE
list_var_ms_blood_req[7,"Adenosine"] = TRUE
list_var_ms_blood_req[7,"AMP"] = TRUE
list_var_ms_blood_req[7,"Nicotinamide N-oxide"] = TRUE
list_var_ms_blood_req[7,"NADPH"] = TRUE
list_var_ms_urine_req[7,"Nicotinamide riboside"] = TRUE
list_var_ms_urine_req[7,"Nicotineamide N-oxide"] = TRUE
list_var_ms_urine_req[7,"N1-Methyl-2-pyridone-5-carboxamide"] = TRUE
list_var_ms_urine_req[7,"Nicotinic acid"] = TRUE
list_var_ms_urine_req[7,"S-Adenosylhomocysteine"] = TRUE
baseBase_MSblood = combineBaselineToBaseline(MS_omics_req_blood, list_var_ms_blood_req, TRUE)
baseBase_MSurine = combineBaselineToBaseline(MS_omics_req_urine, list_var_ms_urine_req, TRUE)
	
# Adds column with multiple testing correction via Benjamini-Hochberg test (FDR) to baseline comparisons: 
baseBase_vs = MultipleTestingCorrectionBaseline_to_Baseline(baseBase_vs)


#remove Total UPDPRS, Hoehn and Yahr score and time since levodopa to perform multiple testing correction at baseline:
baseBase_up_wTimes = combineBaselineToBaseline(data_up, up_listWithTimes, FALSE)
baseBase_up_wTimes_mult = baseBase_up_wTimes[c(-5, -6, -7),]
baseBase_up_wTimes_mult = MultipleTestingCorrectionBaseline_to_Baseline(baseBase_up_wTimes_mult)
baseBase_up_wTimes_mult[5,] = baseBase_up_wTimes[5,]
baseBase_up_wTimes_mult[7,] = baseBase_up_wTimes[6,]
baseBase_up_wTimes_mult[6,] = baseBase_up_wTimes[7,]
baseBase_up_wTimes_mult[5,13] = NA
baseBase_up_wTimes_mult[6,13] = NA
baseBase_up_wTimes_mult[7,13] = NA
baseBase_up_wTimes = baseBase_up_wTimes_mult

#remove Total UPDPRS, Hoehn and Yahr score and time since levodopa to perform multiple testing correction at baseline:
baseBase_up_subset = combineBaselineToBaseline(data_up_subset, up_listWithTimes, FALSE)
baseBase_up_subset_mult = baseBase_up_subset[c(-5, -6, -7),]
baseBase_up_subset_mult = MultipleTestingCorrectionBaseline_to_Baseline(baseBase_up_subset_mult)
baseBase_up_subset_mult[5,] = baseBase_up_subset[5,]
baseBase_up_subset_mult[7,] = baseBase_up_subset[6,]
baseBase_up_subset_mult[6,] = baseBase_up_subset[7,]
baseBase_up_subset_mult[5,13] = NA
baseBase_up_subset_mult[6,13] = NA
baseBase_up_subset_mult[7,13] = NA
baseBase_up_subset = baseBase_up_subset_mult

baseBase_sr = MultipleTestingCorrectionBaseline_to_Baseline(baseBase_sr)
baseBase_nad = MultipleTestingCorrectionBaseline_to_Baseline(baseBase_nad)
baseBase_MSblood = MultipleTestingCorrectionBaseline_to_Baseline(baseBase_MSblood)
baseBase_MSurine = MultipleTestingCorrectionBaseline_to_Baseline(baseBase_MSurine)


# Statitical testing of specific groups of variables: 
# Outputs data as a dataframe with summary statistics of one variable per line:

# 			Vital signs analysis: 
vs_frame = combine(data_vs, vs_list)
vs_mult = multipleTestingCorrectionFDR(vs_frame)
vs_corr =  merge(vs_frame, vs_mult, by = "Name")
vs_rawData = combineData(data_vs, vs_list)

#			NADmed analysis:
nad_frame = combine(nad, nad_list)
nad_mult = multipleTestingCorrectionFDR(nad_frame)
nad_corr = merge(nad_frame, nad_mult, by = "Name")
nad_rawData = combineData(nad, nad_list)


# 			Clinical laboratory values analysis:
#			(uses filtered dataset with specific participants removed)
sr_frame = combine(data_sr_filtered, sr_list)
sr_mult = multipleTestingCorrectionFDR(sr_frame)
sr_corr = merge(sr_frame, sr_mult, by = "Name")
sr_rawDataFiltered = combineData(data_sr_filtered, sr_list)
sr_rawDataNotFiltered = combineData(data_sr, sr_list)

#			MDS-UPDRS analysis:
upWithTime_frame = combine(data_up, up_listWithTimes)
#remove Total UPDPRS, Hoehn and Yahr score and time since levodopa to perform multiple testing correction:
upWithTime_frame_multipleTesting = upWithTime_frame[c(-5, -6, -7),]
upWithTime_mult = multipleTestingCorrectionFDR(upWithTime_frame_multipleTesting)
upWithTime_mult[5,] = c("UPDRS Total", NA, NA, NA)
upWithTime_mult[6,] = c("Hoehn & Yahr", NA, NA, NA)
upWithTime_mult[7,] = c("Time since levodopa", NA, NA, NA)
upWithTime_corr = merge(upWithTime_frame, upWithTime_mult, by = "Name")
up_rawDataWithTimes = combineData(data_up, up_listWithTimes)
colnames(up_rawDataWithTimes) = c("SubjectId", "Randomisation", "ActivityId", up_listWithTimes[2,2], up_listWithTimes[2,3], up_listWithTimes[2,4], up_listWithTimes[2,5], up_listWithTimes[2,6], up_listWithTimes[2,7], up_listWithTimes[2,8], up_listWithTimes[2,9], up_listWithTimes[2,10], up_listWithTimes[2,11])

# 			MDS-UPDRS subset with similar times since levodopa (NR: n = 8, PL: n = 8):
up_subset_frame = combine(data_up_subset, up_listWithTimes)
#remove Total UPDPRS, Hoehn and Yahr score and time since levodopa to perform multiple testing correction:
up_subset_frame_multipleTesting = up_subset_frame[c(-5, -6, -7),]
up_subset_mult = multipleTestingCorrectionFDR(up_subset_frame_multipleTesting)
up_subset_mult[5,] = c("UPDRS Total", NA, NA, NA)
up_subset_mult[6,] = c("Hoehn & Yahr", NA, NA, NA)
up_subset_mult[7,] = c("Time since levodopa", NA, NA, NA)
up_subset_corr = merge(up_subset_frame, up_subset_mult, by = "Name")

# LCMS whole blood requested compounds from targeted metabolomics and subset of relevant compounds from untargetted metabolomics
# Sets this variable to be analysed by Wilcoxon test: 
list_var_ms_blood_req[7,"Nicotine amide"] = TRUE
# all other variables are analysed by t-tests:
ms_bl_req_frame = combineRequiredSheets(MS_omics_req_blood, list_var_ms_blood_req)
ms_bl_req_mult = multipleTestingCorrectionFDR(ms_bl_req_frame)
ms_bl_req_corr = merge(ms_bl_req_frame, ms_bl_req_mult, by = "Name")
MS_omics_req_blood = tibble(MS_omics_req_blood)
ms_bl_req_rawData = combineData(MS_omics_req_blood, list_var_ms_blood_req)
# with values with less than 7 samples below LOD filtered out: 
ms_bl_req_frame_filtered = combineRequiredSheetsFiltersLODS(MS_omics_req_blood, list_var_ms_blood_req)
ms_bl_req_mult_filtered  = multipleTestingCorrectionFDR(ms_bl_req_frame_filtered)
ms_bl_req_corr_filtered  = merge(ms_bl_req_frame_filtered, ms_bl_req_mult_filtered, by = "Name")

# LCMS urine requested compounds from targeted metabolomics and subset of relevant compounds from untargetted metabolomics
# Sets these variables to be analysed by Wilcoxon test: 
list_var_ms_urine_req[7,"Nicotinamide riboside"] = TRUE
list_var_ms_urine_req[7,"Nicotineamide N-oxide"] = TRUE
# all other variables are analysed by t-tests:
ms_ur_req_frame = combineRequiredSheets(MS_omics_req_urine, list_var_ms_urine_req)
ms_ur_req_mult = multipleTestingCorrectionFDR(ms_ur_req_frame)
ms_ur_req_corr = merge(ms_ur_req_frame, ms_ur_req_mult, by = "Name")
MS_omics_req_urine = tibble(MS_omics_req_urine)
ms_ur_req_rawData = combineData(MS_omics_req_urine, list_var_ms_urine_req)
# with values with less than 7 samples below LOD filtered out: 
ms_ur_req_frame_filtered = combineRequiredSheetsFiltersLODS(MS_omics_req_urine, list_var_ms_urine_req)
ms_ur_req_mult_filtered = multipleTestingCorrectionFDR(ms_ur_req_frame_filtered)
ms_ur_req_corr_filtered = merge(ms_ur_req_frame_filtered, ms_ur_req_mult_filtered, by = "Name")

# adds column with type of test to "corr frame":
stat_test_urine = data.frame("Name" = unlist(list_var_ms_urine_req[1,]), "Statistical_test" = unlist(list_var_ms_urine_req[7,]))
rownames(stat_test_urine) = NULL
stat_test_urine[stat_test_urine == FALSE] <- "T-test"
stat_test_urine[stat_test_urine == TRUE] <- "Wilcoxon test"
stat_test_urine = stat_test_urine[-1,]

ms_ur_req_corr_filtered = merge(ms_ur_req_corr_filtered, stat_test_urine, by = "Name")

stat_test_blood = data.frame("Name" = unlist(list_var_ms_blood_req[1,]), "Statistical_test" = unlist(list_var_ms_blood_req[7,]))
rownames(stat_test_blood) = NULL
stat_test_blood[stat_test_blood == FALSE] <- "T-test"
stat_test_blood[stat_test_blood == TRUE] <- "Wilcoxon test"
stat_test_blood = stat_test_blood[-1,]

ms_bl_req_corr_filtered = merge(ms_bl_req_corr_filtered, stat_test_blood, by = "Name")

# Analysis of demographic parameters:

Demographics = data.frame( Value = "", Unit = "", Placebo_mean = "", Placebo_sd = "", NR_mean = "", NR_sd = "", "P_value_t_test" = "", "Method_t" = "", "P_value_wilcox" = "", "Method_w" = "")
data_nr_v00 = filter(data, EventId == "V00" & Randomisation == "NR")
data_pl_v00 = filter(data, EventId == "V00" & Randomisation == "Placebo")


# This code is shown only for illustration, as data is not available due to data privacy laws:

#Sex analysis
#sexDF = data[,c("Randomisation", "1.PISEX", "1.PISEXCD")]
#sexDF = sexDF %>% filter(!is.na(sexDF[,2]))

#male_nr = filter(sexDF, sexDF[,"1.PISEX"] == "Male" & sexDF[,"Randomisation"] == "NR")
#male_nr = nrow(male_nr)
#female_nr = filter(sexDF, sexDF[,"1.PISEX"] == "Female" & sexDF[,"Randomisation"] == "NR")
#female_nr = nrow(female_nr)
#male_pl = filter(sexDF, sexDF[,"1.PISEX"] == "Male" & sexDF[,"Randomisation"] == "Placebo")
#male_pl = nrow(male_pl)
#female_pl = filter(sexDF, sexDF[,"1.PISEX"] == "Female" & sexDF[,"Randomisation"] == "Placebo")
#female_pl = nrow(female_pl)
#conditions = data.frame("Males" = c(male_pl, male_nr), "Females" = c(female_pl, female_nr), row.names = c("Placebo", "NR"))	
#colnames(conditions) = c("Male", "Female")
#conditions
#fisher = fisher.test(conditions)
#chi = chisq.test(conditions)

#line = 1 
#Demographics[line,"Value"] = "Sex"
#Demographics[line,"Unit"] = "(female/male)"
#Demographics[line,"Placebo_mean"] = paste0(female_pl,"/",male_pl)
#Demographics[line,"Placebo_sd"] = ""
#Demographics[line,"NR_mean"] = paste0(female_nr,"/",male_nr)
#Demographics[line,"NR_sd"] = ""
#Demographics[line,"P_value_t_test"] = chi$p.value
#Demographics[line,"Method_t"] = fisher$method
#Demographics[line,"P_value_wilcox"] = fisher$p.value
#Demographics[line,"Method_w"] = chi$method
  
#Age analysis
#nr = data_nr_v00[, "1.PIAGE"]
#pl = data_pl_v00[, "1.PIAGE"]
#line = 2

#sumNR = summary(as.numeric(unlist(nr)))
#sdNR = sd(as.numeric(unlist(nr)))
#sumPL = summary(as.numeric(unlist(pl)))
#sdPL = sd(as.numeric(unlist(pl)))
#Ttest = t.test(as.numeric(unlist(pl)), as.numeric(unlist(nr)), alternative = "two.sided", var.equal=TRUE)
#Wilcox = wilcox.test(as.numeric(unlist(pl)), as.numeric(unlist(nr)), alternative = "two.sided", var.equal=TRUE)
#Demographics[line,"Value"] = "Age"
#Demographics[line,"Unit"] = "(years)"
#Demographics[line,"Placebo_mean"] = sumPL[4]
#Demographics[line,"Placebo_sd"] = sdPL
#Demographics[line,"NR_mean"] = sumNR[4]
#Demographics[line,"NR_sd"] = sdNR
#Demographics[line,"P_value_t_test"] = Ttest$p.value
#Demographics[line,"Method_t"] = Ttest$method
#Demographics[line,"P_value_wilcox"] = Wilcox$p.value
#Demographics[line,"Method_w"] = Wilcox$method




#Height analysis
class(data[["1.VSHEIGHT1"]])<-"numeric"
data_nr_base = filter(data, ActivityId == "V02_1" & Randomisation == "NR")
data_pl_base = filter(data, ActivityId == "V02_1" & Randomisation == "Placebo")
nr = data_nr_base[, "1.VSHEIGHT1"]
pl = data_pl_base[, "1.VSHEIGHT1"]
line = 3
Demographics[line,"Value"] = "Height"
Demographics[line,"Unit"] = "(cm)"

sumNR = summary(as.numeric(unlist(nr)))
sdNR = sd(as.numeric(unlist(nr)))
sumPL = summary(as.numeric(unlist(pl)))
sdPL = sd(as.numeric(unlist(pl)))
Ttest = t.test(as.numeric(unlist(pl)), as.numeric(unlist(nr)), alternative = "two.sided", var.equal=TRUE)
Wilcox = wilcox.test(as.numeric(unlist(pl)), as.numeric(unlist(nr)), alternative = "two.sided", var.equal=TRUE)
Demographics[line,"Placebo_mean"] = sumPL[4]
Demographics[line,"Placebo_sd"] = sdPL
Demographics[line,"NR_mean"] = sumNR[4]
Demographics[line,"NR_sd"] = sdNR
Demographics[line,"P_value_t_test"] = Ttest$p.value
Demographics[line,"Method_t"] = Ttest$method
Demographics[line,"P_value_wilcox"] = Wilcox$p.value
Demographics[line,"Method_w"] = Wilcox$method

#Weight analysis
class(data[["1.VSWEIGHT1"]])<-"numeric"
data_nr_base = filter(data, ActivityId == "V02_1" & Randomisation == "NR")
data_pl_base = filter(data, ActivityId == "V02_1" & Randomisation == "Placebo")
pl = data_pl_base[, "1.VSWEIGHT1"]
nr = data_nr_base[, "1.VSWEIGHT1"]
line = 4
Demographics[line,"Value"] = "Weight"
Demographics[line,"Unit"] = "(kg)"

sumNR = summary(as.numeric(unlist(nr)))
sdNR = sd(as.numeric(unlist(nr)))
sumPL = summary(as.numeric(unlist(pl)))
sdPL = sd(as.numeric(unlist(pl)))
Ttest = t.test(as.numeric(unlist(pl)), as.numeric(unlist(nr)), alternative = "two.sided", var.equal=TRUE)
Wilcox = wilcox.test(as.numeric(unlist(pl)), as.numeric(unlist(nr)), alternative = "two.sided", var.equal=TRUE)
Demographics[line,"Placebo_mean"] = sumPL[4]
Demographics[line,"Placebo_sd"] = sdPL
Demographics[line,"NR_mean"] = sumNR[4]
Demographics[line,"NR_sd"] = sdNR
Demographics[line,"P_value_t_test"] = Ttest$p.value
Demographics[line,"Method_t"] = Ttest$method
Demographics[line,"P_value_wilcox"] = Wilcox$p.value
Demographics[line,"Method_w"] = Wilcox$method

#Drug compliance analysis

class(data[["1.DACOMPL"]])<-"numeric"
data_nr_base = filter(data, ActivityId == "V09_2" & Randomisation == "NR")
data_pl_base = filter(data, ActivityId == "V09_2" & Randomisation == "Placebo")
pl = data_pl_base[, "1.DACOMPL"]
nr = data_nr_base[, "1.DACOMPL"]
line = 5
Demographics[line,"Value"] = "Drug complicance"
Demographics[line,"Unit"] = "(%)"

sumNR = summary(as.numeric(unlist(nr)))
sdNR = sd(as.numeric(unlist(nr)))
sumPL = summary(as.numeric(unlist(pl)))
sdPL = sd(as.numeric(unlist(pl)))
Ttest = t.test(as.numeric(unlist(pl)), as.numeric(unlist(nr)), alternative = "two.sided", var.equal=TRUE)
Wilcox = wilcox.test(as.numeric(unlist(pl)), as.numeric(unlist(nr)), alternative = "two.sided", var.equal=TRUE)
Demographics[line,"Placebo_mean"] = sumPL[4]
Demographics[line,"Placebo_sd"] = sdPL
Demographics[line,"NR_mean"] = sumNR[4]
Demographics[line,"NR_sd"] = sdNR
Demographics[line,"P_value_t_test"] = Wilcox$p.value
Demographics[line,"Method_t"] = Wilcox$method
Demographics[line,"P_value_wilcox"] = Wilcox$p.value
Demographics[line,"Method_w"] = Wilcox$method

# Years of PD diagnosis analysis
PD_diagnosis = filter(data, EventId == "CE04" & data[,"1.MHTERM"] == "Parkinsons sykdom")
PD_diagnose_year = c(2015, 2014, 2018, 2019, 2013, 2018, 2020, 2017, 2012, 2020, 2014, 2014, 2009, 2012, 2018, 2018, 2020, 2020, 2009, 2017)
PD_year = cbind(PD_diagnosis, PD_diagnose_year)
data_pl = filter(PD_year, Randomisation == "Placebo")
data_nr = filter(PD_year, Randomisation == "NR")
year_pl = data_pl[, "PD_diagnose_year"]
year_nr = data_nr[, "PD_diagnose_year"]
pl = as_tibble(year_pl)
nr = as_tibble(year_nr)
yearColumn = rbind(nr, pl)
PD_year = cbind(PD_year, yearColumn)
PD_year[,"value"] = 2022 - PD_year[,"value"] 
#calculate years since diagnosis
pl = 2022 - pl 
nr = 2022 - nr
line = 6 
Demographics[line,"Value"] = "Time since PD diagnosis"
Demographics[line,"Unit"] = "(years)"
sumPL = summary(as.numeric(unlist(pl)))
sdPL = sd(as.numeric(unlist(pl)))
sumNR = summary(as.numeric(unlist(nr)))
sdNR = sd(as.numeric(unlist(nr)))
Demographics[line,"Placebo_mean"] = sumPL[4]
Demographics[line,"Placebo_sd"] = sdPL
Demographics[line,"NR_mean"] = sumNR[4]
Demographics[line,"NR_sd"] = sdNR
Demographics[line,"P_value_t_test"] = Ttest$p.value
Demographics[line,"Method_t"] = Ttest$method
Demographics[line,"P_value_wilcox"] = Wilcox$p.value
Demographics[line,"Method_w"] = Wilcox$method

# BMI analysis
class(data[["1.VSBMI1"]])<-"numeric"
data_nr_base = filter(data, ActivityId == "V02_1" & Randomisation == "NR")
data_pl_base = filter(data, ActivityId == "V02_1" & Randomisation == "Placebo")
pl = data_pl_base[, "1.VSBMI1"]
nr = data_nr_base[, "1.VSBMI1"]
line = 7
Demographics[line,"Value"] = "BMI"
Demographics[line,"Unit"] = "(m/kg*2)"

sumNR = summary(as.numeric(unlist(nr)))
sdNR = sd(as.numeric(unlist(nr)))
sumPL = summary(as.numeric(unlist(pl)))
sdPL = sd(as.numeric(unlist(pl)))
Ttest = t.test(as.numeric(unlist(pl)), as.numeric(unlist(nr)), alternative = "two.sided", var.equal=TRUE)
Wilcox = wilcox.test(as.numeric(unlist(pl)), as.numeric(unlist(nr)), alternative = "two.sided", var.equal=TRUE)
Demographics[line,"Placebo_mean"] = sumPL[4]
Demographics[line,"Placebo_sd"] = sdPL
Demographics[line,"NR_mean"] = sumNR[4]
Demographics[line,"NR_sd"] = sdNR
Demographics[line,"P_value_t_test"] = Ttest$p.value
Demographics[line,"Method_t"] = Ttest$method
Demographics[line,"P_value_wilcox"] = Wilcox$p.value
Demographics[line,"Method_w"] = Wilcox$method

table_demographics = Demographics[,c("Value", "Unit", "Placebo_mean", "Placebo_sd", "NR_mean", "NR_sd", "P_value_t_test")]
colnames(table_demographics) = c("Value", "Unit", "PL_mean", "PL_sd", "NR_mean", "NR_sd", "p-value") 

table_demographics$Variable = paste0(table_demographics[,1]," ",table_demographics[,2])
outTableDemo = table_demographics[,c("Variable", "PL_mean", "PL_sd", "NR_mean", "NR_sd", "p-value")]
outTableUp = baseBase_up_wTimes[,c("Name", "PL_BL_Mean", "PL_BL_Sd", "NR_BL_Mean", "NR_BL_Sd", "T_Test")]
colnames(outTableDemo) = c("Name", "PL_BL_Mean", "PL_BL_Sd", "NR_BL_Mean", "NR_BL_Sd", "p-value")
colnames(outTableUp) = c("Name", "PL_BL_Mean", "PL_BL_Sd", "NR_BL_Mean", "NR_BL_Sd", "p-value")
table_demographics = rbind(outTableDemo, outTableUp)
table_demographics[2:13,2] = as.numeric(table_demographics[2:13,2])
table_demographics[2:13,3] = as.numeric(table_demographics[2:13,3])
table_demographics[2:13,4] = as.numeric(table_demographics[2:13,4])
table_demographics[2:13,5] = as.numeric(table_demographics[2:13,5])
table_demographics[2:13,6] = as.numeric(table_demographics[2:13,6])

Formatted_demographics = FormattedTableRoundedDemographics(table_demographics)


# Adverse events (ae) analysis (Description of medical terms in Norwegian):
# Tables were made manually based on this data:
ae = data_ae
NR_ae = filter(ae, ae[,"Randomisation"] == "NR")
PL_ae = filter(ae, ae[,"Randomisation"] == "Placebo")
ae_frame = data_ae[,c("SubjectId", "Randomisation", "AETERM", "AESEV", "AEREL", "AERELCD", "AESTDTC", "AEENDTC", "AEONGO", "AEACN", "AEOUT", "AEOUTCD", "AECONTRT")]
colnames(ae_frame) = c("SubjectId", "Randomisation", "Adverse_event", "Severity", "Causal_relationship", "Causal_relationship_code", "Event_onset", "Event_end", "Ongoing?", "Action_taken", "Outcome", "Outcome_code", "Another_treatment_given?")


# Formats data frames to tables:

FormattedTableRoundedWithAdjPValue_vs = FormattedTableRoundedWithAdjPValue(vs_corr)
FormattedTableRoundedWithAdjPValue_sr = FormattedTableRoundedWithAdjPValue(sr_corr)
FormattedTableRoundedWithAdjPValue_up_wTimes = FormattedTableRoundedWithAdjPValue(upWithTime_corr)
FormattedTableRoundedWithAdjPValue_up_subset = FormattedTableRoundedWithAdjPValue(up_subset_corr)
FormattedTableRoundedWithAdjPValue_nad = FormattedTableRoundedWithAdjPValue(nad_corr)
FormattedTableOmicsFilterLODsRounded_blood = FormattedTableOmicsFilterLODsRounded(ms_bl_req_corr_filtered)
FormattedTableOmicsFilterLODsRounded_urine = FormattedTableOmicsFilterLODsRounded(ms_ur_req_corr_filtered)

# makes formatted dataframes from values where NAs are sorted out from analysis and in metabolomics values below limit of detection are filtered out, maximum allowed lower than LODS is 3
FormatTableVS = FormattedTable(vs_corr)
FormatTableSR = FormattedTableWithAdjPValue(sr_corr)
FormatTableNAD = FormattedTableWithAdjPValue(nad_corr)
FormatTableUP_wTimes = FormattedTableWithAdjPValue(upWithTime_corr)
FormatTableUP_subset = FormattedTableWithAdjPValue(up_subset_corr)
FormatTableMS_blood_LODs = FormattedTableOmicsFilterLODs(ms_bl_req_corr_filtered)
FormatTableMS_urine_LODs = FormattedTableOmicsFilterLODs(ms_ur_req_corr_filtered)

# This is made for comments for missing data in the excel files: 
a_com = vs_corr[,c("Name", "Number_analysed_PL", "Number_analysed_NR")]
b_com = upWithTime_corr[,c("Name", "Number_analysed_PL", "Number_analysed_NR")]
c_com = sr_corr[,c("Name", "Number_analysed_PL", "Number_analysed_NR")]
d_com = nad_corr[,c("Name", "Number_analysed_PL", "Number_analysed_NR")]
e_com = ms_bl_req_corr_filtered[,c("Name", "Number_subjectId_analysed_PL", "Number_subjectId_analysed_NR")]
f_com = ms_ur_req_corr_filtered[,c("Name", "Number_subjectId_analysed_PL", "Number_subjectId_analysed_NR")]

commentFrame = data.frame("Name" = "", "SubjectIDs_PL_analysed" = "", "SubjectIDs_NR_analysed" = "")
colnames(a_com) = c("Name", "SubjectIDs_PL_analysed", "SubjectIDs_NR_analysed")
colnames(b_com) = c("Name", "SubjectIDs_PL_analysed", "SubjectIDs_NR_analysed")
colnames(c_com) = c("Name", "SubjectIDs_PL_analysed", "SubjectIDs_NR_analysed")
colnames(d_com) = c("Name", "SubjectIDs_PL_analysed", "SubjectIDs_NR_analysed")
colnames(e_com) = c("Name", "SubjectIDs_PL_analysed", "SubjectIDs_NR_analysed")
colnames(f_com) = c("Name", "SubjectIDs_PL_analysed", "SubjectIDs_NR_analysed")
commentFrame = rbind(commentFrame, a_com)
commentFrame = rbind(commentFrame, b_com)
commentFrame = rbind(commentFrame, c_com)
commentFrame = rbind(commentFrame, d_com)
commentFrame = rbind(commentFrame, e_com)
commentFrame = rbind(commentFrame, f_com)
commentFrame[1,] = c("Values listed below are how many individual SubjectIDs that do not have a NA value or a single value lower than LOD. Cutoff for analysis was less than 7 samples for each group.", "", "")

# Formats baseline comparison dataFrames for output: 
baselineCompVS = FormattedBaselineComparisons(baseBase_vs) 
baselineCompUP_wTimes = FormattedBaselineComparisons(baseBase_up_wTimes)
baselineCompUP_subset = FormattedBaselineComparisons(baseBase_up_subset)
baselineCompSR = FormattedBaselineComparisons(baseBase_sr)
baselineCompNAD = FormattedBaselineComparisons(baseBase_nad) 
baselineCompMSblood = FormattedBaselineComparisons(baseBase_MSblood)
baselineCompMSurine = FormattedBaselineComparisons(baseBase_MSurine)
