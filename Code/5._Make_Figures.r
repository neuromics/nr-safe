
# Requires NR_SAFE_Functions.r, Load_data.r and Data_analysis.r scripts to have been run first:

# sets directory to output directory 
setwd(outDir)
# makes folder where  all figures will be located: 
dir.create("Figures")
setwd(paste0(outDir,"/Figures"))

# set the color of the placebo and NR groups for functions: makePairedPlotNoFacets, makePairedPlotLODSremovedNoFacets and LineAverageValue_NO_FACETS:
#color_placebo 	= "royalblue3"
color_placebo 	= "steelblue"
color_NR 		= "tomato3"

# set the color of the placebo and NR groups for function: makeSingleBoxPlotNO_FACETS:
color_placebo_boxplot	= "royalblue2"
color_placebo_boxplot	= "gray43"
color_NR_boxplot 		= "tomato3"

# These sets the size of different parts of plotted graphs: 
# set the size of labels (main titles, x- and y-axis labels)
sizeOfLabels = 5
# set the size of points in the plots:
sizeOfPoints = 0.2
# set the size of lines in the plots: 
sizeOfLines = 0.1
# set the size of axis lines in the plots: 
sizeOfAxisLines = 0.2

# This makes a plot that is used to get the legend which is later added manually to the plots:
dud = data_sr_filtered
dud = filter(dud, dud[,"ActivityId"] == "V02_1" | dud[,"ActivityId"] == "V09_1" )
legend = LineAverageValue_NO_FACETS("SR1_Copy17_Copy1", dud, "Homocysteine", "µmol/L", FALSE, FALSE)
legend = legend + theme_void() + labs(title = "")
# prints a powerpoint with the plot "legend" above: 
doc <- read_pptx()
print_to_pdf_vector_sizeOption(legend, 1, 1)
print(doc, target = "Legend for PL and NR.pptx")

# Below the actual figures for the article are made: 
# The figures were first printed into a powerpoint document and then were subjected to manual editing to format the graphs properly:

# Figure 2:  MDS-UPDRS and subset of MDS-UPDRS with similar times 
# makes plots of MDS-UPDRS subscores and total score for all individuals:
up1_paired = makePairedPlotNoFacets("UPDRS Part 1", up_rawDataWithTimes, "t.test", up_listWithTimes, upWithTime_corr, FALSE, FALSE) + theme(plot.title = element_text(vjust = -3))
up2_paired = makePairedPlotNoFacets("UPDRS Part 2", up_rawDataWithTimes, "t.test", up_listWithTimes, upWithTime_corr, FALSE, FALSE) + theme(plot.title = element_text(vjust = -3))
up3_paired = makePairedPlotNoFacets("UPDRS Part 3", up_rawDataWithTimes, "t.test", up_listWithTimes, upWithTime_corr, FALSE, FALSE) + theme(plot.title = element_text(vjust = -3))
up4_paired = makePairedPlotNoFacets("UPDRS Part 4", up_rawDataWithTimes, "t.test", up_listWithTimes, upWithTime_corr, FALSE, FALSE) + theme(plot.title = element_text(vjust = -3))
upT_paired = makePairedPlotNoFacets("UPDRS Total", up_rawDataWithTimes, "t.test", up_listWithTimes, upWithTime_corr, FALSE, FALSE) + theme(plot.title = element_text(vjust = -3))
# makes plots of total MDS-UPDRS scores with specific individuals filtered out for more even time intervals between the placebo and NR groups:
data_up_subset = filter(data_up, data_up[,"SubjectId"] != "01-20")
data_up_subset = filter(data_up_subset, data_up_subset[,"SubjectId"] != "01-5")
data_up_subset = filter(data_up_subset, data_up_subset[,"SubjectId"] != "01-6")
data_up_subset = filter(data_up_subset, data_up_subset[,"SubjectId"] != "01-8")
up_subset_frame = combine(data_up_subset, up_listWithTimes)
up_subset_mult = multipleTestingCorrectionFDR(up_subset_frame)
up_subset_corr = merge(up_subset_frame, up_subset_mult, by = "Name")
up_subset_rawData = combineData(data_up_subset, up_listWithTimes)
colnames(up_subset_rawData) = c("SubjectId", "Randomisation", "ActivityId", up_listWithTimes[2,2], up_listWithTimes[2,3], up_listWithTimes[2,4], up_listWithTimes[2,5], up_listWithTimes[2,6], up_listWithTimes[2,7], up_listWithTimes[2,8], up_listWithTimes[2,9], up_listWithTimes[2,10], up_listWithTimes[2,11])
up_sub_pairedT = makePairedPlotNoFacets("UPDRS Total", up_subset_rawData, "t.test", up_listWithTimes, up_subset_corr, FALSE, FALSE) + scale_x_discrete(labels=c("PL_BL" = "v1", "PL_V09" = "v7", "NR_BL" = "v1", "NR_V09" = "v7")) + labs(title = "Total MDS-UPDRS") + theme(plot.title = element_text(vjust = -3))
# Formats the sub-graphs by adding labels and titles:
up1_paired = up1_paired + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + labs(title = "MDS-UPDRS Part I")  + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))
up2_paired = up2_paired + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + labs(title = "MDS-UPDRS Part II")  + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))
up3_paired = up3_paired + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + labs(title = "MDS-UPDRS Part III")  + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))
up4_paired = up4_paired + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + labs(title = "MDS-UPDRS Part IV")  + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))
upT_paired = upT_paired + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + labs(title = "Total MDS-UPDRS")  + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) #\n(All participants)
up_sub_pairedT = up_sub_pairedT + labs(title = "Total MDS-UPDRS (Subset)") + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) #, NR, n=8, PL, n=8)
# Combines sub-graphs into one figure laid out in a grid:
fig_2 = plot_grid(upT_paired, up1_paired, up2_paired, up3_paired, up4_paired, up_sub_pairedT, ncol = 3, labels="AUTO", align = "hv", label_size = 6)

fig_2

#axis = "l",

# Prints the figure above into a powerpoint document to allow for editing:
doc <- read_pptx()
print_to_pdf_vector_sizeOption(fig_2, 2, 3)
print(doc, target = "Figure 2. MDS-UPDRS scores_structured.pptx")

# Figure 3: NADmed analysis 
# This converts changes the scale of values GSH, GSSG, GSH pool and GSH/GSSG to µM to allow for clearer visualization:
nadData = nad_rawData
nadData[,"GSH"] = nadData[,"GSH"] * 1000
nadData[,"GSSG"] = nadData[,"GSSG"] * 1000
nadData[,"GSH pool"] = nadData[,"GSH pool"] * 1000
# changes unit labels to µM:
nad_list_for_figure = nad_list
uMList = nad_list_for_figure[3,]
uMList = c("Unit", "µM", "µM", "µM", "Ratio", "µM", "µM", "µM", "Ratio", "µM","µM","µM","Ratio")
nad_list_for_figure[3,] = uMList
# makes sub-plots for the figure: 
nad1 = makePairedPlotNoFacets("NAD+", nadData, "t.test", nad_list_for_figure, nad_corr, TRUE, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + labs(title = expression(bold(NAD^"+"))) + theme(axis.title = element_text(face="bold"))  + theme(plot.title = element_text(vjust = -3))
nad2 = makePairedPlotNoFacets("NADH", nadData, "t.test", nad_list_for_figure, nad_corr, TRUE, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))  + theme(plot.title = element_text(vjust = -3))
nad3 = makePairedPlotNoFacets("NAD+ pool", nadData, "t.test", nad_list_for_figure, nad_corr, TRUE, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + labs(title = "Total NAD") + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))  + theme(plot.title = element_text(vjust = -3))
nad4 = makePairedPlotNoFacets("NAD+/NADH", nadData, "t.test", nad_list_for_figure, nad_corr, TRUE, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + labs(title = expression(bold(NAD^"+"~"/"~NADH)))  + theme(axis.title = element_text(face="bold"))  + theme(plot.title = element_text(vjust = -3))
nad5 = makePairedPlotNoFacets("NADP+", nadData, "t.test", nad_list_for_figure, nad_corr, TRUE, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + labs(title = expression(bold(NADP^"+")))  + theme(axis.title = element_text(face="bold"))  + theme(plot.title = element_text(vjust = -3))
nad6 = makePairedPlotNoFacets("NADPH", nadData, "t.test", nad_list_for_figure, nad_corr, TRUE, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))  + theme(plot.title = element_text(vjust = -3))
nad7 = makePairedPlotNoFacets("NADP+ pool", nadData, "t.test", nad_list_for_figure, nad_corr, TRUE, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + labs(title = "Total NADP") + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))  + theme(plot.title = element_text(vjust = -3))
#nad8 = makePairedPlotNoFacets("NADP+/NADPH", nadData, "t.test", nad_list_for_figure, nad_corr, TRUE, FALSE)+ theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + labs(title = expression(bold(NADP^"+"~"/"~NADPH))) + theme(axis.title = element_text(face="bold"))
nad8 = makePairedPlotNoFacets("NADP+/NADPH", nadData, "t.test", nad_list_for_figure, nad_corr, TRUE, FALSE)+ theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + labs(title = "NADP+/NAPH") + theme(axis.title = element_text(face="bold"))  + theme(plot.title = element_text(vjust = -3))
nad9 = makePairedPlotNoFacets("GSH", nadData, "t.test", nad_list_for_figure, nad_corr, TRUE, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))  + theme(plot.title = element_text(vjust = -3))
nad10 = makePairedPlotNoFacets("GSSG", nadData, "t.test", nad_list_for_figure, nad_corr, TRUE, FALSE)+ theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))  + theme(plot.title = element_text(vjust = -3))
nad11 = makePairedPlotNoFacets("GSH pool", nadData, "t.test", nad_list_for_figure, nad_corr, TRUE, FALSE)+ theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + labs(title = "Total GSH") + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))  + theme(plot.title = element_text(vjust = -3))
nad12 = makePairedPlotNoFacets("GSH/GSSG", nadData, "t.test", nad_list_for_figure, nad_corr, TRUE, FALSE)+ theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + labs(title = "GSH/GSSG") + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))  + theme(plot.title = element_text(vjust = -3))
# combines sub-plots into figure:  
Fig_3 = plot_grid(nad1, nad2, nad3, nad4, nad5, nad6, nad7, nad8, nad9, nad10, nad11, nad12, ncol = 4, align="hv", labels = "AUTO", label_size = 6)
Fig_3


#axis="l"
# Prints the figure above into a powerpoint document to allow for editing:
doc <- read_pptx()
print_to_pdf_vector_sizeOption(Fig_3, 3, 4)
print(doc, target = "Figure 3. NAD and NADP levels in whole blood_structured.pptx")




# Analysis of 
a = nadData[,c("SubjectId", "Randomisation", "ActivityId", "NADP+/NADPH")]
a = a[c(-60, -43, -17, -18, -11, -12),]
nr_base = filter(a, a[,"Randomisation"] == "NR" & a[,"ActivityId"] == "V02_1")
nr_end = filter(a, a[,"Randomisation"] == "NR" & a[,"ActivityId"] == "V09_1")
t.test(nr_base[,4], nr_end[,4], paired = TRUE, two.sided = TRUE)

#Figure 4: LCMS whole blood and urine: 
# Amount_of_LOD sets the number of values that have to be above the limit of detection (LOD) to be included in the analysis: 
# If only one randomisation is shown, the other did not have 7 or more values that were not above LOD or not NA to be analysed. 
Amount_of_LOD = 7
# WHOLE BLOOD: 

#format NR unit from µM to nM:
conversionMetabolomics_bl = ms_bl_req_rawData 
conversionMetabolomicsList_bl = list_var_ms_blood_req
conversionMetabolomics_bl[,"Nicotinamide riboside"] = as.numeric(unlist(conversionMetabolomics_bl[,"Nicotinamide riboside"])) * 1000
conversionMetabolomicsList_bl[3,"Nicotinamide riboside"] = "nM"
conversionMetabolomicsList_bl[6,"Nicotinamide riboside"] = as.numeric(unlist(conversionMetabolomicsList_bl[6,"Nicotinamide riboside"])) * 1000

NR_bl = makePairedPlotLODSremovedNoFacets("Nicotinamide riboside", conversionMetabolomics_bl, "t.test", conversionMetabolomicsList_bl, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) + labs(title = "NR") + theme(plot.title = element_text(vjust = -3))
NAR_bl = makePairedPlotLODSremovedNoFacets("Nicotinic acid ribonucleoside", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + labs(title = "NAR") + theme(title = element_text(face = "bold")) + theme(plot.title = element_text(vjust = -3))
NAM_bl = makePairedPlotLODSremovedNoFacets("Nicotine amide", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7"))+ theme(axis.title = element_text(face="bold"))  + theme(title = element_text(face = "bold")) + labs(title = "Nam")+ theme(plot.title = element_text(vjust = -3))
NMN_bl = makePairedPlotLODSremovedNoFacets("Nicotinamide ribotide", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) + labs(title = "NMN")+ theme(plot.title = element_text(vjust = -3))
NAAD_bl = makePairedPlotLODSremovedNoFacets("Nicotinic acid-adenine dinucleotide", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7"))+ theme(axis.title = element_text(face="bold"))  + theme(title = element_text(face = "bold")) + labs(title = "NAAD")+ theme(plot.title = element_text(vjust = -3))
NAD_bl = makePairedPlotLODSremovedNoFacets("NAD+", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + labs(title = expression(bold(NAD^"+"))) + theme(axis.title = element_text(face="bold"))+ theme(plot.title = element_text(vjust = -3))
Adenosine_bl = makePairedPlotLODSremovedNoFacets("Adenosine", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) + labs(title = "Adenosine")+ theme(plot.title = element_text(vjust = -3))
ADP_bl = makePairedPlotLODSremovedNoFacets("ADP", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7"))+ theme(axis.title = element_text(face="bold"))  + theme(title = element_text(face = "bold")) + labs(title = "ADP")+ theme(plot.title = element_text(vjust = -3))
ATP_bl = makePairedPlotLODSremovedNoFacets("ATP", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7"))+ theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) + labs(title = "ATP")+ theme(plot.title = element_text(vjust = -3))
GDP_bl = makePairedPlotLODSremovedNoFacets("GDP", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7"))+ theme(axis.title = element_text(face="bold"))  + theme(title = element_text(face = "bold")) + labs(title = "GDP")+ theme(plot.title = element_text(vjust = -3))
GTP_bl = makePairedPlotLODSremovedNoFacets("GTP", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7"))+ theme(axis.title = element_text(face="bold"))  + theme(title = element_text(face = "bold")) + labs(title = "GTP")+ theme(plot.title = element_text(vjust = -3))
Me_nam_bl = makePairedPlotLODSremovedNoFacets("1-Methyl Nicotinamide", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7"))+ theme(axis.title = element_text(face="bold"))  + theme(title = element_text(face = "bold")) + labs(title = "Me-Nam")+ theme(plot.title = element_text(vjust = -3))
Me_2_py_bl = makePairedPlotLODSremovedNoFacets("N1-Methyl-2-pyridone-5-carboxamide", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) + labs(title = "Me-2-PY")+ theme(plot.title = element_text(vjust = -3))
Nam_n_ox_bl = makePairedPlotLODSremovedNoFacets("Nicotinamide N-oxide", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) + labs(title = "Nam N-oxide")+ theme(plot.title = element_text(vjust = -3))
ADPR_bl = makePairedPlotLODSremovedNoFacets("Adenosine diphosphate ribose", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) + labs(title = "ADPR")+ theme(plot.title = element_text(vjust = -3))
#cADPR_bl = makePairedPlotLODSremovedNoFacets("Cyclic ADP-ribose", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, TRUE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7"))+ theme(axis.title = element_text(face="bold"))  + theme(title = element_text(face = "bold")) + labs(title = "cADPR")+ theme(axis.text.y = element_text(angle = 90))+ theme(plot.title = element_text(vjust = -3)) + labs(y = "Relative intensity")
AMP_bl = makePairedPlotLODSremovedNoFacets("AMP", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) + labs(title = "AMP")+ theme(plot.title = element_text(vjust = -3))
NADP_bl = makePairedPlotLODSremovedNoFacets("NADP+", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, TRUE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + labs(title = expression(bold(NADP^"+")))+ theme(axis.text.y = element_text(angle = 90))+ theme(plot.title = element_text(vjust = -3))
# NR has (NR n= 8, PL n = 9)
# NAAD has  has (NR n= 9, PL n = 10)
# All other of these have  (NR n= 10, PL n = 10)
# URINE

#format Me-Nam unit from µM to mM:
conversionMetabolomics_ur = ms_ur_req_rawData
conversionMetabolomicsList_ur = list_var_ms_urine_req
conversionMetabolomics_ur[,"1-Methyl Nicotinamide"] = as.numeric(unlist(conversionMetabolomics_ur[,"1-Methyl Nicotinamide"])) / 1000
conversionMetabolomicsList_ur[3,"1-Methyl Nicotinamide"] = "mM"
conversionMetabolomicsList_ur[6,"1-Methyl Nicotinamide"] = as.numeric(unlist(conversionMetabolomicsList_ur[6,"1-Methyl Nicotinamide"])) / 1000

#color_NR 		= "orange2"
Me_nam_ur = makePairedPlotLODSremovedNoFacets("1-Methyl Nicotinamide", conversionMetabolomics_ur, "t.test", conversionMetabolomicsList_ur, ms_ur_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))
Me_nam_ur = Me_nam_ur + labs(title = "Me-Nam")+ theme(plot.title = element_text(vjust = -3))
Me_2_py_ur = makePairedPlotLODSremovedNoFacets("N1-Methyl-2-pyridone-5-carboxamide", ms_ur_req_rawData, "t.test", list_var_ms_urine_req, ms_ur_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))
Me_2_py_ur = Me_2_py_ur + labs(title = "Me-2-PY")+ theme(plot.title = element_text(vjust = -3))
Nam_N_ox_ur = makePairedPlotLODSremovedNoFacets("Nicotineamide N-oxide", ms_ur_req_rawData, "wilcox.test", list_var_ms_urine_req, ms_ur_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))
Nam_N_ox_ur = Nam_N_ox_ur + labs(title = "Nam N-oxide")+ theme(plot.title = element_text(vjust = -3))
NAM_ur = makePairedPlotLODSremovedNoFacets("Nicotine amide", ms_ur_req_rawData, "t.test", list_var_ms_urine_req, ms_ur_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))
NAM_ur = NAM_ur + labs(title = "Nam")+ theme(plot.title = element_text(vjust = -3))
NR_ur = makePairedPlotLODSremovedNoFacets("Nicotinamide riboside", ms_ur_req_rawData, "wilcox.test", list_var_ms_urine_req, ms_ur_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))
NR_ur = NR_ur + labs(title = "NR")+ theme(plot.title = element_text(vjust = -3))
NAR_ur = makePairedPlotLODSremovedNoFacets("Nicotinic acid ribonucleoside", ms_ur_req_rawData, "t.test", list_var_ms_urine_req, ms_ur_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))
NAR_ur = NAR_ur + labs(title = "NAR")+ theme(plot.title = element_text(vjust = -3))
NMN_ur = makePairedPlotLODSremovedNoFacets("Nicotinamide ribotide", ms_ur_req_rawData, "t.test", list_var_ms_urine_req, ms_ur_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))
NMN_ur = NMN_ur + labs(title = "NMN") + theme(plot.title = element_text(vjust = -3))
# Nam has (NR n= 9, PL n = 4)
# All other have (NR n= 10, PL n = 9)
# combines sub-plots into figure:  
#Figure_5_panels_5_col_adrp_at_end = plot_grid(NAR_bl, NR_bl, NMN_bl, NAAD_bl, NAD_bl, NADP_bl, NAM_bl, Nam_n_ox_bl, Me_nam_bl, Me_2_py_bl, AMP_bl, ADP_bl, ATP_bl, Adenosine_bl, GTP_bl, GDP_bl, ADPR_bl, cADPR_bl, NULL, NULL, NAR_ur, NR_ur, NAM_ur, NMN_ur, Nam_N_ox_ur, Me_nam_ur, Me_2_py_ur, ncol = 5, labels = c("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "", "", "S", "T", "U", "V", "W", "X", "Y"), align="hv", label_size = 6)
Figure_5_panels_5_col_adrp_at_end = plot_grid(NAR_bl, NR_bl, NMN_bl, NAAD_bl, NAD_bl, NADP_bl, NAM_bl, Nam_n_ox_bl, Me_nam_bl, Me_2_py_bl, AMP_bl, ADP_bl, ATP_bl, Adenosine_bl, GTP_bl, GDP_bl, ADPR_bl, NULL, NULL, NULL, NAR_ur, NR_ur, NAM_ur, NMN_ur, Nam_N_ox_ur, Me_nam_ur, Me_2_py_ur, ncol = 5, labels = c("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "", "", "S", "T", "U", "V", "W", "X", "Y"), align="hv", label_size = 6)

 #axis="l",

Figure_5_panels_5_col_adrp_at_end

# Prints the figure above into a powerpoint document to allow for editing:
doc <- read_pptx()
print_to_pdf_vector_sizeOption(Figure_5_panels_5_col_adrp_at_end, 6, 5)
print(doc, target = "Figure 4. LCMS panels 5 col adrp at end.pptx")



# Figure 5: Homocysteine and methyl donor pool analysis figure: 
color_NR 		= "tomato3"
lcms_bl_SAM = makePairedPlotLODSremovedNoFacets("S-Adenosylmethionine", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) + labs(title = "SAM (whole blood)")+ theme(plot.title = element_text(vjust = -3))
lcms_bl_SAH = makePairedPlotLODSremovedNoFacets("S-Adenosylhomocysteine", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) + labs(title = "SAH (whole blood)")+ theme(plot.title = element_text(vjust = -3))
lcms_bl_Hcy = makePairedPlotLODSremovedNoFacets("Homocysteine", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, TRUE) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) + labs(title = "HCy (whole blood)") + labs(y = "Relative intensity") + theme(axis.text.y = element_text(angle = 90))+ theme(plot.title = element_text(vjust = -3))
lcms_bl_SAM_SAH = makePairedPlotLODSremovedNoFacets("SAM_SAH ratio", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) + labs(title = "SAM/SAH (whole blood)") + labs(y = "Ratio")+ theme(plot.title = element_text(vjust = -3))
homocysteine_paired_plot = makePairedPlotNoFacets("Homocysteine", sr_rawDataFiltered, "t.test", sr_list, sr_corr, TRUE, FALSE) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) + labs(title = "HCy (serum)") + labs(y = "µM")+ theme(plot.title = element_text(vjust = -3))
homocysteine_boxPlot = makeSingleBoxPlotNO_FACETS("Homocysteine", sr_rawDataFiltered, "t.test", sr_list, sr_corr, TRUE, FALSE) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) + labs(title = "Δ HCy (serum)") + labs(y = "µM")+ theme(plot.title = element_text(vjust = -3))
homocysteine_all_values = LineAverageValue_NO_FACETS("SR1_Copy17_Copy1", data_sr_filtered, "Homocysteine", "µmol/L", "WithStDev", FALSE) + scale_x_discrete(labels=c("V02" = "V1", "V04" = "V2", "V05" = "V3", "V06" = "V4", "V07" = "V5", "V08" = "V6", "V09" = "V7")) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) 
homocysteine_all_values = homocysteine_all_values + labs(title = "Mean HCy (serum)")
homocysteine_all_values = homocysteine_all_values + theme(legend.position="none") + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) + theme(plot.title = element_text(size= sizeOfLabels)) + theme(plot.title = element_text(hjust = 0.5)) + labs(y = "µM")+ theme(plot.title = element_text(vjust = -3))
#color_NR 		= "orange2"
lcms_ur_Hcy = makePairedPlotLODSremovedNoFacets("Homocysteine", ms_ur_req_rawData, "t.test", list_var_ms_urine_req, ms_ur_req_corr_filtered, TRUE, Amount_of_LOD, TRUE) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold"))  + labs(title = "HCy (urine)")  + labs(y = "Relative intensity")+ theme(axis.text.y = element_text(angle = 90))+ theme(plot.title = element_text(vjust = -3))
lcms_ur_SAM = makePairedPlotLODSremovedNoFacets("S-Adenosylmethionine", ms_ur_req_rawData, "t.test", list_var_ms_urine_req, ms_ur_req_corr_filtered, TRUE, Amount_of_LOD, FALSE) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) + labs(title = "SAM (urine)")+ theme(plot.title = element_text(vjust = -3))
color_NR 		= "tomato3"
meth_bl_2 = makePairedPlotLODSremovedNoFacets("Betaine", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, TRUE) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) + labs(title = "Betaine (whole blood)")  + labs(y = "Relative intensity")+ theme(axis.text.y = element_text(angle = 90))+ theme(plot.title = element_text(vjust = -3))
meth_bl_3 = makePairedPlotLODSremovedNoFacets("Methionine", ms_bl_req_rawData, "t.test", list_var_ms_blood_req, ms_bl_req_corr_filtered, TRUE, Amount_of_LOD, TRUE) + scale_x_discrete(labels=c("PL_BL" = "V1", "PL_V09" = "V7", "NR_BL" = "V1", "NR_V09" = "V7")) + theme(plot.margin = unit(c(0, 0, 0, 0), "cm")) + theme(axis.title = element_text(face="bold")) + theme(title = element_text(face = "bold")) + labs(title = "Methionine (whole blood)")  + labs(y = "Relative intensity")+ theme(axis.text.y = element_text(angle = 90))+ theme(plot.title = element_text(vjust = -3))
# Homocysteine and SAM in urine has (NR n= 10, PL n = 9)
# All other have (NR n= 10, PL n = 10)
# combines sub-plots into figure:
# This one has all on 5 lines:   
#fig_5x = plot_grid(homocysteine_paired_plot, homocysteine_boxPlot, homocysteine_all_values, lcms_bl_Hcy, lcms_bl_SAM, lcms_bl_SAH, lcms_bl_SAM_SAH, NULL, NULL, lcms_ur_Hcy, lcms_ur_SAM, NULL, meth_bl_2, meth_bl_3, ncol = 3, align = "hv", axis = "l", labels=c("A", "B", "C", "D", "E", "F", "G", "", "", "H", "I", "", "J", "K"), label_size = 6)
# This version has all on 4 lines: 
fig_5x_4x3 = plot_grid(homocysteine_paired_plot, homocysteine_boxPlot, homocysteine_all_values, NULL, lcms_bl_Hcy, lcms_bl_SAM, lcms_bl_SAH, lcms_bl_SAM_SAH, lcms_ur_Hcy, lcms_ur_SAM, meth_bl_2, meth_bl_3, ncol = 4, align = "hv", axis = "l", labels=c("A", "B", "C", "", "D", "E", "F", "G", "H", "I", "J", "K"), label_size = 6)
# Prints the figure above into a powerpoint document to allow for editing:
#doc <- read_pptx()
#print_to_pdf_vector_sizeOption(fig_5x, 5, 3)
#print(doc, target = "Figure 5. Homocysteine and methylgroups.pptx")
# Prints the figure above into a powerpoint document to allow for editing:
doc <- read_pptx()
print_to_pdf_vector_sizeOption(fig_5x_4x3, 3, 4)
print(doc, target = "Figure 5. Homocysteine and methylgroups alternative.pptx")

