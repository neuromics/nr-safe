
################################################
################################################# PRINTS Data frames IN EXCEL OR CSV FORMATS: 

setwd(outDir)
dir.create("Unformatted_Tables")
setwd(outDir)
dir.create("Formatted_Tables")

setwd(paste0(outDir,"/Formatted_Tables"))
outFile = "Baseline comparisons in formatted tables.xlsx"
out = createWorkbook()
addWorksheet(out, "VS")
addWorksheet(out, "SR")
addWorksheet(out, "UPDRS")
addWorksheet(out, "UPDRS_subset")
addWorksheet(out, "NADmed")
addWorksheet(out, "MS_omics_whole_blood")
addWorksheet(out, "MS_omics_urine")
writeData(out, sheet = "VS", x = baselineCompVS, rowNames = FALSE)
writeData(out, sheet = "SR", x = baselineCompSR, rowNames = FALSE)
writeData(out, sheet = "UPDRS", x = baselineCompUP_wTimes, rowNames = FALSE)
writeData(out, sheet = "UPDRS_subset", x = baselineCompUP_subset, rowNames = FALSE)
writeData(out, sheet = "NADmed", x = baselineCompNAD, rowNames = FALSE)
writeData(out, sheet = "MS_omics_whole_blood", x = baselineCompMSblood, rowNames = FALSE)
writeData(out, sheet = "MS_omics_urine", x = baselineCompMSurine, rowNames = FALSE)
saveWorkbook(out, outFile, overwrite = TRUE)


setwd(paste0(outDir,"/Unformatted_Tables"))
outFile = "Baseline comparisons in unformatted table.xlsx"
out = createWorkbook()
addWorksheet(out, "VS")
addWorksheet(out, "SR")
addWorksheet(out, "UPDRS")
addWorksheet(out, "UPDRS_subset")
addWorksheet(out, "NADmed")
addWorksheet(out, "MS_omics_whole_blood")
addWorksheet(out, "MS_omics_urine")
writeData(out, sheet = "VS", x = baseBase_vs, rowNames = FALSE)
writeData(out, sheet = "SR", x = baseBase_sr, rowNames = FALSE)
writeData(out, sheet = "UPDRS", x = baseBase_up_wTimes, rowNames = FALSE)
writeData(out, sheet = "UPDRS_subset", x = baseBase_up_subset, rowNames = FALSE)
writeData(out, sheet = "NADmed", x = baseBase_nad, rowNames = FALSE)
writeData(out, sheet = "MS_omics_whole_blood", x = baseBase_MSblood, rowNames = FALSE)
writeData(out, sheet = "MS_omics_urine", x = baseBase_MSurine, rowNames = FALSE)
saveWorkbook(out, outFile, overwrite = TRUE)

setwd(paste0(outDir,"/Formatted_Tables"))
outFile = "Analysed data in formatted and rounded tables.xlsx"
out = createWorkbook()
addWorksheet(out, "VS")
addWorksheet(out, "SR")
addWorksheet(out, "UPDRS")
addWorksheet(out, "UPDRS_subset")
addWorksheet(out, "NADmed")
addWorksheet(out, "MS_omics_whole_blood")
addWorksheet(out, "MS_omics_urine")
addWorksheet(out, "Comment")
writeData(out, sheet = "VS", x = FormattedTableRoundedWithAdjPValue_vs, rowNames = FALSE)
writeData(out, sheet = "SR", x = FormattedTableRoundedWithAdjPValue_sr, rowNames = FALSE)
writeData(out, sheet = "UPDRS", x = FormattedTableRoundedWithAdjPValue_up_wTimes, rowNames = FALSE)
writeData(out, sheet = "UPDRS_subset", x = FormattedTableRoundedWithAdjPValue_up_subset, rowNames = FALSE)
writeData(out, sheet = "NADmed", x = FormattedTableRoundedWithAdjPValue_nad, rowNames = FALSE)
writeData(out, sheet = "MS_omics_whole_blood", x = FormattedTableOmicsFilterLODsRounded_blood, rowNames = FALSE)
writeData(out, sheet = "MS_omics_urine", x = FormattedTableOmicsFilterLODsRounded_urine, rowNames = FALSE)
writeData(out, sheet = "Comment", x = commentFrame, rowNames = FALSE)
saveWorkbook(out, outFile, overwrite = TRUE)

setwd(paste0(outDir,"/Formatted_Tables"))
outFile = "Analysed data in formatted and unrounded tables.xlsx"
out = createWorkbook()
addWorksheet(out, "VS")
addWorksheet(out, "SR")
addWorksheet(out, "UPDRS")
addWorksheet(out, "UPDRS_subset")
addWorksheet(out, "NADmed")
addWorksheet(out, "MS_omics_whole_blood")
addWorksheet(out, "MS_omics_urine")
addWorksheet(out, "Comment")
writeData(out, sheet = "VS", x = FormatTableVS, rowNames = FALSE)
writeData(out, sheet = "SR", x = FormatTableSR, rowNames = FALSE)
writeData(out, sheet = "UPDRS", x = FormatTableUP_wTimes, rowNames = FALSE)
writeData(out, sheet = "UPDRS_subset", x = FormatTableUP_subset, rowNames = FALSE)
writeData(out, sheet = "NADmed", x = FormatTableNAD, rowNames = FALSE)
writeData(out, sheet = "MS_omics_whole_blood", x = FormatTableMS_blood_LODs, rowNames = FALSE)
writeData(out, sheet = "MS_omics_urine", x = FormatTableMS_urine_LODs, rowNames = FALSE)
writeData(out, sheet = "Comment", x = commentFrame, rowNames = FALSE)
saveWorkbook(out, outFile, overwrite = TRUE)

setwd(paste0(outDir,"/Unformatted_Tables"))
outFile = "Analysed data in unformatted tables.xlsx"
out = createWorkbook()
addWorksheet(out, "VS")
addWorksheet(out, "SR")
addWorksheet(out, "UPDRS")
addWorksheet(out, "UPDRS_subset")
addWorksheet(out, "NADmed")
addWorksheet(out, "MS_omics_whole_blood")
addWorksheet(out, "MS_omics_urine")
writeData(out, sheet = "VS", x = vs_corr, rowNames = FALSE)
writeData(out, sheet = "SR", x = sr_corr, rowNames = FALSE)
writeData(out, sheet = "UPDRS", x = upWithTime_corr, rowNames = FALSE)
writeData(out, sheet = "UPDRS_subset", x = up_subset_corr, rowNames = FALSE)
writeData(out, sheet = "NADmed", x = nad_corr, rowNames = FALSE)
writeData(out, sheet = "MS_omics_whole_blood", x = ms_bl_req_corr, rowNames = FALSE)
writeData(out, sheet = "MS_omics_urine", x = ms_ur_req_corr, rowNames = FALSE)
saveWorkbook(out, outFile, overwrite = TRUE)

outFile = "Requested LCMS metabolomics (values with more than 3 values below LOD removed).xlsx"
out = createWorkbook()
addWorksheet(out, "MS_omics_whole_blood")
addWorksheet(out, "MS_omics_urine")
writeData(out, sheet = "MS_omics_whole_blood", x = ms_bl_req_corr_filtered, rowNames = FALSE)
writeData(out, sheet = "MS_omics_urine", x = ms_ur_req_corr_filtered, rowNames = FALSE)
saveWorkbook(out, outFile, overwrite = TRUE)

# Write demographics data frame to file
setwd(outDir)	
outFile = "Demographics analysis.xlsx"
out = createWorkbook()
addWorksheet(out, "Formatted_demographics")
addWorksheet(out, "Unformatted_demographics")
writeData(out, sheet = "Formatted_demographics", x = Formatted_demographics, rowNames = FALSE)
writeData(out, sheet = "Unformatted_demographics", x = Demographics, rowNames = FALSE)
saveWorkbook(out, outFile, overwrite = TRUE)

# writes to file: 
#setwd(outDir)
#outFile = "Wilcoxon and T test for selected variables.xlsx"
#out = createWorkbook()
#addWorksheet(out, "Delta values")
#addWorksheet(out, "Demographics baseline values")
#writeData(out, sheet = "Delta values", x = a, rowNames = FALSE)
#writeData(out, sheet = "Demographics baseline values", x = comparingTestsDF, rowNames = FALSE)
#saveWorkbook(out, outFile, overwrite = TRUE)

##############################################################################
# print to file: 
setwd(outDir)
outFile = "Normality testing with Shapiro Wilk test.xlsx"
out = createWorkbook()
addWorksheet(out, "Delta values")
addWorksheet(out, "Demographic baseline values")
writeData(out, sheet = "Delta values", x = normality_test, rowNames = FALSE)
writeData(out, sheet = "Demographic baseline values", x = Demo_normality, rowNames = FALSE)
saveWorkbook(out, outFile, overwrite = TRUE)


outFile = "Adverse Events.xlsx"
out = createWorkbook()
addWorksheet(out, "adverse_events")
writeData(out, sheet = "adverse_events", x = ae_frame, rowNames = FALSE)
saveWorkbook(out, outFile, overwrite = TRUE)




