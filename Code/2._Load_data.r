# Loads data from datasets into data frames:
# Requires NR_SAFE_Functions to have been run first:


# Set input directory where data files are located: 
inDir = ""
setwd(inDir)
# Set output directory where output folders and files will be generated:
outDir = ""

inputFile = "VieDocData_in_sheets.xlsx"
inputFileAll = "VieDoc_Data.xlsx"
inputNAD = "NADmed_data.xlsx" # NB! Requires that you add column with activityId and EventName:
inputMS_omics_blood = "LC_MS_Requested_Metabolomics_whole_blood.xlsx"
inputMS_omics_urine = "LC_MS_Requested_Metabolomics_urine.xlsx"
inputMS_omics_blindkey = "BlindingKey_Metabolomics.xlsx"
inputMS_omics_all_annotationsBlood = "LC_MS_Selected_Metabolomics_whole_blood_annotation_1-3.xlsx"

#filter NR and Placebo Subjects into data frame
Blind = read_excel(inputFile, sheet = "Blindkey", skip = 1)
nr_ids = filter(Blind, RANDTRTCD == '1')
pl_ids = filter(Blind, RANDTRTCD == '0')

#read all sheet names in Excel file
sheet_names = excel_sheets(inputFileAll) 

#join data sheet with blinkey sheet: 
data_nr = joinDataFrames("Data", nr_ids, "NR", inputFileAll)
data_pl = joinDataFrames("Data", pl_ids, "Placebo", inputFileAll)
data = rbind(data_nr, data_pl)

#Get VS (vital signs) sheet: 
data_vs_nr = joinDataFrames("VS", nr_ids, "NR", inputFile)
data_vs_pl = joinDataFrames("VS", pl_ids, "Placebo", inputFile)
data_vs = rbind(data_vs_nr, data_vs_pl)

#Get SR (blood values) sheet: 
data_sr_nr = joinDataFrames("SR", nr_ids, "NR", inputFile)
data_sr_pl = joinDataFrames("SR", pl_ids, "Placebo", inputFile)
data_sr = rbind(data_sr_nr, data_sr_pl)

#Read in adverse events sheet from "sheeted" file
data_ae_nr = joinDataFrames("AE", nr_ids, "NR", inputFile)
data_ae_pl = joinDataFrames("AE", pl_ids, "Placebo", inputFile)
data_ae = rbind(data_ae_nr, data_ae_pl)


#Reads and formats UPDRS sheets from VieDocData_in_sheets file:
# Calculates Sum of MDS-UPDRS subscores: 
UPDRS_sheet_1 = read_excel(inputFile, sheet = "MDSUPDRP1", skip = 1)
UPDRS_sheet_2 = read_excel(inputFile, sheet = "MDSUPDRP2", skip = 1)
UPDRS_sheet_3 = read_excel(inputFile, sheet = "MDSUPDRP3", skip = 1)
UPDRS_sheet_4 = read_excel(inputFile, sheet = "MDSUPDRP4", skip = 1)
UPDRS_sheet_T = read_excel(inputFile, sheet = "MDSUPDRTO", skip = 1)
		# 13 items
upPart1 = UPDRS_sheet_1[,c("SubjectId", "ActivityId", "MDSUPDRS11CD", "MDSUPDRS12CD", "MDSUPDRS13CD", "MDSUPDRS14CD", "MDSUPDRS15CD", "MDSUPDRS16CD",				
						"MDSUPDRS17CD", "MDSUPDRS18CD", "MDSUPDRS19CD", "MDSUPDRS110CD", "MDSUPDRS111CD", "MDSUPDRS112CD", "MDSUPDRS113CD")]
sum_UPDRS_1 = upPart1[,c(3:length(colnames(upPart1)))] 
sum_UPDRS_1 <- sum_UPDRS_1 %>% mutate_if(is.character, as.numeric)
sum_UPDRS_1 = rowSums(sum_UPDRS_1, na.rm=TRUE)
UPDRS1 = upPart1[c("SubjectId", "ActivityId")]
UPDRS1[,"MDSUPDRS_Section1"] = sum_UPDRS_1
		# 13 items
upPart2 = UPDRS_sheet_2[,c("SubjectId", "ActivityId", "MDSUPDRS21CD",	"MDSUPDRS22CD",	"MDSUPDRS23CD",	"MDSUPDRS24CD",	"MDSUPDRS25CD",	"MDSUPDRS26CD",	
						"MDSUPDRS27CD",	"MDSUPDRS28CD",	"MDSUPDRS29CD",	"MDSUPDRS210CD", "MDSUPDRS211CD", "MDSUPDRS212CD", "MDSUPDRS213CD")]	
sum_UPDRS_2 = upPart2[,c(3:length(colnames(upPart2)))] 
sum_UPDRS_2 <- sum_UPDRS_2 %>% mutate_if(is.character, as.numeric)
sum_UPDRS_2 = rowSums(sum_UPDRS_2, na.rm=TRUE)
UPDRS2 = upPart2[c("SubjectId", "ActivityId")]
UPDRS2[,"MDSUPDRS_Section2"] = sum_UPDRS_2
			# items 33 (Hoehn and Yahr score excluded)
upPart3 = UPDRS_sheet_3[,c("SubjectId", "ActivityId", "MDSUPDRS31CD", "MDSUPDRS32CD", "MDSUPDRS33_NeckCD", "MDSUPDRS33_RUECD", "MDSUPDRS33_LUECD", 
						"MDSUPDRS33_RLECD", "MDSUPDRS33_LLECD", "MDSUPDRS34_RCD", "MDSUPDRS34_LCD", "MDSUPDRS35_RCD", "MDSUPDRS35_LCD", "MDSUPDRS36_RCD", 
						"MDSUPDRS36_LCD", "MDSUPDRS37_RCD", "MDSUPDRS37_LCD", "MDSUPDRS38_RCD", "MDSUPDRS38_LCD", "MDSUPDRS39CD", "MDSUPDRS310CD", "MDSUPDRS311CD", 
						"MDSUPDRS312CD", "MDSUPDRS313CD", "MDSUPDRS314CD", "MDSUPDRS315_RCD", "MDSUPDRS315_LCD", "MDSUPDRS316_RCD", "MDSUPDRS316_LCD", 
						"MDSUPDRS317_RUECD", "MDSUPDRS317_LUECD", "MDSUPDRS317_RLECD", "MDSUPDRS317_LLECD", "MDSUPDRS317_LipCD", "MDSUPDRS318CD")]
sum_UPDRS_3 = upPart3[,c(3:length(colnames(upPart3)))] 
sum_UPDRS_3 <- sum_UPDRS_3 %>% mutate_if(is.character, as.numeric)
sum_UPDRS_3 = rowSums(sum_UPDRS_3, na.rm=TRUE)
UPDRS3 = upPart3[c("SubjectId", "ActivityId")]
UPDRS3[,"MDSUPDRS_Section3"] = sum_UPDRS_3
			# items 6
upPart4 = UPDRS_sheet_4[,c("SubjectId", "ActivityId", "MDSUPDRS41CD", "MDSUPDRS42CD", "MDSUPDRS43CD", "MDSUPDRS44CD", "MDSUPDRS45CD", "MDSUPDRS46CD")]
sum_UPDRS_4 = upPart4[,c(3:length(colnames(upPart4)))] 
sum_UPDRS_4 <- sum_UPDRS_4 %>% mutate_if(is.character, as.numeric)
sum_UPDRS_4 = rowSums(sum_UPDRS_4, na.rm=TRUE)
UPDRS4 = upPart4[c("SubjectId", "ActivityId")]
UPDRS4[,"MDSUPDRS_Section4"] = sum_UPDRS_4

hoehnYahr = UPDRS_sheet_3[,c("SubjectId", "ActivityId", "EventName", "HoehnYahrCD", "MDSUPDRS_DysPresCD", "MDSUPDRP34", "MDSUPDRP32CD", "MDSUPDRP33CD")]
colnames(hoehnYahr) = c("SubjectId", "ActivityId", "EventName", "HoehnYahrCD", "Dyskinesia_present", "Minutes_since_levodopa", "Patient_in_on", "Patient_on_levodopa")

UPDRS = merge(UPDRS1, UPDRS2, by = c("SubjectId", "ActivityId"))
UPDRS = merge(UPDRS, UPDRS3, by = c("SubjectId", "ActivityId"))
UPDRS = merge(UPDRS, UPDRS4, by = c("SubjectId", "ActivityId"))
UPDRS = merge(UPDRS, hoehnYahr, by = c("SubjectId", "ActivityId"))
UPDRS[,"MDSUPDRTOS"] = UPDRS[,"MDSUPDRS_Section1"] + UPDRS[,"MDSUPDRS_Section2"] + UPDRS[,"MDSUPDRS_Section3"] +UPDRS [,"MDSUPDRS_Section4"]

Randomisations = Blind[,c("SubjectId", "RANDTRT")]
Randomisations[Randomisations == "3000mg NR"] <- "NR"
colnames(Randomisations) = c("SubjectId", "Randomisation")
data_up = merge(Randomisations, UPDRS, by = "SubjectId")

data_up[,"EventId"] = data_up[,"ActivityId"]
data_up[,"EventId"] = gsub("_1", "", data_up[,"ActivityId"])


#Reads and formats NADmed data file for analysis
NAD_data = read_excel(inputNAD, sheet = "Results")
nad = NAD_data
names(nad)[names(nad) == "Subject key"] <- "SubjectId"
names(nad)[names(nad) == "Treatment group"] <- "Randomisation"
nad[,"EventId"] = nad[,"ActivityId"] 
nad = nad %>%  mutate(Randomisation = recode(Randomisation, '3000mg NR' = 'NR'))
nad = nad %>%  mutate(ActivityId = recode(ActivityId, 'V02' = 'V02_1'))
nad = nad %>%  mutate(ActivityId = recode(ActivityId, 'V09' = 'V09_1'))
colnames(nad) = gsub("/", "_", colnames(nad))  #replaces / with _ 
colnames(nad) = gsub(" ", "", colnames(nad))  #replaces " " with ""
colnames(nad) = gsub(",", "_", colnames(nad))  #replaces " " with ""
colnames(nad) = gsub("[+]", "_", colnames(nad))  #replaces " " with ""

#Reads and formats MS_omics files for analysis:
# Specifically it makes a column with times and SubjectID according to the other datasets 
MS_omics_blindKey = read_excel(inputMS_omics_blindkey, sheet = "Sheet1", skip = 11)
MS_omics_blindKey = MS_omics_blindKey[-1,]
MS_omics_blindKey = as.data.frame(MS_omics_blindKey)
MS_omics_blindKey = MS_omics_blindKey %>% mutate(ActivityId = case_when(endsWith(MS_omics_blindKey[,"Sample Label"], "1") ~ "V02_1", endsWith(MS_omics_blindKey[,"Sample Label"], "2") ~ "V09_1"))
MS_omics_blindKey[,"SubjectId"] = substring(MS_omics_blindKey[,"Sample Label"], 0, 4)
MS_omics_blindKey[,"SubjectId"] = substring(MS_omics_blindKey[,"SubjectId"], 3)
MS_omics_blindKey[,"SubjectId"] = gsub("-", "", MS_omics_blindKey[,"SubjectId"])
MS_omics_blindKey[,"SubjectId"] = paste0("01-", MS_omics_blindKey[,"SubjectId"])
MS_omics_blindKey = MS_omics_blindKey[,c(2,3,5,6,7)]
colnames(MS_omics_blindKey)[3] = "Randomisation"
MS_omics_blindKey = MS_omics_blindKey %>% mutate(EventName = case_when(endsWith(ActivityId, "02_1") ~ "Baseline",    endsWith(ActivityId, "09_1") ~ "Day28"))

# whole blood:
MS_omics_req_blood = readOmicsReqSheet(inputMS_omics_blood)
list_var_ms_blood_req = MS_omics_req_blood[1:5,c(-1, -2)]
list_var_ms_blood_req[6,] = colnames(list_var_ms_blood_req)
list_var_ms_blood_req[7,] = colnames(list_var_ms_blood_req)
list_var_ms_blood_req = list_var_ms_blood_req[c(6,7,4,1,2,3),]
MS_omics_req_blood = MS_omics_req_blood[c(-1, -2, -3, -4),]

# calculates and adds a column with SAM/SAH ratio:
MS_omics_req_blood[,"SAM_SAH"] = as.numeric(unlist(MS_omics_req_blood[,"S-Adenosylmethionine"])) / as.numeric(unlist(MS_omics_req_blood[,"S-Adenosylhomocysteine"]))
list_var_ms_blood_req[,"SAM_SAH"] = c("SAM_SAH ratio", "SAM_SAH ratio", "Relative", "0", "0", "0")

#joins data frames from MS_omics_req with blindKey file:
MS_omics_req_blood = inner_join(MS_omics_blindKey, MS_omics_req_blood, "Sample number")

MS_omics_all_annotationsBlood = readOmicsAllAnnotations(inputMS_omics_all_annotationsBlood)
list_allAnno_ms_blood = MS_omics_all_annotationsBlood[1:10,]
list_allAnno_ms_blood = list_allAnno_ms_blood[,c(-1, -2)]
MS_omics_all_blood = MS_omics_all_annotationsBlood[c(-1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11),]
MS_omics_all_blood = MS_omics_all_blood[,-3]
MS_omics_all_blood = inner_join(MS_omics_blindKey, MS_omics_all_blood, "Sample number")
otherAnnoSubset = MS_omics_all_blood[,c( "ActivityId", "SubjectId", "R000073", "SR00076")] #"SR00089")]
#otherAnnoSubset = MS_omics_all_blood[,c( "ActivityId", "SubjectId", "R001310", "R000073", "SR00076")] #"SR00089")]
otherAnnoSubset$combineID = paste(otherAnnoSubset$SubjectId, otherAnnoSubset$ActivityId, sep = "_")
otherAnnoSubset = otherAnnoSubset[,c(-1, -2)]

MS_omics_req_blood$combineID = paste(MS_omics_req_blood$SubjectId, MS_omics_req_blood$ActivityId, sep = "_")
MS_omics_req_blood = merge(MS_omics_req_blood, otherAnnoSubset, by = "combineID")
MS_omics_req_blood = MS_omics_req_blood[,-1]

# adds cyclic ADPR, Betaine and Methionine from different annotations to list of variables for MS_omics whole blood:
#list_var_ms_blood_req[,"R001310"] = c(list_allAnno_ms_blood[7,"R001310"], list_allAnno_ms_blood[7,"R001310"], list_allAnno_ms_blood[10,"R001310"], list_allAnno_ms_blood[1,"R001310"], NA, list_allAnno_ms_blood[2,"R001310"])
list_var_ms_blood_req[,"R000073"] = c(list_allAnno_ms_blood[7,"R000073"], list_allAnno_ms_blood[7,"R000073"], list_allAnno_ms_blood[10,"R000073"], list_allAnno_ms_blood[1,"R000073"], NA, list_allAnno_ms_blood[2,"R000073"])
list_var_ms_blood_req[,"SR00076"] = c(list_allAnno_ms_blood[7,"SR00076"], list_allAnno_ms_blood[7,"SR00076"], list_allAnno_ms_blood[10,"SR00076"], list_allAnno_ms_blood[1,"SR00076"], NA, list_allAnno_ms_blood[2,"SR00076"])

########################################

# urine:
MS_omics_req_urine = readOmicsReqSheet(inputMS_omics_urine)
list_var_ms_urine_req = MS_omics_req_urine[1:5,c(-1, -2)]
list_var_ms_urine_req[6,] = colnames(list_var_ms_urine_req)
list_var_ms_urine_req[7,] = colnames(list_var_ms_urine_req)
list_var_ms_urine_req = list_var_ms_urine_req[c(6,7,4,1,2,3),]
MS_omics_req_urine = MS_omics_req_urine[c(-1, -2, -3, -4),]

#joins data frames from MS_omics_req with blindKey file:
MS_omics_req_urine = inner_join(MS_omics_blindKey, MS_omics_req_urine, "Sample number")

# This setting chooses which test (Wilcoxon or T-test) to be used. TRUE is Wilcoxon, FALSE is T-test: 
# Row 7 in the list of metabolites to be analysed is the marker for which test to be used. 
# Set method in dataFrame list with all variables to be analysed: 
list_var_ms_blood_req[7,] = FALSE
list_var_ms_urine_req[7,] = FALSE




